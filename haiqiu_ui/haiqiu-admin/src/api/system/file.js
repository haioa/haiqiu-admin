import request from '@/utils/request'

// 查询FTP文件列表
export function listFtp(query) {
  return request({
    url: '/file/ftp/list',
    method: 'get',
    data: query
  })
}


// 查询服务器文件列表
export function listServer(folder,name,sort) {
  return request({
    url: '/file/server/list',
    method: 'get',
    params: {
      folder,name,sort
    }
  })
}


// 获取当前目录的上一级目录
export function getBackFolder(folder) {
  return request({
    url: '/file/server/back',
    method: 'get',
    params: {
      folder
    }
  })
}

// 服务器下载文件
export function downloadServerFile(filePath) {
  return request({
    responseType: 'blob',
    url: '/file/server/download',
    method: 'get',
    params: {
      filePath
    },
    timeout: 1800000, //设置超时
  })
}


// 服务器批量删除文件
export function deleteFiles(filePaths) {
  return request({
    url: '/file/server/delete',
    method: 'delete',
    data: filePaths
  })
}

// 服务器创建文件夹
export function addFolder(folders) {
  return request({
    url: '/file/server/folder',
    method: 'post',
    params: {
      folders
    }
  })
}

// 服务器重命名文件夹或文件
export function renameFile(filePath, newName, parentPath) {
  return request({
    url: '/file/server/rename',
    method: 'get',
    params: {
      filePath,
      newName,
      parentPath
    }
  })
}

// 服务器复制文件或文件夹
export function copyFile(filePath, newName, parentPath) {
  return request({
    url: '/file/server/copy',
    method: 'get',
    params: {
      filePath,
      newName,
      parentPath
    }
  })
}


// 多模式批量上传文件
export function uploadFiles(formData, onUploadProgress) {
  return request({
    url: '/file/upload',
    method: 'post',
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    onUploadProgress,
    timeout: 1800000, //设置超时
  })
}
