import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/role/list',
    method: 'post',
    data: query
  })
}

// 查询角色列表
export function allRole() {
  return request({
    url: '/role/all',
    method: 'post',
  })
}


// 查询角色详细
export function getRole(id) {
  return request({
    url: '/role',
    method: 'get',
    params: {
      id
    }
  })
}

// 新增角色
export function addRole(data) {
  return request({
    url: '/role',
    method: 'post',
    data: data
  })
}

// 修改角色
export function updateRole(data) {
  return request({
    url: '/role',
    method: 'put',
    data: data
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/system/role/dataScope',
    method: 'put',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(id, active) {
  const data = {
    id,
    active
  }
  return request({
    url: '/role/active',
    method: 'put',
    data: [data]
  })
}

// 删除角色
export function delRole(ids) {
  return request({
    url: '/role',
    method: 'delete',
    data: ids,
  })
}

// 导出角色
export function exportRole(query) {
  return request({
    url: '/system/role/export',
    method: 'get',
    params: query
  })
}

// 查询角色已授权用户列表
export function allocatedUserList(query) {
  return request({
    url: '/role/allocatedList',
    method: 'get',
    params: query
  })
}

// 查询角色未授权用户列表
export function unallocatedUserList(query) {
  return request({
    url: '/role/unallocatedList',
    method: 'get',
    params: query
  })
}

// 批量取消或者批量添加用户授权角色
export function authorize(data) {
  return request({
    url: '/role/authorize',
    method: 'put',
    data: data

  })
}

// 取消用户授权角色
export function authUserCancel(data) {
  return request({
    url: '/system/role/authUser/cancel',
    method: 'put',
    data: data
  })
}

// 批量取消用户授权角色
export function authUserCancelAll(data) {
  return request({
    url: '/system/role/authUser/cancelAll',
    method: 'put',
    params: data
  })
}

// 授权用户选择
export function authUserSelectAll(data) {
  return request({
    url: '/system/role/authUser/selectAll',
    method: 'put',
    params: data
  })
}
