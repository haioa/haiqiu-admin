import request from '@/utils/request'

// 查询公告列表
export function listNotice(query) {
  return request({
    url: '/notice/list',
    method: 'post',
    data: query
  })
}

// 查询公告详细
export function getNotice(id) {
  return request({
    url: '/notice',
    method: 'get',
    params: {
      id
    }
  })
}

// 新增公告
export function addNotice(data) {
  return request({
    url: '/notice',
    method: 'post',
    data: data
  })
}

// 修改公告
export function updateNotice(data) {
  return request({
    url: '/notice',
    method: 'put',
    data: data
  })
}

// 删除公告
export function delNotice(ids) {
  return request({
    url: '/notice',
    method: 'delete',
    data: ids
  })
}


export function noticeList(data) {
  return request({
    url: "/notice/list",
    method: "post",
    data
  })
}



export function updateNoticeByView(id) {
  return request({
    url: "/notice/view",
    method: "put",
    params: {
      id
    }
  })
}


export function getStatistics() {
  return request({
    url: "/notice/statistics",
    method: "get",
  })
}
