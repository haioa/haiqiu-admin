import request from '@/utils/request'

// 分页邮件
export function listEmail(data) {
  return request({
    url: '/email/list',
    method: 'post',
    data: data
  })
}


// 获取邮件
export function getEmail(id) {
    return request({
      url: '/email',
      method: 'get',
      params: {id}
    })
  }


  // 删除邮件
export function delEmail(ids) {
    return request({
      url: '/email',
      method: 'delete',
      data: ids
    })
  }


  // 添加邮件
export function addEmail(data) {
    return request({
      url: '/email',
      method: 'post',
      data: data
    })
  }

    // 修改邮件
export function updateEmail(data) {
    return request({
      url: '/email',
      method: 'put',
      data: data
    })
  }