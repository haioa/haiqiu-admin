import request from '@/utils/request'

// 查询岗位列表
export function listPost(query) {
  return request({
    url: '/position/list',
    method: 'post',
    data: query
  })
}

// 查询岗位详细
export function getPost(id) {
  return request({
    url: '/position',
    method: 'get',
    params: {id}
  })
}

// 查询全部岗位
export function allPost(positionName) {
  return request({
    url: '/position/all',
    method: 'get',
    params: {positionName}
  })
}

// 新增岗位
export function addPost(data) {
  return request({
    url: '/position',
    method: 'post',
    data: data
  })
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: '/position',
    method: 'put',
    data: data
  })
}

// 删除岗位
export function delPost(postId) {
  return request({
    url: '/position',
    method: 'delete',
    data: postId
  })
}

// 导出岗位
export function exportPost(query) {
  return request({
    url: '/system/post/export',
    method: 'get',
    params: query
  })
}