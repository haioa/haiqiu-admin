import request from '@/utils/request'
import {
  praseStrEmpty
} from "@/utils/ruoyi";

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/user/list',
    method: 'post',
    data: query
  })
}

// 查询用户详细
export function getUser(id) {
  return request({
    url: '/user',
    method: 'get',
    params: {
      id
    },
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/user',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateUser(data) {
  return request({
    url: '/user',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delUser(ids) {
  return request({
    url: '/user',
    method: 'delete',
    data: ids,
  })
}

// 导出用户
export function exportUser(query) {
  return request({
    responseType: 'blob',
    url: '/user/export',
    method: 'get',
    // params: {
    //   data: 1
    // }
  })
}

// 用户密码重置
export function resetUserPwd(userId, newPassword) {
  const data = {
    userId,
    newPassword
  }
  return request({
    url: '/user/resetPwd',
    method: 'put',
    data: data
  })
}

// 用户状态修改
export function changeUserStatus(id, active) {
  const data = {
    id,
    active
  }
  return request({
    url: '/user/active',
    method: 'put',
    data: data
  })
}

// 查询用户个人信息
export function getUserProfile() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

// 修改用户个人信息
export function updateUserProfile(data) {
  return request({
    url: '/user/profile',
    method: 'put',
    params: data
  })
}

// 用户密码个人中心修改
export function updateUserPwd(oldPassword, newPassword) {
  const data = {
    oldPassword,
    newPassword
  }
  return request({
    url: '/user/profile/updatePwd',
    method: 'put',
    data: data
  })
}

// 用户头像上传
export function uploadAvatar(data) {
  return request({
    url: '/user/profile/avatar',
    method: 'post',
    data: data
  })
}

// 下载用户导入模板
export function importTemplate() {
  return request({
    url: '/system/user/importTemplate',
    method: 'get'
  })
}

// 查询授权角色
export function getAuthRole(userId) {
  return request({
    url: '/system/user/authRole/' + userId,
    method: 'get'
  })
}

// 保存授权角色
export function updateAuthRole(data) {
  return request({
    url: '/user/authRole',
    method: 'put',
    params: data
  })
}

// 同步在线用户信息
export function online() {
  return request({
    url: '/user/online',
    method: 'get'
  })
}
