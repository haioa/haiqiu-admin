import request from '@/utils/request'

// 查询部门列表
export function listDept(query) {
  return request({
    url: '/system/dept/list',
    method: 'get',
    params: query
  })
}

// 查询全部部门列表
export function allDept(param) {
  return request({
    url: '/dept/all',
    method: 'get',
    params: param
  })
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: '/system/dept/list/exclude/' + deptId,
    method: 'get'
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/dept',
    method: 'get',
    params:{deptId}
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: '/dept/tree',
    method: 'post'
  })
}

// 根据角色ID查询部门树结构
export function roleDeptTreeselect(roleId) {
  return request({
    url: '/system/dept/roleDeptTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addDept(data) {
  return request({
    url: '/dept',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/dept',
    method: 'put',
    data: data
  })
}

// 部门状态修改
export function changeDeptStatus(data) {
  return request({
    url: '/dept/active',
    method: 'put',
    data: data
  })
}

// 删除部门
export function delDept(deptId) {
  return request({
    url: '/dept',
    method: 'delete',
    data: deptId
  })
}