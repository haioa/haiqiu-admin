import request from '@/utils/request'

// 获取聊天用户列表
export function listUser() {
    return request({
      url: '/chat/list',
      method: 'post',
    })
}