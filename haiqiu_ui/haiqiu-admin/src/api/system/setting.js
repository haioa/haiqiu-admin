import request from '@/utils/request'

export function getSetting(){
    return request({
        url: '/system/config',
        method: 'get',
    
    })
}


// 获取初始化系统参数（系统名称，logo，登录背景图等信息）
export function getConfigMsg(){
    return request({
        url: '/system/theme',
        method: 'get',
    
    })
}

//修改系统参数配置
export function updateSetting(data){
    return request({
        url: '/system/config',
        method: 'put',
        data: data
    
    })
}