import request from '@/utils/request'

// 查询操作日志列表
export function list(query) {
  return request({
    url: '/system/log/list',
    method: 'post',
    data: query
  })
}

// 删除操作日志
export function delOperlog(operId) {
  return request({
    url: '/system/log',
    method: 'delete',
    data: operId
  })
}

// 清空操作日志
export function cleanOperlog() {
  return request({
    url: '/monitor/operlog/clean',
    method: 'delete'
  })
}

// 导出操作日志
export function exportOperlog(query) {
  return request({
    url: '/monitor/operlog/export',
    method: 'get',
    params: query
  })
}

//日志区域统计
export function statistics() {
  return request({
    url: '/system/log/statistics',
    method: 'get'
  })
}