import request from '@/utils/request'

// 查询在线用户列表
export function list(query) {
  return request({
    url: '/user/online/list',
    method: 'get',
    params: query,
  })
}

// 强退用户
export function forceLogout(token) {
  return request({
    url: '/user/online',
    method: 'delete',
    params: {
      token
    }
  })
}
