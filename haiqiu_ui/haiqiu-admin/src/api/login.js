import request from '@/utils/request'
import md5 from "js-md5";

// 登录方法
export function login(username, password, captcha, key) {
  const data = {
    username,
    password,
    captcha,
    key
  }
  return request({
    url: '/login',
    method: 'post',
    data: data
  })
}

// 注册方法
export function register(data) {
  //md5加密密码
  data.password = md5(
    data.password
  );
  data.confirmPassword = md5(
    data.confirmPassword
  );
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/account/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captcha',
    method: 'get',
    timeout: 20000
  })
}

// 忘记密码验证
export function forgetVerify(email) {
  return request({
    url: '/forget/code',
    method: 'get',
    params: { email },
    timeout: 20000,
  })
}

// 忘记密码重置
export function forgetReset(data) {
  return request({
    url: '/forget/reset',
    method: 'post',
    data: data,
    timeout: 20000,
  })
}