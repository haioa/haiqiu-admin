import { createRouter, createWebHistory } from 'vue-router'
import index from '../views/index.vue'
const routerHistory = createWebHistory()
// createWebHashHistory hash 路由
// createWebHistory history 路由
// createMemoryHistory 带缓存 history 路由
const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: '/',
      component: index
    },
  ]
})
 
export default router