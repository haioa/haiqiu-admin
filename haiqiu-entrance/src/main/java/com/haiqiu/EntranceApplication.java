package com.haiqiu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author HaiQiu
 * @date 2021/4/1 20:25
 * @desc
 **/
@EnableScheduling
@SpringBootApplication
@MapperScan("com.haiqiu.*.mapper")
public class EntranceApplication {
    public static void main(String[] args) {
        SpringApplication.run(EntranceApplication.class,args);
        System.out.println("\n" +
                "+-----------+-----------+-----------+-----------+-----------+\n" +
                "|                     HaiQiu-Admin                          |\n" +
                "|   swagger文档地址：http://localhost:8888/swagger-ui.html    |\n" +
                "+                                                           +\n" +
                "+-----------+-----------+-----------+-----------+-----------+ ");
    }
}
