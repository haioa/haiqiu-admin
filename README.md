<div align="center">
	<img src="data:image/svg+xml;base64,PHN2ZyB0PSIxNjM0MDIyODA0NjQwIiBjbGFzcz0iaWNvbiIgdmlld0JveD0i%0AMCAwIDEwMjcgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3%0Ady53My5vcmcvMjAwMC9zdmciIHAtaWQ9Ijg1MjIiIHdpZHRoPSIyMDAiIGhl%0AaWdodD0iMjAwIj48cGF0aCBkPSJNOTg5LjY5MDQ4OCAxLjc2NDUyOWE2Ljgx%0AOTk5NiA2LjgxOTk5NiAwIDAgMC01LjgwMDk5NyAyLjMxNjk5OUw4NjIuODk0%0ANTUzIDE3Ny41NzM0MzhhMzkuNDAzOTggMzkuNDAzOTggMCAwIDEtMzIuMjA0%0AOTgzIDE3LjExNjk5MUg1NTkuOTc5NzFhMzkuMDMxOTggMzkuMDMxOTggMCAw%0AIDEtMzEuMDQ0OTg0LTE1LjY2OTk5MkwzOTEuOTY3Nzk3IDAuODcyNTNBMy41%0AMTE5OTggMy41MTE5OTggMCAwIDAgMzg5LjY0OTc5OCAwLjAwMDUzYTUuNzk0%0AOTk3IDUuNzk0OTk3IDAgMCAwLTIuNjEyOTk4IDAuNTgzTDM2MC42MTk4MTMg%0AOTUxLjk5NzAzN2E0MC4zNzk5NzkgNDAuMzc5OTc5IDAgMCAxLTM5Ljc0Nzk3%0AOSAzOS43NDg5OGgtMC4yODhhMzIyLjkxMTgzMyAzMjIuOTExODMzIDAgMCAx%0ALTEyOS42OTY5MzMtMjguNzI3OTg1Yy0wLjI4OCAwLTAuNTgzLTAuMjg4LTAu%0AODcwOTk5LTAuMjg4YTEzMi4yNTk5MzIgMTMyLjI1OTkzMiAwIDAgMS03OS43%0AOTE5NTktMTIxLjI4MTkzOGMtMS40NTM5OTktNDEuNDg5OTc5IDExLjYwMjk5%0ANC04My41ODQ5NTcgMjIuMDQ3OTg5LTExNy4yMjI5MzkgMi4zMTc5OTktNy44%0AMzE5OTYgNC42NDI5OTgtMTUuMDg2OTkyIDYuMzg0OTk2LTIxLjQ3MTk4OWE1%0ANzcuNzM5NzAxIDU3Ny43Mzk3MDEgMCAwIDAgMjMuNTAxOTg4LTE3NS41NDE5%0AMDljLTIuMDI5OTk5LTg0LjEzOTk1Ni01OS40Nzk5NjktMTU2LjA5OTkxOS0x%0ANDMuMDQyOTI2LTE3OS42MDI5MDdhMTkuNzE1OTkgMTkuNzE1OTkgMCAwIDAt%0ANS41MTM5OTctMC44Njk5OTljLTguOTk2OTk1IDAtMTIuNzY5OTkzIDExLjYw%0AMjk5NC0xMy4zNDQ5OTMgMTYuMjQ1OTkxLTEuNzQxOTk5IDEyLjc2OTk5MyA1%0ALjgwMTk5NyAxNC43OTk5OTIgOS44MzI5OTUgMTUuOTU3OTkyQzU2LjUxMTk3%0AMSAzOTIuMDAwMzI3IDkwLjQ1Nzk1MyA0MTcuNTMyMzE0IDExMC43NzA5NDMg%0ANDU1LjI1MDI5NGM0My4yMzI5NzggNzkuNzkxOTU5IDEyLjE4Njk5NCAxODcu%0AMTQ1OTAzLTEwLjQ0Mzk5NSAyNjUuNTA0ODYzLTEuNzQxOTk5IDYuMDg5OTk3%0ALTMuNTExOTk4IDExLjg5ODk5NC01LjIyNTk5NyAxNy42OTk5OTFDNzYuMjQw%0AOTYxIDgwNS4xODQxMTMgNzUuOTUzOTYxIDg1MC4xMzcwOSA5My42NDY5NTIg%0AOTA1LjAwMDA2MmMyOS4zMDM5ODUgOTAuMjM2OTUzIDE0MC43MjQ5MjcgMTE0%0ALjAyNjk0MSAyMjkuMjE5ODgxIDExOC4zODE5MzhhMzYuMjA3OTgxIDM2LjIw%0ANzk4MSAwIDAgMSA2LjA4OTk5NyAwLjU4M2g2NjAuMzc1NjU4YTM2LjMxOTk4%0AMSAzNi4zMTk5ODEgMCAwIDAgMjYuOTg1OTg2LTExLjg5ODk5NCAzOS4yOTE5%0AOCAzOS4yOTE5OCAwIDAgMCAxMC43MzI5OTQtMjcuNTYxOTg2TDk5Ny4yMzQ0%0AODQgNi42OTQ1MjdjLTAuMjg4LTQuNjQxOTk4LTcuMjU1OTk2LTQuOTE1OTk3%0ALTcuNTQzOTk2LTQuOTE1OTk4ek01NTIuNDM2NzE0IDUyMC4yNzAyNjFhMTEz%0ALjczODk0MSAxMTMuNzM4OTQxIDAgMSAxIDExMy43MjQ5NDEtMTEzLjc1Mjk0%0AMSAxMTMuOTIwOTQxIDExMy45MjA5NDEgMCAwIDEtMTEzLjcyNDk0MSAxMTMu%0ANzUyOTQxeiBtMjk5LjcyNTg0NSAwQTExMy43Mzg5NDEgMTEzLjczODk0MSAw%0AIDEgMSA5NjUuODk5NSA0MDYuNTMyMzJhMTEzLjkyMDk0MSAxMTMuOTIwOTQx%0AIDAgMCAxLTExMy43Mzc5NDEgMTEzLjczODk0MXogbTAgMCIgcC1pZD0iODUy%0AMyIgZmlsbD0iIzEyOTZkYiI+PC9wYXRoPjxwYXRoIGQ9Ik01NTIuNDI5NzE0%0AIDM2My4yODYzNDJhNDMuMjMxOTc4IDQzLjIzMTk3OCAwIDEgMCA0My4yMzE5%0ANzggNDMuMjMxOTc4IDQzLjMzNzk3OCA0My4zMzc5NzggMCAwIDAtNDMuMjMx%0AOTc4LTQzLjIzMTk3OHogbTAgMGgyOTkuNzI0ODQ1YTQzLjIzMTk3OCA0My4y%0AMzE5NzggMCAxIDAgNDMuMjMyOTc4IDQzLjIzMTk3OCA0My4zMzc5NzggNDMu%0AMzM3OTc4IDAgMCAwLTQzLjIzMjk3OC00My4yMzE5Nzh6IG0wIDAiIHAtaWQ9%0AIjg1MjQiIGZpbGw9IiMxMjk2ZGIiPjwvcGF0aD48L3N2Zz4=">
	<!-- <p align="center">
	    <a href="https://v3.vuejs.org/" target="_blank">
	        <img src="https://img.shields.io/badge/vue.js-vue3.x-green" alt="vue">
	    </a>
	    <a href="https://element-plus.gitee.io/#/zh-CN/component/changelog" target="_blank">
	        <img src="https://img.shields.io/badge/element--plus-%3E1.0.0-blue" alt="element plus">
	    </a>
		<a href="https://www.tslang.cn/" target="_blank">
	        <img src="https://img.shields.io/badge/typescript-%3E4.0.0-blue" alt="typescript">
	    </a>
		<a href="https://vitejs.dev/" target="_blank">
		    <img src="https://img.shields.io/badge/vite-%3E2.0.0-yellow" alt="vite">
		</a>
		<a href="https://gitee.com/haiqiu80/vue-haiqiu-admin/blob/master/LICENSE" target="_blank">
		    <img src="https://img.shields.io/badge/license-MIT-success" alt="license">
		</a>
	</p> -->
	<p>&nbsp;</p>
</div>


#### 🌈 介绍

(前端) vue2.x + webpack + element + vue-router + echarts，（后端）springboot 2.3.x + mysql8 + redis + security + ftp + oss + swagger + druid 的后台快速开发平台，希望减少工作量，帮助大家实现快速开发。

最近更新了golang版本，<a href="https://gitee.com/haioa/haiqiu-go-admin" target="_blank">haiqiu-go-admin</a>

#### 🐬内置功能
- 1.用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 2.部门管理：树结构展现支持数据权限。
- 3.岗位管理：配置系统用户所属担任职务。
- 4.菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 5.角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
- ~~6.字典管理：对系统中经常使用的一些较为固定的数据进行维护。~~
- ~~7.参数管理：对系统动态配置常用参数。~~
- 8.通知公告：系统通知公告信息发布维护。
- 9.操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
- 10.登录日志：系统登录日志记录查询包含登录异常。
- 11.在线用户：当前系统中活跃用户状态监控。
- ~~12.定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。~~
- ~~13.代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。~~
- 14.系统接口：根据业务代码自动生成相关的api接口文档。
- 15.服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
- 16.缓存监控：对系统的缓存信息查询，命令统计等。
- 17.在线构建器：拖动表单元素生成相应的HTML代码。
- 18.连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
- 19.文件管理：就像网盘一样管理自己服务器的所有文件
- ~~20.聊天社交：使用websocket协议的聊天IM功能~~

进度详情：带删除线表示计划中未完成

#### 🐀 本系统亮点
- 1.支持文件管理（ps：吐槽下看了好多的后台系统都没有文件管理这个功能，果断自己加上了）
- 2.权限细化到每个请求，支持动态添加修改（免去security的注解式硬编码权限注解）
- 3.支持忽略日志注解
- 4.支持令牌桶限流注解
- 5.支持websocket实时消息发送通知

#### ⛱️ 线上预览

- 在线预览（vue-haiqiu-admin）<a href="https://haiqiu.top/" target="_blank">在线体验</a>
- 体验账号密码：test/test

#### 💒 代码仓库

- 代码仓库 <a href="https://gitee.com/haioa/vue-haiqiu-admin" target="_blank">https://gitee.com/haioa/vue-haiqiu-admin</a>

#### 🚧 安装 cnpm、yarn

- 复制代码(桌面 cmd 运行) `npm install -g cnpm --registry=https://registry.npm.taobao.org`
- 复制代码(桌面 cmd 运行) `npm install -g yarn`

#### ⚡ 使用说明

建议使用 cnpm，因为 yarn 有时会报错。<a href="http://nodejs.cn/" target="_blank">node 版本 > 12xx.xx.x</a>

```bash
# 克隆项目
git clone "gitee.com/haioa/vue-haiqiu-admin.git

# 进入项目
cd vue-haiqiu-admin

# 前端运行
cd haiqiu_ui

# 安装依赖
cnpm install

# 运行项目
cnpm run dev

# 打包发布 
cnpm run build:prod

# 后端运行 
cd sql 导入相关sql文件脚本 

# IDEA打开后端文件夹
cd haiqiu-admin

# 进入haiqiu-entrance文件夹的resource配置文件夹下，
修改application.yml的mysql配置
run app
# 默认登录账号：admin admin
```

#### 🍉 项目文档
- 待完善.....

#### 🍉 git 命令

- 在本地新建一个分支：`git branch newBranch`
- 切换到你的新分支：`git checkout newBranch`
- 将新分支发布在 github、gitee 上：`git push origin newBranch`
- 在本地删除一个分支：`git branch -d newBranch`
- 在 github 远程端删除一个分支：`git push origin :newBranch (分支名前的冒号代表删除)`
- 注意删除远程分支后，如果有对应的本地分支，本地分支并不会同步删除！

#### 💯 学习交流加 QQ 群
##### 暂无QQ群，有问题请issue
- 


#### 🐀 分支说明
- 暂时有两个分支，分别为dev开发分支，master主分支，当dev分支趋近稳定将会合并到master分支 
- 开源项目离不开你的star和代码贡献，欢迎你加入一起完善本项目 


#### 🐀 系统相关截图
![输入图片说明](doc/img/login.png.png)
![输入图片说明](doc/img/home1.png)
![输入图片说明](doc/img/home2.png)
![输入图片说明](doc/img/files.png)
![输入图片说明](doc/img/server.png)
![输入图片说明](doc/img/chat.png)
![输入图片说明](doc/img/setting.png)

#### ❤️ 鸣谢列表

- <a href="https://github.com/vuejs/vue" target="_blank">vue</a>
- <a href="https://github.com/vuejs/vue-next" target="_blank">vue-next</a>
- <a href="https://github.com/ElemeFE/element" target="_blank">element-ui</a>
- <a href="https://github.com/element-plus/element-plus" target="_blank">element-plus</a>
- <a href="https://github.com/vuejs/vue-router-next" target="_blank">vue-router-nex</a>
- <a href="https://github.com/vuejs/vuex" target="_blank">vuex</a>
- <a href="https://github.com/apache/echarts" target="_blank">echarts</a>
- <a href="https://github.com/axios/axios" target="_blank">axios</a>
- <a href="https://github.com/zenorocha/clipboard.js" target="_blank">clipboard</a>
- <a href="https://github.com/inorganik/countUp.js" target="_blank">countUp</a>
- <a href="https://github.com/developit/mitt" target="_blank">mitt</a>
- <a href="https://github.com/rstacruz/nprogress" target="_blank">nprogress</a>
- <a href="https://github.com/sindresorhus/screenfull.js" target="_blank">screenfull</a>
- <a href="https://github.com/SortableJS/Sortable" target="_blank">sortablejs</a>
- <a href="https://github.com/sass/sass" target="_blank">sass</a>
- <a href="https://github.com/microsoft/TypeScript" target="_blank">typescript</a>
- <a href="https://github.com/vitejs/vite" target="_blank">vite</a>
- <a href="https://github.com/wangeditor-team/wangEditor" target="_blank">wangeditor</a>
- <a href="https://github.com/fengyuanchen/cropperjs" target="_blank">cropperjs</a>
- <a href="https://github.com/davidshimjs/qrcodejs" target="_blank">qrcodejs</a>
- <a href="https://github.com/crabbly/Print.js" target="_blank">print-js</a>
- <a href="https://github.com/likaia/screen-shot" target="_blank">vue-web-screen-shot</a>
- <a href="https://github.com/jbaysolutions/vue-grid-layout" target="_blank">vue-grid-layout</a>
- <a href="https://github.com/antoniandre/splitpanes" target="_blank">splitpanes</a>

#### 💕 特别感谢


- 

#### 💌 支持作者

如果觉得框架不错，或者已经在使用了，希望你可以去 <a target="_blank" href="https://gitee.com/haiqiu80/vue-haiqiu-admin">Github</a> 或者
<a target="_blank" href="https://gitee.com/haiqiu80/vue-haiqiu-admin">Gitee</a> 帮我点个 ⭐ Star，这将是对我极大的鼓励与支持。

#### ❤ 喝瓶可乐

如果此项目对你的工作或者学习有很大的帮助，你可以请我喝瓶快乐水。感谢你的支持，谢谢！比心
<div align="center">
	<img src="./doc/img/lADPJwY7RFaHG6_NBpbNBNo_1242_1686.jpg" width="30%">
</div>

<div align="center">
	<img src="./doc/img/lADPJv8gRwz6G7XNBwjNBLA_1200_1800.jpg" width="30%">
</div>
