package com.haiqiu.busines.controller;

import io.swagger.v3.oas.annotations.Operation;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.common.system.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author haiqiu
 */
@Tag(name = "服务器监控",description = "获取服务器的基本硬件信息")
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {
    @Operation(description = "获取服务器硬件信息")
    @GetMapping()
    public ResultData getInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        return decide(server);
    }
}
