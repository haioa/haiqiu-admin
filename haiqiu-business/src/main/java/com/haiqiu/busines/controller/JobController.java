package com.haiqiu.busines.controller;


import com.haiqiu.common.controller.BaseController;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author HaiQiu
 */
@RestController
@Tag(name = "定时任务",description = "定时任务")
@RequestMapping("/quartz")
public class JobController extends BaseController {
    private final static Logger LOGGER = LoggerFactory.getLogger(JobController.class);


}