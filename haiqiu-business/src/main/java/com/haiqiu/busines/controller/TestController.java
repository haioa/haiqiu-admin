package com.haiqiu.busines.controller;

import io.swagger.v3.oas.annotations.media.Schema;
import com.haiqiu.common.entity.WebSockMsg;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HaiQiu
 * @date 2021/10/18
 * @desc 测试
 */
@Tag( name = "测试接口")
@RestController
@RequestMapping
public class TestController {

    @RequestMapping("/test")
    public String testWebSocket(@RequestBody WebSockMsg webSockMsg){
        return "yes";
    }

}
