package com.haiqiu.busines.controller;


import com.haiqiu.busines.entity.SysNotice;
import com.haiqiu.busines.service.SysNoticeService;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author haiqiu
 * 通知公告
 */
@Tag(name = "通知公告",description = "通知公告的新增修改操作")
@RestController
@RequestMapping("/notice")
public class NoticeController extends BaseController {

    @Autowired
    private SysNoticeService sysNoticeService;

    @Operation(summary = "通知公告新增",description = "新增公告的操作")
    @PostMapping("")
    public ResultData insert(@Parameter(description = "新增公告，无须传ID") @RequestBody SysNotice sysNotice){
        sysNoticeService.insert(sysNotice);
        return decide(Constants.SUCCESS_ADD);
    }


    @Operation(summary = "通知公告修改",description = "修改公告的操作")
    @PutMapping("")
    public ResultData updateByPrimaryKeySelective(@Parameter(description = "修改公告") @RequestBody SysNotice sysNotice){
        sysNoticeService.updateByPrimaryKeySelective(sysNotice);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(summary = "通知公告增加阅读数",description = "增加阅读数公告的操作")
    @PutMapping("/view")
    public ResultData viewAdd(@Parameter(description = "数据id") @RequestParam Long id){
        sysNoticeService.viewAdd(id);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(summary = "通知公告批量删除",description = "批量删除公告的操作")
    @DeleteMapping("")
    public ResultData delBatch(@Parameter(description = "数据ID集合数组，传body") @RequestBody Set<Long> ids){
        return decide(String.format("已成功删除%d条数据", sysNoticeService.delBatch(ids)));
    }

    @Operation(summary = "通知公告模糊分页",description = "模糊分页公告的操作")
    @PostMapping("/list")
    public ResultData list(@Parameter(description = "模糊分页公告") @RequestBody PageRequest<SysNotice> request){
        return decide(sysNoticeService.list(request));
    }


    @Operation(summary = "通知公告查询单条",description = "查询单条公告的操作")
    @GetMapping("")
    public ResultData selectByPrimaryKey(@Parameter(description = "数据ID") @RequestParam Long id){
        return decide(sysNoticeService.selectByPrimaryKey(id));
    }

    @Operation(summary = "通知公告查询所有",description = "查询所有公告的操作")
    @GetMapping("/all")
    public ResultData all(){
        return decide(sysNoticeService.all());
    }

    @Operation(summary = "通知公告统计各类发布数量")
    @GetMapping("/statistics")
    public ResultData statistics(){
        return decide(sysNoticeService.statistics());
    }
}
