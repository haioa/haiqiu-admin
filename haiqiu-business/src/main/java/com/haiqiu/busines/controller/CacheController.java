package com.haiqiu.busines.controller;



import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.result.ResultData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 缓存监控
 *
 * @author haiqiu
 */
@Tag(name = "缓存监控",description = "获取redis的基本硬件信息")
@RestController
@RequestMapping("/monitor/cache")
public class CacheController extends BaseController {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Operation(description = "获取redis服务信息")
    @GetMapping()
    public ResultData getInfo() {
        Properties info = (Properties) redisTemplate.execute((RedisCallback<Object>) RedisServerCommands::info);
        Properties commandStats = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info("commandstats"));
        Object dbSize = redisTemplate.execute((RedisCallback<Object>) RedisServerCommands::dbSize);

        Map<String, Object> result = new HashMap<>(3);
        result.put("info", info);
        result.put("dbSize", dbSize);

        List<Map<String, String>> pieList = new ArrayList<>();
        Set<String> redisMsgList = commandStats.stringPropertyNames();
        for (String key : redisMsgList) {
            Map<String, String> data = new HashMap<>(2);
            String property = commandStats.getProperty(key);
            data.put("name", StringUtils.removeStart(key, "cmdstat_"));
            data.put("description", StringUtils.substringBetween(property, "calls=", ",usec"));
            pieList.add(data);
        }
        result.put("commandStats", pieList);
        return decide(result);
    }
}
