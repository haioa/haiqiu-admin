package com.haiqiu.busines.controller;


import com.haiqiu.busines.entity.SysEmail;
import com.haiqiu.busines.service.SysEmailService;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author HaiQiu
 */
@Tag(name = "邮件管理",description = "邮件发送接口操作")
@RestController
@RequestMapping("/email")
public class MailController extends BaseController {

    @Autowired
    private SysEmailService sysEmailService;


    @Operation(summary = "新增邮件",description = "新增邮箱的操作")
    @PostMapping("")
    public ResultData insert(@Parameter(description = "新增邮箱") @RequestBody @Validated SysEmail sysEmail){
        sysEmailService.insert(sysEmail);
        return decide(Constants.SUCCESS_ADD);
    }


    @Operation(summary = "修改邮件",description = "修改邮箱的操作")
    @PutMapping("")
    public ResultData updateByPrimaryKeySelective(@Parameter(description = "修改邮箱") @RequestBody @Validated SysEmail sysEmail){
        sysEmailService.updateByPrimaryKeySelective(sysEmail);
        return decide(Constants.SUCCESS_UPDATE);
    }


    @Operation(summary = "邮件批量删除",description = "批量删除邮箱的操作")
    @DeleteMapping("")
    public ResultData delBatch(@Parameter(description = "数据ID集合数组") @RequestBody Set<Long> ids){
        return decide(Constants.DELETE, sysEmailService.delBatch(ids));
    }

    @Operation(summary = "邮件模糊分页",description = "模糊分页邮箱的操作")
    @PostMapping("/list")
    public ResultData list(@Parameter(description = "模糊分页邮箱") @RequestBody PageRequest<SysEmail> request){
        return decide(sysEmailService.list(request));
    }


    @Operation(summary = "邮件查询单条",description = "查询单条邮箱的操作")
    @GetMapping("")
    public ResultData selectByPrimaryKey(@Parameter(description = "数据ID") @RequestParam Long id){
        return decide(sysEmailService.selectByPrimaryKey(id));
    }

    @Operation(summary = "邮件查询所有",description = "查询所有邮箱的操作")
    @GetMapping("/all")
    public ResultData all(){
        return decide(sysEmailService.all());
    }
}
