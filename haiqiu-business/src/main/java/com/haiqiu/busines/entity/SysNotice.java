package com.haiqiu.busines.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * sys_notice
 * @author 
 */
@Schema(description="通知通告")
@Data
public class SysNotice implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Long id;

    /**
     * 公告标题
     */
    @Schema(description="公告标题",required = true)
    private String title;

    /**
     * 公告内容
     */
    @Schema(description="公告内容",required = true)
    private String content;

    /**
     * 排序
     */
    @Schema(description="排序")
    private Integer sort;

    /**
     * 发布者
     */
    @Schema(description="发布者",hidden = true)
    private String username;

    /**
     * 阅读量
     */
    @Schema(description="阅读量",hidden = true)
    private Long view;

    /**
     * 封面图片
     */
    @Schema(description="封面图片")
    private String cover;

    /**
     * 内容类型（0公告，1通知，2日志）
     */
    @Schema(description="内容类型（0公告，1通知，2日志）")
    private Integer type;

    /**
     * 状态：（0关闭，1正常）
     */
    @Schema(description="状态：（0关闭，1正常）")
    private Integer status;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}