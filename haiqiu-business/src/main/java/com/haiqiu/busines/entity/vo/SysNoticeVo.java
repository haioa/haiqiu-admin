package com.haiqiu.busines.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.haiqiu.busines.entity.SysNotice;

import java.io.Serializable;

/**
 * sys_notice
 * @author 
 */
@Schema
@Data
public class SysNoticeVo extends SysNotice implements Serializable {


    /**
     * 发布者
     */
    @Schema(description="发布者",hidden = true)
    private String author;



    /**
     * 统计数量
     */
    @Schema(description="统计数量",hidden = true)
    private Long count;

    /**
     * 类型名称
     */
    @Schema(description="类型名称",hidden = true)
    private String typeName;

    private static final long serialVersionUID = 1L;
}