package com.haiqiu.busines.entity;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import org.springframework.data.annotation.Transient;

/**
 * file
 *
 * @author
 */
@Schema(description = "服务器文件")
@Data
public class SysFile implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description = "主键ID")
    private Long id;

    /**
     * 文件名称
     */
    @Schema(description = "文件名称")
    private String title;

    /**
     * 文件地址
     */
    @Schema(description = "文件地址",hidden = true)
    private String path;

    /**
     * 文件类型
     */
    @Schema(description = "文件类型",hidden = true)
    private String type;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer sort;

    /**
     * 文件大小
     */
    @Schema(description = "文件大小",hidden = true)
    private String size;

    /**
     * 文件标识
     */
    @Schema(description = "文件标识")
    private String fileFlag;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间",hidden = true)
    private Date updateTime;

    /**
     * 文件ftp文件路径
     */
    @Schema(description = "文件ftp文件路径")
    @Transient
    private String folder;

    /**
     * 是否保存到数据库，true保存，false不保存
     */
    @Schema(description = "是否保存到数据库，true保存，false不保存")
    @Transient
    private Boolean isSave;

    private static final long serialVersionUID = 1L;



}