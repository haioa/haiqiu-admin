package com.haiqiu.busines.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * email
 * @author 
 */
@Schema(description="邮件")
@Data
public class SysEmail implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Long id;

    /**
     * 主题
     */
    @Schema(description="主题")
    @NotEmpty(message = "主题不能为空")
    @Length(min = 1,max = 50,message = "主题字符长度应该在1-50之间")
    private String subject;

    /**
     * 内容
     */
    @Schema(description="内容")
    @NotEmpty(message = "内容不能为空")
    private String content;

    /**
     * 收件人【数组多个】
     */
    @Schema(description="收件人【数组多个】")
    @NotEmpty(message = "收件人不能为空")
    private String toWho;

    /**
     * 抄送人【数组多个】
     */
    @Schema(description="抄送人【数组多个】")
    private String ccPeoples;

    /**
     * 密送人【数组多个】
     */
    @Schema(description="密送人【数组多个】")
    private String bccPeoples;

    /**
     * 附件
     */
    @Schema(description="附件")
    private String attachments;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    /**
     * 0存储草稿，1准备发送，2发送失败，3发送成功
     */
    @Schema(description="0存储草稿，1准备发送，2发送失败，3发送成功")
    @NotNull(message = "状态不能为空")
    private Integer status;

    /**
     * 发送邮件类型：text发送文本，html发送Html
     */
    @Schema(description="发送邮件类型：text发送文本，html发送Html")
    @NotEmpty(message = "邮件类型不能为空")
    private String sendType;

    private static final long serialVersionUID = 1L;
}