package com.haiqiu.busines.mapper;

import com.haiqiu.busines.entity.SysFile;
import com.haiqiu.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * FileMaper继承基类
 * @author HaiQiu
 */
@Mapper
public interface FileMaper extends BaseMapper<SysFile, Long> {
    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 成功条数
     */
    int batchDel(Set<Long> ids);

    /**
     * 模糊分页总数
     * @param sysFile 实体
     * @return 总数
     */
    long count(@Param("file") SysFile sysFile);

    /**
     * 模糊分页
     * @param sysFile 实体
     * @param offset 页数
     * @param pageSize 每页大小
     * @return 数据集合
     */
    List<SysFile> page(@Param("file") SysFile sysFile, @Param("offset")  long offset, @Param("pageSize")  Long pageSize);

    /**
     * 查询所有
     * @return 数据集合
     */
    List<SysFile> all();

    /**
     * 查询所有文件标签
     * @param type 类型
     * @return  标签
     */
    List<Map<String,String>> allFlag(String type);

}