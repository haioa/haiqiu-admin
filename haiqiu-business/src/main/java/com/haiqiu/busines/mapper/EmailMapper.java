package com.haiqiu.busines.mapper;

import com.haiqiu.busines.entity.SysEmail;
import com.haiqiu.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * EmailMapper继承基类
 * @author HaiQiu
 */
@Mapper
public interface EmailMapper extends BaseMapper<SysEmail, Long> {
    /**
     * 查询所有
     *
     * @return 返回所有
     */
    List<SysEmail> all();

    /**
     * 批量删除
     *
     * @param ids 数据ID集合
     * @return 删除成功条数
     */
    int batchDel(Set<Long> ids);

    /**
     * 模糊分页总条数
     *
     * @param sysEmail 查询实体
     * @return 总条数
     */
    long count(@Param("email") SysEmail sysEmail);

    /**
     * 模糊分页
     *
     * @param sysEmail 查询参数
     * @param offset   页数
     * @param pageSize 每页数量
     * @return 分页数据
     */
    List<SysEmail> page(@Param("email") SysEmail sysEmail, @Param("offset") long offset, @Param("pageSize") Long pageSize);
}