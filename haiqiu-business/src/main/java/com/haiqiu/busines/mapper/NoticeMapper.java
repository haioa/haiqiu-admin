package com.haiqiu.busines.mapper;


import com.haiqiu.busines.entity.SysNotice;
import com.haiqiu.busines.entity.vo.SysNoticeVo;
import com.haiqiu.common.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * SysNoticeMapper继承基类
 * @author HaiQiu
 */
@Repository
public interface NoticeMapper extends BaseMapper<SysNotice,Long> {

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 成功条数
     */
    int delBatch(Set<Long> ids);


    /**
     * 模糊分页总数
     * @param sysNotice 模糊条件
     * @return 总数
     */
    long count(@Param("sys_notice") SysNotice sysNotice);


    /**
     * 模糊分页
     * @param sysNotice 模糊条件
     * @param offset 页数
     * @param pageSize 每页大小
     * @return 数据集合
     */
    List<SysNotice> page(@Param("sys_notice") SysNotice sysNotice, @Param("offset") long offset, @Param("pageSize") Long pageSize);

    /**
     * 所有公告
     * @return 全部数据集合
     */
    List<SysNotice> all();

    /**
     * 公告新增浏览量
     * @param id 数据ID
     */
    int viewAdd(Long id);

    /**
     * 统计各类发布数量
     * @return 数据
     */
    List<SysNoticeVo> statistics();
}