package com.haiqiu.busines.service;


import com.haiqiu.busines.entity.SysFile;
import com.haiqiu.common.entity.FileServer;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysFileService {
    /**
     * 删除
     *
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增
     *
     * @param sysFile 邮箱实体
     */
    void insert(SysFile sysFile);

    /**
     * 查询单条
     *
     * @param id
     * @return
     */
    SysFile selectByPrimaryKey(Long id);

    /**
     * 修改
     *
     * @param sysFile 邮箱实体
     */
    void updateByPrimaryKeySelective(SysFile sysFile);

    /**
     * 批量删除
     *
     * @param ids 数据ID集合
     * @return 返回删除成功的条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页
     *
     * @param request 查询实体（包含分页）
     * @return 返回数据集合
     */
    PageResponse<SysFile> list(PageRequest<SysFile> request);

    /**
     * 所有邮箱
     *
     * @return 所有邮箱
     */
    List<SysFile> all();

    /**
     * 批量文件上传
     * @param files 批量文件
     * @param mode 上传文件存储方式：oss,ftp,server,qiniu,ftp
     * @param folder 文件夹
     * @return 成功返回文件的url地址
     */
    String  upload(MultipartFile files, String mode, String folder);

    /**
     * 批量ftp文件上传
     * @param files 批量文件
     * @param sysFileModel 文件信息
     * @return 成功返回文件的url地址
     */
    List<String> uploadFile(List<MultipartFile> files, SysFile sysFileModel);

    /**
     * 文件流下载
     * @param filePath 文件url地址
     */
    void downloadFile(String filePath);

    List<FTPFile> show(String folder);

    /**
     * ftp资源文件上传
     *
     * @param file       文件
     * @param sysFileEntity 信息实体
     */
    void uploadResourceFile(MultipartFile file, SysFile sysFileEntity);

    /**
     * 查询所有图片标签
     * @param type 标签
     * @return 查询所有图片标签
     */
    List<Map<String,String>> allFlag(String type);


    /**
     * 批量服务器文件上传
     * @param files 批量文件
     * @param sysFileModel 文件信息
     * @return 成功返回文件的url地址
     */
    List<String> serverUpload(List<MultipartFile> files, SysFile sysFileModel);

    /**
     * 删除文件
     * @param fileUrl 文件地址
     */
    void delFile(String fileUrl);

    /**
     * 系统背景图logo文件上传
     * @param file 文件
     * @return 文件地址
     */
    String uploadTheme(MultipartFile file);


    /**
     * 服务器文件列表
     * @param folder 路径
     * @param name 文件或者文件夹搜索名称
     * @param sort 排序
     * @return 文件列表集合
     */
    List<FileServer> serverFileList(String folder, String name, String sort);

    /**
     * 批量删除服务器文件或文件夹
     * @param filePaths 文件路径
     * @return 成功条数
     */
    int deleteFiles(Set<String> filePaths);

    /**
     * 创建文件或文件夹
     * @param folders 文件夹
     */
    void addFolder(String folders);

    /**
     * 服务器文件或文件夹重命名
     * @param filePath 源文件路径
     * @param newName 新文件名称或直接旧文件名称
     * @param filePath 路径
     */
    void serverRename(String filePath, String newName, String parentPath);

    /**
     * 服务器复制文件或文件夹
     * @param filePath 源文件路径
     * @param newName 新文件名称或直接旧文件名称
     * @param filePath 复制到哪儿，父级路径
     */
    void serverCopy(String filePath, String newName, String parentPath);
}