package com.haiqiu.busines.service.impl;

import com.haiqiu.busines.entity.SysNotice;
import com.haiqiu.busines.entity.vo.SysNoticeVo;
import com.haiqiu.busines.mapper.NoticeMapper;
import com.haiqiu.busines.service.SysNoticeService;
import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.web.ServletUtil;
import com.haiqiu.common.utils.tools.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author: HaiQiu
 * @Date: 2021/4/6
 * @Description: 公告逻辑层
 */
@Service
public class SysNoticeServiceImpl implements SysNoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    @Autowired
    private ServletUtil servletUtil;

    /**
     * 根据ID删除数据
     *
     * @param id 数据ID
     */
    @Override
    public void deleteByPrimaryKey(Long id) {
        if (StringUtils.isEmpty(id)) {
            throw new BaseException(Constants.ID_EXITS);
        }
        if (noticeMapper.deleteByPrimaryKey(id) == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
    }


    /**
     * 新增
     *
     * @param sysNotice 公告实体
     */
    @Override
    public void insert(SysNotice sysNotice) {
        SysNotice notice = checkAddData(sysNotice);
        Date date = new Date();
        notice.setCreateTime(date);
        notice.setUpdateTime(date);
        notice.setUsername(servletUtil.getUserName());
        notice.setView(Constants.DEFAULT_VIEW);
        if (noticeMapper.insert(notice) == 0) {
            throw new BaseException(Constants.FAIL_ADD);
        }
    }

    /**
     * 检查数据
     *
     * @param sysNotice 公告实体
     * @return 返回检验后的实体
     */
    private SysNotice checkAddData(SysNotice sysNotice) {
        if (StringUtils.isEmpty(sysNotice)) {
            throw new BaseException("无效数据");
        }
        if (StringUtils.isEmpty(sysNotice.getContent())) {
            throw new BaseException("无效公告内容");
        }
        if (StringUtils.isEmpty(sysNotice.getTitle())) {
            throw new BaseException("无效公告名称");
        }
        return sysNotice;
    }


    /**
     * 根据ID查询
     *
     * @param id 数据ID
     * @return 公告实体
     */
    @Override
    public SysNotice selectByPrimaryKey(Long id) {
        SysNotice sysNotice = noticeMapper.selectByPrimaryKey(id);
        if (sysNotice == null) {
            throw new BaseException(Constants.DATA_EXIT);
        }
        if (sysNotice.getContent().contains("<img")){
            sysNotice.setCover(StringUtil.getImgStr(sysNotice.getContent()).get(0));
        }
        return sysNotice;
    }


    /**
     * 修改公告
     *
     * @param sysNotice 公告实体
     */
    @Override
    public void updateByPrimaryKeySelective(SysNotice sysNotice) {
        //检查数据
        SysNotice notice = checkAddData(sysNotice);
        notice.setUpdateTime(new Date());
        if (noticeMapper.updateByPrimaryKeySelective(notice) == 0) {
            throw new BaseException(Constants.FAIL_UPDATE);
        }
    }


    /**
     * 批量删除公告
     *
     * @param ids 数据ID集合
     * @return 成功条数
     */
    @Override
    public int delBatch(Set<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new BaseException(Constants.ID_EXITS);
        }
        int i = noticeMapper.delBatch(ids);
        if (i == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
        return i;
    }


    /**
     * 分页模糊查询
     *
     * @param request 查询实体（包含分页）
     * @return 返回分页数据
     */
    @Override
    public PageResponse<SysNotice> list(PageRequest<SysNotice> request) {
        SysNotice sysNotice = StringUtil.checkObjFieldIsNull(request.getParams());
        long count = noticeMapper.count(sysNotice);
        PageResponse<SysNotice> response = new PageResponse<>();
        response.setPageSize(request.getPageSize());
        response.setTotal(count);
        response.setPageIndex(request.getPageIndex());
        if (count > 0) {
            List<SysNotice> list = noticeMapper.page(sysNotice, request.getOffset(), request.getPageSize());
            for (SysNotice notice : list) {
                if (notice.getContent().contains("<img")){
                    notice.setCover(StringUtil.getImgStr(notice.getContent()).get(0));
                }
            }
            response.setData(list);
        }
        return response;
    }


    /**
     * 查询所有公告
     *
     * @return 返回所有公告实体
     */
    @Override
    public List<SysNotice> all() {
        return noticeMapper.all();
    }


    /**
     * 公告新增浏览量
     *
     * @param id 数据ID
     */
    @Override
    public void viewAdd(Long id) {
        noticeMapper.viewAdd(id);
    }

    @Override
    public List<SysNoticeVo> statistics() {
        List<SysNoticeVo> statistics = noticeMapper.statistics();
        for (SysNoticeVo statistic : statistics) {
            if (statistic.getType().equals(0)) {
                statistic.setTypeName("公告");
            } else if (statistic.getType().equals(1)) {
                statistic.setTypeName("通知");
            } else if (statistic.getType().equals(2)) {
                statistic.setTypeName("日志");
            }
        }
        return statistics;
    }
}
