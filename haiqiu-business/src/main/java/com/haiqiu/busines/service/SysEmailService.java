package com.haiqiu.busines.service;



import com.haiqiu.busines.entity.SysEmail;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysEmailService {
    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增
     * @param sysEmail 邮箱实体
     */
    void insert(SysEmail sysEmail);

    /**
     * 查询单条
     * @param id
     * @return
     */
    SysEmail selectByPrimaryKey(Long id);

    /**
     * 修改
     * @param sysEmail 邮箱实体
     */
    void updateByPrimaryKeySelective(SysEmail sysEmail);

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 返回删除成功的条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页
     * @param request 查询实体（包含分页）
     * @return 返回数据集合
     */
    PageResponse<SysEmail> list(PageRequest<SysEmail> request);

    /**
     * 所有邮箱
     * @return 所有邮箱
     */
    List<SysEmail> all();

}