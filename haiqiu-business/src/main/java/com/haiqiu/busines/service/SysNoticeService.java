package com.haiqiu.busines.service;



import com.haiqiu.busines.entity.SysNotice;
import com.haiqiu.busines.entity.vo.SysNoticeVo;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysNoticeService {
    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增
     * @param sysNotice 公告实体
     */
    void insert(SysNotice sysNotice);

    /**
     * 查询单条
     * @param id
     * @return
     */
    SysNotice selectByPrimaryKey(Long id);

    /**
     * 修改
     * @param sysNotice 公告实体
     */
    void updateByPrimaryKeySelective(SysNotice sysNotice);

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 返回删除成功的条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页
     * @param request 查询实体（包含分页）
     * @return 返回数据集合
     */
    PageResponse<SysNotice> list(PageRequest<SysNotice> request);

    /**
     * 所有公告
     * @return 所有公告
     */
    List<SysNotice> all();

    /**
     * 公告新增浏览量
     * @param id 数据ID
     */
    void viewAdd(Long id);


    /**
     * 统计各类发布数量
     * @return 数据
     */
    List<SysNoticeVo>  statistics();
}