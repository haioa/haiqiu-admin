package com.haiqiu.busines.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.haiqiu.common.entity.WebSockMsg;
import com.haiqiu.common.system.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author HaiQiu
 * @date 2021/10/29
 * @desc 定时任务
 */
@Configuration
@EnableScheduling
public class TimedTaskService {

    @Autowired
    private WebSocketServer webSocketServer;
    //  定时发送服务器状态信息
    @Scheduled(cron = "0/5 * * * * ?")
    public void getSystemInfo() throws Exception {
        Server server = new Server();
        server.copyTo();
        WebSockMsg msg = new WebSockMsg();
        msg.setFrom("system");
        msg.setExtras(server);
        webSocketServer.sendMsgAll(JSONObject.toJSONString(msg));
    }
}
