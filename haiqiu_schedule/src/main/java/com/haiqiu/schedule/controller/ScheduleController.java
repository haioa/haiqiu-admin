//package com.haiqiu.schedule.controller;
//
//import com.haiqiu.common.controller.BaseController;
//import com.haiqiu.common.result.Constants;
//import com.haiqiu.common.result.ResultData;
//import com.haiqiu.schedule.entity.SysSchedule;
//import com.haiqiu.schedule.service.ScheduledOfTask;
//import com.haiqiu.schedule.service.SysScheduleService;
//import io.swagger.v3.oas.annotations.media.Schema;
//import io.swagger.annotations.Operation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Date;
//
///**
// * @author HaiQiu
// * @date 2022/3/14
// */
//@RestController
//@Api(tags = "定时任务接口")
//@RequestMapping("/schedule")
//public class ScheduleController extends BaseController implements ScheduledOfTask {
//
//    @Autowired
//    private SysScheduleService sysScheduleService;
//
//    @Operation("新增定时任务")
//    @PostMapping()
//    public ResultData insert(SysSchedule sysSchedule){
//        sysScheduleService.insert(sysSchedule);
//        return decide(Constants.SUCCESS_ADD);
//    }
//
//    @Override
//    public void execute() {
//        System.out.println("测试定时任务:"+ new Date());
//    }
//}
//
