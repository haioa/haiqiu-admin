package com.haiqiu.schedule.service.impl;


import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.result.Constants;
import com.haiqiu.schedule.entity.SysSchedule;
import com.haiqiu.schedule.mapper.SysScheduleMapper;
import com.haiqiu.schedule.service.SysScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author HaiQiu
 * @date 2022/3/14
 */
@Service
public class SysScheduleServiceImpl implements SysScheduleService {

    @Autowired
    private SysScheduleMapper sysScheduleMapper;

    @Override
    public void insert(SysSchedule sysSchedule) {
        if (sysScheduleMapper.insert(sysSchedule)==0){
            throw new BaseException(Constants.FAIL_ADD);
        }
    }
}
