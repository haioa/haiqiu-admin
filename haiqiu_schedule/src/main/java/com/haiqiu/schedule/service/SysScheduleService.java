package com.haiqiu.schedule.service;

import com.haiqiu.schedule.entity.SysSchedule;

/**
 * @author HaiQiu
 * @date 2022/3/14
 */
public interface SysScheduleService {

    void insert(SysSchedule sysSchedule);
}
