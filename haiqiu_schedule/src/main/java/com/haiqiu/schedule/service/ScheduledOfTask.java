package com.haiqiu.schedule.service;

import com.haiqiu.common.utils.web.SpringUtil;
import com.haiqiu.schedule.entity.SysSchedule;
import com.haiqiu.schedule.mapper.SysScheduleMapper;

public interface ScheduledOfTask extends Runnable {
    /**
     * 定时任务方法
     */
    void execute();
    /**
     * 实现控制定时任务启用或禁用的功能
     */
    @Override
    default void run() {
        SysScheduleMapper repository = SpringUtil.getBean(SysScheduleMapper.class);
        SysSchedule scheduledCron = repository.findByCronKey(this.getClass().getName());
        if (!scheduledCron.getStatus()) {
            // 任务是禁用状态
            return;
        }
        execute();
    }
}