//package com.haiqiu.schedule.config;
//
//import com.haiqiu.common.exception.BaseException;
//import com.haiqiu.schedule.entity.SysSchedule;
//import com.haiqiu.schedule.mapper.SysScheduleMapper;
//import io.jsonwebtoken.lang.Assert;
//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//import org.springframework.scheduling.support.CronTrigger;
//import org.springframework.util.CollectionUtils;
//
//import java.util.List;
//import java.util.concurrent.Executor;
//import java.util.concurrent.Executors;
//
///**
// * @author HaiQiu
// * @date 2022/3/14
// */
//@Configuration
//public class ScheduleConfig implements SchedulingConfigurer {
//
//    @Autowired
//    private ApplicationContext context;
//
//    @Autowired
//    private SysScheduleMapper sysScheduleMapper;
//
//
//    @Override
//    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//        List<SysSchedule> schedules = sysScheduleMapper.findAll();
//        if (!CollectionUtils.isEmpty(schedules)){
//            for (SysSchedule schedule : schedules) {
//                Class<?> clazz;
//                Object task;
//                try {
//                    clazz = Class.forName(schedule.getCronKey());
//                    task = context.getBean(clazz);
//                } catch (ClassNotFoundException e) {
//                    throw new IllegalArgumentException("spring_scheduled_cron表数据" + schedule.getCronKey() + "有误", e);
//                } catch (BeansException e) {
//                    throw new IllegalArgumentException(schedule.getCronKey() + "未纳入到spring管理", e);
//                }
//                // 可以通过改变数据库数据进而实现动态改变执行周期
//                taskRegistrar.addTriggerTask(((Runnable) task),
//                        triggerContext -> {
//                            String cronExpression = schedule.getCornExpression();
//                            return new CronTrigger(cronExpression).nextExecutionTime(triggerContext);
//                        }
//                );
//
//            }
//        }
//    }
//
//    @Bean
//    public Executor taskExecutor() {
//        return Executors.newScheduledThreadPool(10);
//    }
//}
