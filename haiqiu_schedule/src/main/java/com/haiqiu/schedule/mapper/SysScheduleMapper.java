package com.haiqiu.schedule.mapper;

import com.haiqiu.schedule.entity.SysSchedule;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author HaiQiu
 * @date 2022/3/14
 */
@Repository
public interface SysScheduleMapper {

    /**
     * 新增定时任务
     * @param sysSchedule 新增实体
     * @return 新增的ID返回
     */
    Long insert(SysSchedule sysSchedule);

    /**
     * 查询所有定时任务
     * @return 定时任务
     */
    List<SysSchedule> findAll();

    SysSchedule findByCronKey(String cronkey);
}
