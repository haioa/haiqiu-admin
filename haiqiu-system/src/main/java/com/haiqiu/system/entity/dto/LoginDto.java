package com.haiqiu.system.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author HaiQiu
 * @date 2021/4/3 23:53
 * @desc
 **/
@Schema(description = "后台登录实体类")
@Data
public class LoginDto implements Serializable {

    /**
     * 账户：用户名登录
     */
    @Schema(description="账户：用户名登录",required = true)
    private String username;

    /**
     * 用户密码
     */
    @Schema(description="用户密码",required = true)
    private String password;


    /**
     * 验证码
     */
    @Schema(description="验证码",required = true)
    private String captcha;

    /**
     * 验证码key
     */
    @Schema(description="验证码key",required = true)
    private String key;

    private static final long serialVersionUID = 1L;

}
