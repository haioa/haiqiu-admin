package com.haiqiu.system.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * sys_permission
 *
 * @author haiqiu
 */
@Schema(description = "菜单权限")
@Data
public class SysPermission implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description = "主键ID")
    private Long id;

    /**
     * 父级ID
     */
    @Schema(description = "父级ID")
    private Long parentId;

    /**
     * 权限名称（菜单名称）
     */
    @Schema(description = "权限名称（菜单名称）")
    @NotEmpty(message = "菜单名称不能为空")
    private String title;

    /**
     * 权限值
     */
    @Schema(description = "权限值")
    @NotEmpty(message = "权限字符不能为空")
    private String value;

    /**
     * 权限图标
     */
    @Schema(description = "权限图标")
    @NotEmpty(message = "图标不能为空")
    private String icon;

    /**
     * 权限链接
     */
    @Schema(description = "路由地址")
    @NotEmpty(message = "路由地址不能为空")
    private String path;

    /**
     * 是否开启（true激活，false禁用）
     */
    @Schema(description = "是否开启（true激活，false禁用）")
    @NotNull(message = "激活状态不能为空")
    private Boolean active;

    /**
     * 权限描述
     */
    @Schema(description = "权限描述")
    @NotEmpty(message = "权限描述不能为空")
    private String desc;

    /**
     * 请求方法
     */
    @Schema(description = "请求方法")
    @NotEmpty(message = "请求方法不能为空")
    private String method;

    /**
     * 类型：0目录。1菜单，2按钮
     */
    @Schema(description = "类型：0目录。1菜单，2按钮")
    @NotNull(message = "类型不能为空")
    private Byte type;

    /**
     * 排序
     */
    @Schema(description = "排序")
    @NotNull(message = "排序不能为空")
    private Short sort;

    /**
     * 路由组件
     */
    @Schema(description = "路由组件")
    @NotEmpty(message = "路由组件不能为空")
    private String component;

    /**
     * 是否为外链（true是 false否）
     */
    @Schema(description = "是否为外链（true是 false否）")
    @NotNull(message = "是否为外链不能为空")
    private Boolean isFrame;

    /**
     * 是否缓存（true缓存 false不缓存）
     */
    @Schema(description = "是否缓存（true缓存 false不缓存）")
    @NotNull(message = "是否缓存不能为空")
    private Boolean isCache;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间", hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间", hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;

}