package com.haiqiu.system.entity.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.haiqiu.system.entity.*;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * description: UserVo类
 * date: 2021/4/7 8:14 下午
 * @author haiqiu
 */
@Schema(description = "系统用户Vo类")
@Data
@ExcelTarget("userVo")
public class SysUserVo extends SysUser {
    /**
     * 角色集合标识
     */
    @Schema(description="角色集合标识")
    private List<String> roles;

    /**
     * 用户角色列表
     */
    @Schema(description="用户角色列表")
    @Transient
    private List<SysRole> roleList;

    /**
     * 用户职位列表
     */
    @Schema(description="用户职位列表")
    @Transient
    private List<SysPosition> positionList;

    /**
     * 部门
     */
    @Schema(description="部门")
    private SysDept dept;

    /**
     * 部门名称
     */
    @Excel(name = "部门",orderNum = "8")
    @Schema(description="部门名称")
    private String deptName;

    /**
     * 角色ID
     */
    @Schema(description="角色ID")
    @NotNull(message = "没有选择角色")
    private List<Long> roleIds;

    /**
     * 职位ID
     */
    @Schema(description="职位ID")
    @NotNull(message = "没有选择职位")
    private List<Long> positionIds;

}
