package com.haiqiu.system.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author HaiQiu
 * @ClassName UserPwd.java
 * @Description 用户重置密码实体
 * @createTime 2021年09月12日
 */
@Schema(description = "用户重置密码实体")
@Data
public class UserPwd {

    @Schema(description = "用户ID(后台管理修改必传)")
    private Long userId;

    @Schema(description = "新密码",required = true)
    private String newPassword;

    @Schema(description = "旧密码(个人中心修改必传)")
    private String oldPassword;
}
