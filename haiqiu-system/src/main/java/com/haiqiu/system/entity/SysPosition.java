package com.haiqiu.system.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * sys_position
 * @author 
 */
@Schema(description="系统职位")
@Data
public class SysPosition implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Long id;

    /**
     * 岗位编码
     */
    @Schema(description="岗位编码")
    @NotNull(message = "岗位编码不能为空")
    @Length(min = 1,max = 80,message = "岗位编码字符长度应该在1-80之间")
    private String positionCode;

    /**
     * 职位名称
     */
    @Schema(description="职位名称")
    @NotEmpty(message = "职位名称不能为空")
    @Length(min = 1,max = 30,message = "职位名称字符长度应该在1-30之间")
    private String positionName;

    /**
     * 职位描述
     */
    @Schema(description="职位描述")
    @NotEmpty(message = "职位描述不能为空")
    @Length(min = 1,max = 200,message = "职位描述字符长度应该在1-200之间")
    private String describe;

    /**
     * 排序
     */
    @Schema(description="排序")
    @NotNull(message = "排序字段不能为空")
    private Integer sort;

    /**
     * 图标
     */
    @Schema(description="图标")
    private String icon;

    /**
     * 状态：（0禁用，1激活）
     */
    @Schema(description="状态：（0禁用，1激活）")
    @NotNull(message = "状态不能为空")
    private Byte status;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}