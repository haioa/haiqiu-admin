package com.haiqiu.system.entity.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.haiqiu.system.entity.SysRole;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author haiqiu
 */
@Data
public class SysRoleDto extends SysRole {

    /**
     * 角色的权限菜单ID集合
     */
    @Schema(description = "角色的权限菜单ID集合")
    @Size(min = 1, message = "未选中权限菜单")
    List<Long> menuIds;
}
