package com.haiqiu.system.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * sys_config
 * @author 
 */
@Schema(description="系统配置")
@Data
public class SysConfig implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Integer id;

    /**
     * 配置名称
     */
    @Schema(description="配置名称")
    @NotEmpty(message = "配置名称不能为空")
    @Length(min = 1,max = 15,message = "配置名称长度只能在1-15个字符")
    private String name;

    /**
     * 系统标题
     */
    @Schema(description="系统标题")
    @NotEmpty(message = "系统标题不能为空")
    @Length(min = 1,max = 15,message = "系统标题长度只能在1-15个字符")
    private String title;

    /**
     * 系统logo
     */
    @Schema(description="系统logo")
    private String logo;

    /**
     * 侧边栏主题 深色主题theme-dark，浅色主题theme-light
     */
    @Schema(description="侧边栏主题 深色主题theme-dark，浅色主题theme-light")
    @NotEmpty(message = "侧边栏主题不能为空")
    private String sideTheme;

    /**
     * 网站底部信息（授权信息）
     */
    @Schema(description="网站底部信息（授权信息）")
    @NotEmpty(message = "网站底部信息不能为空")
    @Length(min = 1,max = 200,message = "网站底部信息只能在1-200个字符")
    private String bottom;

    /**
     * 主题颜色
     */
    @Schema(description="主题颜色")
    @NotEmpty(message = "主题颜色不能为空")
    private String color;

    /**
     * 是否系统布局配置（true是，false否）
     */
    @Schema(description="是否系统布局配置（true是，false否）")
    @NotNull(message = "是否系统布局配置不能为空")
    private Boolean showSettings;

    /**
     * 是否显示顶部导航（true是，false否）
     */
    @Schema(description="是否显示顶部导航（true是，false否）")
    @NotNull(message = "是否显示顶部导航不能为空")
    private Boolean topNav;

    /**
     * 是否显示 tagsView（true是，false否）
     */
    @Schema(description="是否显示 tagsView（true是，false否）")
    @NotNull(message = "是否显示 tagsView不能为空")
    private Boolean tagsView;

    /**
     * 是否固定头部（true是，false否）
     */
    @Schema(description="是否固定头部（true是，false否）")
    @NotNull(message = "是否固定头部不能为空")
    private Boolean fixedHeader;

    /**
     * 是否显示logo（true是，false否）
     */
    @Schema(description="是否显示logo（true是，false否）")
    @NotNull(message = "是否显示logo不能为空")
    private Boolean sidebarLogo;

    /**
     * 是否显示动态标题（true是，false否）
     */
    @Schema(description="是否显示动态标题（true是，false否）")
    @NotNull(message = "是否显示动态标题不能为空")
    private Boolean dynamicTitle;

    /**
     * 是否多端登录（true是，false否）
     */
    @Schema(description="是否多端登录（true是，false否）")
    @NotNull(message = "是否多端登录不能为空")
    private Boolean manyLogin;

    /**
     * 状态（0禁用，1激活）
     */
    @Schema(description="状态（0禁用，1激活）")
    @NotNull(message = "状态配置不能为空")
    private Integer status;

    /**
     * 排序字段
     */
    @Schema(description="排序字段")
    @NotNull(message = "排序字段不能为空")
    private Integer sort;

    /**
     * 是否开放注册账号（true是，false否）
     */
    @Schema(description="是否开放注册账号（true是，false否）")
    @NotNull(message = "是否开放注册账号不能为空")
    private Boolean register;

    /**
     * 用户token令牌缓存时间
     */
    @Schema(description="用户token令牌缓存时间")
    @NotNull(message = "用户token令牌缓存时间不能为空")
    private Long tokenTime;

    /**
     * 登录页面背景图
     */
    @Schema(description="登录页面背景图")
    private String background;

    /**
     * 是否开启验证码登录（true是，false否）
     */
    @Schema(description="是否开启验证码登录（true是，false否）")
    @NotNull(message = "是否开启验证码登录不能为空")
    private Boolean captcha;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}