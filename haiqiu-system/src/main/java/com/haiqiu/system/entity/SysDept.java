package com.haiqiu.system.entity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * sys_dept
 * @author haiqiu
 */
@Schema(description="系统部门")
@Data
public class SysDept implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Long id;

    /**
     * 部门名称
     */
    @Schema(description="部门名称")
    @NotEmpty(message = "部门名称不能为空")
    @Length(min = 1,max = 15,message = "文本长度1-15之间")
    private String deptName;

    /**
     * 部门封面
     */
    @Schema(description="部门封面")
    private String cover;

    /**
     * 父级ID
     */
    @Schema(description="父级ID")
    @NotNull(message = "父级部门未选择")
    private Long parentId;

    /**
     * 部门简介
     */
    @Schema(description="部门简介")
    @NotEmpty(message = "部门简介不能为空")
    @Length(min = 1,max = 200,message = "文本长度1-200之间")
    private String desc;

    /**
     * 部门负责人
     */
    @Schema(description="部门负责人")
    @NotEmpty(message = "部门负责人不能为空")
    @Length(min = 1,max = 50,message = "文本长度1-50之间")
    private String leader;

    /**
     * 部门联系电话
     */
    @Schema(description="部门联系电话")
    @NotEmpty(message = "部门电话不能为空")
    @Length(min = 1,max = 11,message = "文本长度1-11之间")
    private String phone;

    /**
     * 部门简介
     */
    @Schema(description="部门邮箱")
    @NotEmpty(message = "部门邮箱不能为空")
    @Length(min = 1,max = 50,message = "文本长度1-50之间")
    private String email;

    /**
     * 排序
     */
    @Schema(description="排序")
    @NotNull(message = "部门排序不能为空")
    private Short sort;

    /**
     * 是否激活（true开启，false关闭）
     */
    @Schema(description="是否激活（true开启，false关闭）")
    @NotNull(message = "状态不能为空")
    private Boolean status;

    /**
     * 逻辑删除（false未删除，true已删除）
     */
    @Schema(description="逻辑删除（false未删除，true已删除）")
    private Boolean isDelete;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}