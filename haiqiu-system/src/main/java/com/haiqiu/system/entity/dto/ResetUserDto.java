package com.haiqiu.system.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author HaiQiu
 * @date 2021/11/4
 * @desc 忘记密码重置账户密码
 */
@Schema(description = "忘记密码重置账户密码")
@Data
public class ResetUserDto {

    @Schema(description="邮箱",required = true)
    @NotEmpty(message = "邮箱不能为空")
    @Length(min = 1,max = 128,message = "邮箱长度应该在1-128个字符之间")
    private String email;

    @Schema(description="验证码",required = true)
    @NotEmpty(message = "验证码不能为空")
    @Length(min = 1,max = 128,message = "验证码长度应该在1-128个字符之间")
    private String captcha;

    /**
     * 用户密码
     */
    @Schema(description="用户密码",required = true)
    @NotEmpty(message = "密码不能为空")
    @Length(min = 1,max = 128,message = "密码长度应该在1-128个字符之间")
    private String password;

    /**
     * 再次输入密码
     */
    @Schema(description="再次输入密码",required = true)
    @NotEmpty(message = "确认密码不能为空")
    @Length(min = 1,max = 128,message = "确认密码长度应该在1-128个字符之间")
    private String confirmPassword;
}
