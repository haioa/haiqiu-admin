package com.haiqiu.system.entity;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * sys_log
 *
 * @author
 */
@Schema(description = "登录日志")
@Data
public class SysLoginLog implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description = "主键ID")
    private Long id;

    /**
     * 请求参数
     */
    @Schema(description = "请求参数")
    private String requestParam;

    /**
     * 响应参数
     */
    @Schema(description = "响应参数")
    private String responseJson;

    /**
     * 来源IP
     */
    @Schema(description = "来源IP")
    private String requestIp;

    /**
     * 来源地址
     */
    @Schema(description = "来源地址")
    private String requestAddress;

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    private String username;

    /**
     * 访问接口
     */
    @Schema(description = "访问接口")
    private String restUrl;

    /**
     * 接口返回消息
     */
    @Schema(description = "接口返回消息")
    private String result;

    /**
     * 客户端操作系统
     */
    @Schema(description = "客户端操作系统")
    private String os;

    /**
     * 客户端浏览器
     */
    @Schema(description = "客户端浏览器")
    private String browser;

    /**
     * 创建时间
     */
    @Schema(description = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description = "修改时间")
    private Date updateTime;

    /**
     * 查询时间范围参数
     */
    private String beginTime;
    private String endTime;

    private static final long serialVersionUID = 1L;
}