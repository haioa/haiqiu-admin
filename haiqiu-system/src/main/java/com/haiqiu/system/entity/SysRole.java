package com.haiqiu.system.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * sys_role
 * @author haiqiu
 */
@Schema(description="系统角色")
@Data
public class SysRole implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description="主键ID")
    private Long id;

    /**
     * 角色名称
     */
    @Schema(description="角色名称")
    @NotEmpty(message = "角色名称不能为空")
    private String name;

    /**
     * 标识符
     */
    @Schema(description="标识符")
    @NotEmpty(message = "角色标识符不能为空")
    private String sign;

    /**
     * 角色描述
     */
    @Schema(description="角色描述")
    @NotEmpty(message = "角色描述不能为空")
    private String desc;

    /**
     * 角色图标
     */
    @Schema(description="角色图标")
    private String icon;

    /**
     * 是否开启（true激活，false禁用）
     */
    @Schema(description="是否开启（true激活，false禁用）")
    @NotNull(message = "激活状态不能为空")
    private Boolean active;

    /**
     * 排序
     */
    @Schema(description="排序")
    @NotNull(message = "角色排序不能为空")
    private Short sort;

    /**
     * 逻辑删除（false未删除，true已删除）
     */
    @Schema(description="逻辑删除（false未删除，true已删除）")
    private Boolean del;

    /**
     * 创建时间
     */
    @Schema(description="创建时间",hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Schema(description="修改时间",hidden = true)
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}