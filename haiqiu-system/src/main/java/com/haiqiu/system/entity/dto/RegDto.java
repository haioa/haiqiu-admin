package com.haiqiu.system.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author HaiQiu
 * @date 2021/4/3 23:53
 * @desc
 **/
@Schema(description = "后台注册实体类")
@Data
public class RegDto implements Serializable {

    /**
     * 用户昵称
     */
    @Schema(description="用户昵称")
    private String nickname;

    /**
     * 用户名
     */
    @Schema(description="用户名",required = true)
    @NotEmpty(message = "用户名不能为空")
    @Length(min = 1,max = 20,message = "用户名长度应该在1-20个字符之间")
    private String username;

    /**
     * 用户密码
     */
    @Schema(description="用户密码",required = true)
    @NotEmpty(message = "密码不能为空")
    @Length(min = 1,max = 128,message = "密码长度应该在1-128个字符之间")
    private String password;

    /**
     * 再次输入密码
     */
    @Schema(description="再次输入密码",required = true)
    @NotEmpty(message = "确认密码不能为空")
    @Length(min = 1,max = 128,message = "确认密码长度应该在1-128个字符之间")
    private String confirmPassword;

    /**
     * 头像
     */
    @Schema(description="头像")
    private String avatar;

    /**
     * 邮箱
     */
    @Schema(description="邮箱")
    private String email;

    /**
     * 手机号
     */
    @Schema(description="手机号")
    private String phone;

    /**
     * 验证码
     */
    @Schema(description="验证码",required = true)
    @NotEmpty(message = "验证码不能为空")
    private String captcha;

    /**
     * 验证码key
     */
    @Schema(description="验证码key",required = true)
    @NotEmpty(message = "验证码key不能为空")
    private String key;

    private static final long serialVersionUID = 1L;


}
