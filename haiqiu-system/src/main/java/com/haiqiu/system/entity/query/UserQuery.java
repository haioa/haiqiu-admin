package com.haiqiu.system.entity.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Set;


/**
 * @author haiqiu
 * 全局查询实体
 */
@Data
@Schema(description = "用户全局查询")
public class UserQuery {

    /**
     * 角色ID
     */
    @Schema(description = "角色ID")
    private Long roleId;

    /**
     * 部门ID
     */
    @Schema(description = "部门ID")
    private Long deptId;

    /**
     * 查询参数：用户名
     */
    @Schema(description = "查询参数：用户名")
    private String username;

    /**
     * 查询参数：邮箱
     */
    @Schema(description = "查询参数：邮箱")
    private String email;

    /**
     * 查询参数：激活状态
     */
    @Schema(description = "查询参数：激活状态")
    private Boolean active;


    /**
     * 部门ID和子级ID集合（服务器查询使用，无前端交互）
     */
    @Schema(description = "部门ID和子级ID集合（服务器查询使用，无前端交互）",hidden = true)
    private Set<Long> deptIds;

    /**
     * 用户id集合
     */
    @Schema(description = "用户id集合")
    private Set<Long> userIds;

    /**
     * 查询时间范围参数
     */
    @Schema(description = "开始时间")
    private String beginTime;
    @Schema(description = "结束时间")
    private String endTime;

}
