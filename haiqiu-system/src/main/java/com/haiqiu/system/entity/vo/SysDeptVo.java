package com.haiqiu.system.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.haiqiu.system.entity.SysDept;

import java.util.List;

/**
 * @author HaiQiu
 * @date 2021/5/8 18:06
 * @desc
 **/
@Data
@Schema(description = "系统部门VO")
public class SysDeptVo extends SysDept {

    @Schema(description = "子部门集合")
    private List<SysDept> children;
}
