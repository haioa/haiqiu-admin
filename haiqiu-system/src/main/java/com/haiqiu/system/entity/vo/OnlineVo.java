package com.haiqiu.system.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author HaiQiu
 * @ClassName OnlineVo.java
 * @Description 在线用户
 * @createTime 2021年09月20日
 */
@Data
@Schema(description = "在线用户")
public class OnlineVo extends SysUserVo {


    /**
     * token
     */
    @Schema(description = "token")
    private String token;

    /**
     * 来源IP
     */
    @Schema(description = "来源IP")
    private String requestIp;

    /**
     * 来源地址
     */
    @Schema(description = "来源地址")
    private String requestAddress;

    /**
     * 访问接口
     */
    @Schema(description = "访问接口")
    private String restUrl;

    /**
     * 客户端操作系统
     */
    @Schema(description = "客户端操作系统")
    private String os;

    /**
     * 客户端浏览器
     */
    @Schema(description = "客户端浏览器")
    private String browser;

    /**
     * 在线状态
     */
    @Schema(description = "在线状态")
    private String onlineStatus;

}
