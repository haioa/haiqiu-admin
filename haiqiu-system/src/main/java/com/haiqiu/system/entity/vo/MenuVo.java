package com.haiqiu.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import com.haiqiu.system.entity.SysPermission;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * description: MenuVo 递归菜单
 * date: 2021/4/5 10:54 下午 <br>
 * author: haiqiu <br>
 * version: 1.0 <br>
 * @author haiqiu
 */
@Data
@Schema(description = "权限菜单VO")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MenuVo extends SysPermission {

    /**
     * 子菜单
     */
    @Schema(description = "子菜单集合",required = true)
    List<MenuVo> children = new ArrayList<>();

}
