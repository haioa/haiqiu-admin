package com.haiqiu.system.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import com.haiqiu.system.entity.SysPermission;
import com.haiqiu.system.entity.SysRole;

import java.util.List;

/**
 * @author haiqiu
 */
@Data
@Schema(description = "系统角色VO")
public class SysRoleVo extends SysRole {

    /**
     * 角色权限集合
     */
    List<SysPermission> sysPermissionList;


    //TODO 默认父子联动状态，暂时为真
    /**
     * 菜单父子联动
     */
    @Schema(description = "菜单父子联动")
    Boolean menuCheckStrictly = true;

    //TODO 默认父子联动状态，暂时为真
    /**
     * 部门父子联动
     */
    @Schema(description = "菜单父子联动")
     Boolean deptCheckStrictly = true;
}
