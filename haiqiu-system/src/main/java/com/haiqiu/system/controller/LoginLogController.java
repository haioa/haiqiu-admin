package com.haiqiu.system.controller;


import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysLoginLog;
import com.haiqiu.system.service.SysLoginLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author HaiQiu
 * @ClassName MailController.java
 * @Description 登录日志管理
 * @createTime 2021年08月04日
 */
@Tag(name = "登陆日志",description = "登录日志接口操作")
@RestController
@RequestMapping("/login/log")
public class LoginLogController extends BaseController {


    @Autowired
    private SysLoginLogService sysLoginLogService;


    @Operation(summary = "登录日志批量删除",description = "批量删除登录日志的操作")
    @DeleteMapping("")
    public ResultData delete(@Parameter(description = "日志ID，数组[]") @RequestBody Set<Long> ids){
        return decide(Constants.DELETE, sysLoginLogService.delete(ids));
    }

    @Operation(summary = "登录日志分页",description = "模糊分页登录日志的操作")
    @PostMapping("/list")
    public ResultData list(@Parameter(description = "模糊分页登录日志") @RequestBody PageRequest<SysLoginLog> request){
        return decide(sysLoginLogService.list(request));
    }


    @Operation(summary = "登录查询单条",description = "查询单条登录日志的操作")
    @GetMapping("")
    public ResultData get(@Parameter(description = "数据ID") @RequestParam Long id){
        return decide(sysLoginLogService.get(id));
    }

    @Operation(summary = "登录日志查询所有",description = "查询所有登录日志的操作")
    @GetMapping("/all")
    public ResultData all(){
        return decide(sysLoginLogService.all());
    }
}
