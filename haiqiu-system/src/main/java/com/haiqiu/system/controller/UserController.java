package com.haiqiu.system.controller;

import io.swagger.v3.oas.annotations.Parameter;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.syslog.annotation.NotLog;
import com.haiqiu.system.entity.SysUser;
import com.haiqiu.system.entity.query.UserQuery;
import com.haiqiu.system.entity.vo.UserPwd;
import com.haiqiu.system.entity.vo.SysUserVo;
import com.haiqiu.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

/**
 * @author HaiQiu
 * @date 2021/4/2 23:10
 * @desc
 **/
@Tag(name = "用户管理", description = "用户增删改查等方法")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {


    @Autowired
    private SysUserService sysUserService;


    @NotLog
    @Operation(description = "同步用户在线")
    @GetMapping("/online")
    public ResultData online() {
        sysUserService.online();
        return decide("同步成功");
    }

    @Operation(description = "强制用户下线")
    @DeleteMapping("/online")
    public ResultData forceLogout(@Parameter(description = "用户token") @RequestParam String token) {
        sysUserService.forceLogout(token);
        return decide("下线成功");
    }

    @Operation(description = "用户在线列表")
    @GetMapping("/online/list")
    public ResultData onlineList(@RequestParam(required = false) String userName, @RequestParam(required = false) String ipaddr) {
        return decide(sysUserService.onlineList(userName, ipaddr));
    }

    @Operation(description = "新增(用户)")
    @PostMapping
    public ResultData save(@RequestBody @Validated SysUserVo userVo) {
        sysUserService.save(userVo);
        return decide(Constants.SUCCESS_ADD);
    }

    @Operation(description = "个人中心头像上传)")
    @PostMapping("/profile/avatar")
    public ResultData avatarProfile(@RequestParam("avatarfile") MultipartFile file) {
        return decide((Object) sysUserService.avatar(file));
    }

    @Operation(description = "后台列表头像上传)")
    @PostMapping("/avatar")
    public ResultData avatarUpload(@RequestParam("avatarfile") MultipartFile file) {
        return decide((Object) sysUserService.avatarUpload(file));
    }


    @Operation(description = "修改(用户)")
    @PutMapping
    public ResultData update(@RequestBody @Validated SysUserVo userVo) {
        sysUserService.update(userVo);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "根据用户ID授权用户角色")
    @PutMapping("/authRole")
    public ResultData authRole(@Parameter(description = "用户ID") @RequestParam Long userId, @Parameter(description = "角色ID集合") @RequestParam List<Long> roleIds) {
        sysUserService.authRole(userId, roleIds);
        return decide("授权成功");
    }

    @Operation(description = "后台管理重置用户密码")
    @PutMapping("/resetPwd")
    public ResultData resetPwd(@RequestBody UserPwd userPwd) {
        sysUserService.resetPwd(userPwd);
        return decide(Constants.SUCCESS_RESET);
    }

    @Operation(description = "用户个人中心修改密码")
    @PutMapping("/profile/updatePwd")
    public ResultData updateUserPwd(@RequestBody UserPwd userPwd) {
        sysUserService.updateUserPwd(userPwd);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "用户个人中心修改信息")
    @PutMapping("/profile")
    public ResultData updateUserProfile(@Parameter(description = "头像") @RequestParam(required = false) String avatar, @Parameter(description = "昵称") @RequestParam(required = false) String nickname, @Parameter(description = "邮箱") @RequestParam(required = false) String email, @Parameter(description = "手机") @RequestParam(required = false) String phone, @Parameter(description = "性别") @RequestParam(required = false) Integer sex) {
        sysUserService.updateUserProfile(avatar, nickname, email, phone, sex);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "激活禁用(用户)")
    @PutMapping("/active")
    public ResultData active(@RequestBody SysUser sysUser) {
        return decide(sysUserService.active(sysUser));
    }


    @Operation(description = "批量删除(用户)")
    @DeleteMapping
    public ResultData delete(@RequestBody List<Long> ids) {
        return decide(Constants.DELETE, sysUserService.delete(ids));
    }

    @Operation(description = "获取用户信息")
    @GetMapping("/info")
    public ResultData info(@RequestParam(required = false) String token) {
        return decide(sysUserService.info(token));
    }


    @Operation(description = "ID获取用户信息")
    @GetMapping
    public ResultData get(@RequestParam Long id) {
        return decide(sysUserService.get(id));
    }


    @Operation(description = "模糊分页(用户)")
    @PostMapping("/list")
    public ResultData list(@RequestBody PageRequest<UserQuery> request) {
        return decide(sysUserService.list(request));
    }

    @Operation(description = "查询所有(用户)")
    @PostMapping("/all")
    public ResultData all() {
        return decide(sysUserService.all());
    }

    @Operation(description = "查询所有(用户昵称和用户名和邮箱)")
    @PostMapping("/emailAll")
    public ResultData emailAll() {
        return decide(sysUserService.emailAll());
    }

    @Operation(description = "导出用户数据")
    @GetMapping("/export")
    public ResultData export() throws IOException {
        sysUserService.export();
        return decide("导出成功");
    }


}
