package com.haiqiu.system.controller;

import io.swagger.v3.oas.annotations.Parameter;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysRole;
import com.haiqiu.system.entity.dto.SysRoleDto;
import com.haiqiu.system.entity.query.UserQuery;
import com.haiqiu.system.service.SysRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author HaiQiu
 * @date 2021/4/2 23:10
 * @desc
 **/
@Tag(name = "角色管理",description = "角色删除查询等方法")
@RestController
@RequestMapping("/role")
public class RoleController extends BaseController {


    @Autowired
    private SysRoleService sysRoleService;


    @Operation(description = "角色新增")
    @PostMapping
    public ResultData save(@RequestBody @Validated SysRoleDto roleDto){
        sysRoleService.save(roleDto);
        return decide(Constants.SUCCESS_ADD);
    }

    @Operation(description = "角色批量删除")
    @DeleteMapping
    public ResultData delete(@Parameter(description = "角色ID，数组[]")@RequestBody Set<Long> ids){
        return decide(Constants.DELETE, sysRoleService.delete(ids));
    }

    @Operation(description = "角色修改")
    @PutMapping
    public ResultData update(@RequestBody @Validated SysRoleDto roleDto){
        sysRoleService.update(roleDto);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "角色批量激活禁用")
    @PutMapping("/active")
    public ResultData active(@Parameter(description = "只需要提供角色id和激活参数即可") @RequestBody List<SysRoleDto> roleDtos){
        sysRoleService.active(roleDtos);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "角色根据ID查询")
    @GetMapping
    public ResultData get(@RequestParam Long id){
        return decide(sysRoleService.get(id));
    }

    @Operation(description = "角色模糊分页")
    @PostMapping("/list")
    public ResultData list(@RequestBody PageRequest<SysRole> request){
        return decide(sysRoleService.list(request));
    }

    @Operation(description = "根据角色ID查询角色已授权用户列表")
    @GetMapping("/allocatedList")
    public ResultData allocatedList(@RequestParam Long roleId,
                           @RequestParam Long pageIndex,
                           @RequestParam Long pageSize,
                           @RequestParam(required = false) String username,
                           @RequestParam(required = false) String phone,
                           @RequestParam(required = false) String email){
        return decide(sysRoleService.allocatedList(roleId,pageIndex,pageSize,username,phone,email));
    }

    @Operation(description = "根据角色ID查询角色未授权用户列表")
    @GetMapping("/unallocatedList")
    public ResultData unallocatedList(@RequestParam Long roleId,
                           @RequestParam Long pageIndex,
                           @RequestParam Long pageSize,
                           @RequestParam(required = false) String username,
                           @RequestParam(required = false) String phone,
                           @RequestParam(required = false) String email){
        return decide(sysRoleService.unallocatedList(roleId,pageIndex,pageSize,username,phone,email));
    }

    @Operation(description = "角色查询所有")
    @PostMapping("/all")
    public ResultData all(){
        return decide(sysRoleService.all());
    }

    @Operation(description = "批量取消或者批量添加用户授权角色")
    @PutMapping("/authorize")
    public ResultData authorize(@RequestBody UserQuery userQuery){
        return decide(String.format(Constants.SUCCESS_BATCH, sysRoleService.authorize(userQuery)));
    }
}
