package com.haiqiu.system.controller;

import com.haiqiu.syslog.annotation.Limit;

import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysConfig;
import com.haiqiu.system.service.SysConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author haiqiu
 */
@Tag(name = "系统配置",description = "系统配置主题样式，功能配置等")
@RestController
@RequestMapping("/system/config")
public class ConfigController extends BaseController {

    @Autowired
    private SysConfigService sysConfigService;

    @Limit(name = "系统配置请求",perSecond = 5,timeOut = 1)
    @Operation(description = "查询系统配置")
    @GetMapping("")
    public ResultData get(@Parameter(description = "系统配置ID") @RequestParam(required = false) Long id){
        // TODO 暂时不使用ID查询
        return decide(sysConfigService.get(Constants.CONFIG_ID));
    }

    @Operation(description = "修改系统配置")
    @PutMapping("")
    public ResultData update(@RequestBody @Validated SysConfig sysConfig){
        sysConfigService.update(sysConfig);
        return decide(Constants.SUCCESS_UPDATE);
    }

}
