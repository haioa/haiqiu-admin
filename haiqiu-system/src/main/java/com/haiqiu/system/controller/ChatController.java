package com.haiqiu.system.controller;

import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.service.ChatService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author HaiQiu
 * @date 2021/10/26
 * @desc 及时通讯
 */
@Tag(name = "聊天通讯管理", description = "聊天通讯管理")
@RestController
@RequestMapping("/chat")
public class ChatController extends BaseController {

    @Autowired
    private ChatService chatService;

    @Operation(description = "获取聊天用户列表")
    @PostMapping("/list")
    public ResultData list(){
        return decide(chatService.list());
    }
}
