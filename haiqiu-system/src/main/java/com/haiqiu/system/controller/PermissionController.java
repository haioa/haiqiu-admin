package com.haiqiu.system.controller;


import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysPermission;
import com.haiqiu.system.service.SysPermissionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author HaiQiu
 * @date 2021/4/2 23:10
 * @desc
 **/
@Tag(name = "菜单权限",description = "权限菜单等方法")
@RestController
@RequestMapping("/auth")
public class PermissionController extends BaseController {


    @Autowired
    private SysPermissionService sysPermissionService;


    @Operation(summary = "用户令牌解析获取菜单")
    @PostMapping(value = "/menu")
    public ResultData menu(){
        return decide(sysPermissionService.menu());
    }


    @Operation(summary = "菜单列表集合")
    @PostMapping( value = "/menu/list")
    public ResultData menuList(@RequestBody(required = false) SysPermission sysPermission){
        return decide(sysPermissionService.menuList(sysPermission));
    }

    @Operation(summary = "树形菜单权限列表")
    @PostMapping( value = "/tree")
    public ResultData tree(){
        return decide(sysPermissionService.tree());
    }


    @Operation(summary = "角色ID查询菜单ID集合与菜单下拉树结构")
    @GetMapping("/roleMenuTreeSelect")
    public ResultData roleMenuTreeSelect(@Parameter(description = "角色ID") @RequestParam Long roleId){
        return decide(sysPermissionService.roleMenuTreeSelect(roleId));
    }

    @Operation(summary = "菜单权限新增")
    @PostMapping
    public ResultData save(@RequestBody SysPermission sysPermission){
        sysPermissionService.save(sysPermission);
        return decide(Constants.SUCCESS_ADD);
    }

    @Operation(summary = "菜单权限批量删除")
    @DeleteMapping
    public ResultData delete(@Parameter(description = "批量ID,数组传body") @RequestBody Set<Long> ids){
        return decide(String.format("成功删除%d条数据", sysPermissionService.delete(ids)));
    }

    @Operation(summary = "菜单权限批量激活")
    @PutMapping("/active")
    public ResultData active(@Parameter(description = "批量ID,数组传body") @RequestBody List<SysPermission> sysPermissions){
        return decide(String.format("成功激活%d条数据", sysPermissionService.active(sysPermissions)));
    }

    @Operation(summary = "菜单权限修改")
    @PutMapping
    public ResultData update(@RequestBody SysPermission sysPermission){
        sysPermissionService.update(sysPermission);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(summary = "菜单权限查询单条")
    @GetMapping
    public ResultData get(Long id){
        return decide(sysPermissionService.get(id));
    }

    @Operation(summary = "菜单权限分页")
    @PostMapping("/list")
    public ResultData list(@RequestBody PageRequest<SysPermission> request){
        return decide(sysPermissionService.list(request));
    }

    @Operation(summary = "菜单权限树形分页")
    @PostMapping("tree/list")
    public ResultData treeList(@RequestBody PageRequest<SysPermission> request){
        return decide(sysPermissionService.treeList(request));
    }

    @Operation(summary = "菜单权限查询所有")
    @PostMapping("/all")
    public ResultData all(){
        return decide(sysPermissionService.all());
    }
}
