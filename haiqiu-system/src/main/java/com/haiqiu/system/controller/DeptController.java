package com.haiqiu.system.controller;


import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysDept;
import com.haiqiu.system.service.SysDeptService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
@Tag(name = "部门管理",description = "部门的增删改查")
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController {

    @Autowired
    private SysDeptService sysDeptService;

    @Operation(description = "所有部门")
    @GetMapping("/all")
    public ResultData all (@Parameter(description = "部门名称") @RequestParam(required = false) String deptName,
                           @Parameter(description = "激活状态：true激活，false禁用") @RequestParam(required = false) Boolean status){
        return decide(sysDeptService.all(deptName,status));
    }

    @Operation(description = "部门树形")
    @PostMapping("/tree")
    public ResultData tree (){
        return decide(sysDeptService.tree());
    }

    @Operation(description = "部门新增")
    @PostMapping
    public ResultData save (@RequestBody @Validated SysDept sysDept){
        sysDeptService.save(sysDept);
        return decide(Constants.SUCCESS_ADD);
    }

    @Operation(description = "部门查询")
    @GetMapping
    public ResultData get (@RequestParam Long deptId){
        return decide(sysDeptService.get(deptId));
    }


    @Operation(description = "部门批量删除")
    @DeleteMapping
    public ResultData delete (@RequestBody Set<Long> ids){
        return decide(Constants.DELETE, sysDeptService.delete(ids));
    }

    @Operation(description = "部门修改")
    @PutMapping
    public ResultData update (@RequestBody @Validated SysDept sysDept){
        sysDeptService.update(sysDept);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "部门批量激活")
    @PutMapping("/active")
    public ResultData active(@Parameter(description = "部门ID，数组[]") @RequestBody List<SysDept> sysDepts){
        return decide(Constants.ACTIVE, sysDeptService.active(sysDepts));
    }

    @Operation(description = "部门模糊分页")
    @PostMapping("/list")
    public ResultData list (@RequestBody PageRequest<SysDept> request){
        return decide(sysDeptService.list(request));
    }

    @Operation(description = "部门树形模糊分页")
    @PostMapping("/tree/list")
    public ResultData treeList (@RequestBody PageRequest<SysDept> request){
        return decide(sysDeptService.treeList(request));
    }
}
