package com.haiqiu.system.controller;

import com.haiqiu.syslog.annotation.Limit;
import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultCode;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.common.utils.web.CaptchaUtil;
import com.haiqiu.system.entity.SysConfig;
import com.haiqiu.system.entity.dto.LoginDto;
import com.haiqiu.system.entity.dto.RegDto;
import com.haiqiu.system.entity.dto.ResetUserDto;
import com.haiqiu.system.service.SysConfigService;
import com.haiqiu.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author HaiQiu
 * @date 2021/4/2 23:10
 * @desc
 **/
@Tag(name = "开放系统", description = "实现登录注册等方法")
@RestController
@RequestMapping("")
public class EntranceController extends BaseController {


    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private CaptchaUtil captchaUtil;

    @Autowired
    private SysConfigService sysConfigService;


    @Operation(summary = "用户登录", description = "用户登录，暂时只用用户名+密码方式")
    @PostMapping("/login")
    public ResultData login(@Parameter(description = "登录请求实体") @RequestBody LoginDto loginDto) {
        return ResultData.ok("登录成功", sysUserService.login(loginDto));
    }

    @Limit(name = "获取验证码",perSecond = 1,timeOut = 1)
    @Operation(summary = "获取验证码", description = "验证码获取刷新")
    @GetMapping("/captcha")
    public ResultData captcha() {
        return ResultData.ok(captchaUtil.generateCode());
    }


    @Operation(summary = "用户注册", description = "使用邮箱+密码方式注册")
    @PostMapping(value = "/register")
    public ResultData register(@Parameter(description = "注册实体类") @RequestBody @Validated RegDto regDto) {
        sysUserService.register(regDto);
        return decide(Constants.REGISTER);
    }

    @Operation(summary = "忘记密码验证",description = "忘记密码发送验证码")
    @GetMapping(value = "/forget/code")
    public ResultData forgetVerify(@Parameter(description = "邮箱") @RequestParam String email){
        sysUserService.forgetVerify(email);
        return decide("验证码发送到邮箱成功，请登录邮箱获取验证码");
    }

    @Operation(summary = "忘记密码重置", description = "忘记密码进行重置密码操作")
    @PostMapping(value = "/forget/reset")
    public ResultData reset(@Parameter(description = "重置实体类") @RequestBody @Validated ResetUserDto resetUserDto) {
        sysUserService.reset(resetUserDto);
        return decide("重置密码成功");
    }


    @Operation(summary = "退出登录", description = "账户退出登录")
    @PostMapping("/account/logout")
    public ResultData logout(@Parameter(description = "用户token") @RequestParam(required = false) String token) {
        sysUserService.logout(token);
        return ResultData.ok(ResultCode.ACCOUNT_CANCELLED);
    }

    @Operation(summary = "初始化系统参数", description = "获取初始化系统参数（系统名称，logo，登录背景图等信息）")
    @GetMapping(value = "/system/theme")
    public ResultData getConfigMsg() {
        SysConfig sysConfig = sysConfigService.get(Constants.CONFIG_ID);
        SysConfig sysConfigTheme = new SysConfig();
        sysConfigTheme.setBackground(sysConfig.getBackground());
        sysConfigTheme.setCaptcha(sysConfig.getCaptcha());
        sysConfigTheme.setRegister(sysConfig.getRegister());
        sysConfigTheme.setLogo(sysConfig.getLogo());
        sysConfigTheme.setTitle(sysConfig.getTitle());
        sysConfigTheme.setBottom(sysConfig.getBottom());
        return ResultData.ok(sysConfigTheme);
    }

}
