package com.haiqiu.system.controller;


import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.system.entity.SysPosition;
import com.haiqiu.system.service.SysPositionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author haiqiu
 */
@Tag(name = "岗位管理", description = "岗位职位的数据操作管理")
@RestController
@RequestMapping("/position")
public class PositionController extends BaseController {

    @Autowired
    private SysPositionService sysPositionService;

    @Operation(description = "职位分页")
    @PostMapping("/list")
    public ResultData list(@RequestBody PageRequest<SysPosition> request) {
        return decide(  sysPositionService.list(request));
    }


    @Operation(description = "新增职位")
    @PostMapping
    public ResultData save(@RequestBody @Validated SysPosition sysPosition) {
        sysPositionService.save(sysPosition);
        return decide(Constants.SUCCESS_ADD);
    }

    @Operation(description = "修改职位")
    @PutMapping
    public ResultData update(@RequestBody @Validated SysPosition sysPosition){
        sysPositionService.update(sysPosition);
        return decide(Constants.SUCCESS_UPDATE);
    }

    @Operation(description = "批量删除职位")
    @DeleteMapping
    public ResultData delete(@RequestBody Set<Long> ids){
        return decide(Constants.DELETE, sysPositionService.delete(ids));
    }

    @Operation(description = "查询职位")
    @GetMapping
    public ResultData get(@Parameter(description = "职位ID") @RequestParam Long id){
        return decide(sysPositionService.get(id));
    }

    @Operation(description = "查询全部职位")
    @GetMapping("/all")
    public ResultData all(@Parameter(description = "职位名称模糊匹配参数") @RequestParam(required = false) String positionName){
        return decide(sysPositionService.all(positionName));
    }
}
