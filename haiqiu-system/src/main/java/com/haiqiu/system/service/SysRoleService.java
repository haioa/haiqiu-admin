package com.haiqiu.system.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysRole;
import com.haiqiu.system.entity.dto.SysRoleDto;
import com.haiqiu.system.entity.query.UserQuery;
import com.haiqiu.system.entity.vo.SysRoleVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysRoleService {
    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增
     * @param roleDto 角色实体
     */
    void save(SysRoleDto roleDto);

    /**
     * 查询单条
     * @param id
     * @return
     */
    SysRoleVo get(Long id);

    /**
     * 修改
     * @param roleDto 角色实体
     */
    void update(SysRoleDto roleDto);

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 返回删除成功的条数
     */
    int delete(Set<Long> ids);

    /**
     * 模糊分页
     * @param request 查询实体（包含分页）
     * @return 返回数据集合
     */
    PageResponse<SysRole> list(PageRequest<SysRole> request);

    /**
     * 所有角色
     * @return 所有角色
     */
    List<SysRole> all();

    /**
     * 批量激活禁用
     * @param roleDtos 角色实体集合
     * @return 成功条数
     */
    int active(List<SysRoleDto> roleDtos);

    /**
     * 根据角色ID查询角色已授权用户列表
     * @param roleId 角色ID
     * @param pageIndex 页数
     * @param pageSize 每页数量
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 数据集合
     */
    Map<String,Object> allocatedList(Long roleId, Long pageIndex, Long pageSize, String username, String phone, String email);

    /**
     * 根据角色ID查询角色未授权用户列表
     * @param roleId 角色ID
     * @param pageIndex 页数
     * @param pageSize 每页数量
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 数据集合
     */
    Map<String,Object> unallocatedList(Long roleId, Long pageIndex, Long pageSize, String username, String phone, String email);

    /**
     * 批量取消或者批量添加用户授权角色
     * @param userQuery 查询实体
     */
    int authorize(UserQuery userQuery);
}