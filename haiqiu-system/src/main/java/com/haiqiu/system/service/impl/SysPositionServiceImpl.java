package com.haiqiu.system.service.impl;

import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.tools.StringUtil;
import com.haiqiu.system.entity.SysPosition;
import com.haiqiu.system.entity.SysUser;
import com.haiqiu.system.mapper.SysPositionMapper;
import com.haiqiu.system.mapper.SysUserMapper;
import com.haiqiu.system.service.SysPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author haiqiu
 */
@Service
public class SysPositionServiceImpl implements SysPositionService {

    @Autowired
    private SysPositionMapper sysPositionMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public void save(SysPosition sysPosition) {
        if (sysPosition.getStatus() == null) {
            sysPosition.setStatus(Constants.ENABLE);
        }
        sysPosition.setCreateTime(new Date());
        sysPosition.setUpdateTime(new Date());
        if (sysPositionMapper.insert(sysPosition) == 0) {
            throw new BaseException(Constants.FAIL_ADD);
        }
    }

    @Override
    public void update(SysPosition sysPosition) {
        if (sysPosition.getId() == null) {
            throw new BaseException(Constants.ID_EXITS);
        }
        sysPosition.setUpdateTime(new Date());
        if (sysPositionMapper.updateByPrimaryKeySelective(sysPosition) == 0) {
            throw new BaseException(Constants.FAIL_ADD);
        }
    }

    @Override
    public int delete(Set<Long> ids) {
        if (ids == null || CollectionUtils.isEmpty(ids) || ids.size() == 0) {
            throw new BaseException(Constants.ID_NOT);
        }
        List<SysUser> sysUsers = sysUserMapper.selectUserByPositionId(ids);
        if (!CollectionUtils.isEmpty(sysUsers)){
            throw new BaseException(String.format("所选数据绑定有用户%s占用", sysUsers.stream()
                    .map(SysUser::getUsername).collect(Collectors.toSet())));
        }
        int deleteBatch = sysPositionMapper.deleteBatch(ids);
        if (deleteBatch == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
        return deleteBatch;
    }

    @Override
    public SysPosition get(Long id) {
        return sysPositionMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysPosition> all(String positionName) {
        return sysPositionMapper.all(positionName);
    }

    @Override
    public PageResponse<SysPosition> list(PageRequest<SysPosition> request) {
        SysPosition sysPosition = StringUtil.checkObjFieldIsNull(request.getParams());
        PageResponse<SysPosition> response = new PageResponse<>();
        long count = sysPositionMapper.count(sysPosition);
        response.setPageSize(request.getPageSize());
        response.setPageIndex(request.getPageIndex());
        response.setTotal(count);
        if (count > 0) {
            List<SysPosition> page = sysPositionMapper.page(sysPosition, request.getOffset(), request.getPageSize());
            response.setData(page);
        }
        return response;
    }
}
