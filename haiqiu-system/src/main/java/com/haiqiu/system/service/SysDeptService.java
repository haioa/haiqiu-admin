package com.haiqiu.system.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysDept;
import com.haiqiu.system.entity.vo.SysDeptVo;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysDeptService {

    /**
     * 删除
     *
     * @param id 数据ID
     */
    int deleteByPrimaryKey(Long id);

    /**
     * 新增
     *
     * @param sysDept 新增实体
     */
    void save(SysDept sysDept);

    /**
     * 根据ID查询
     *
     * @param id 数据ID
     * @return 部门
     */
    SysDept get(Long id);

    /**
     * 根据ID查询查询本身和下级数据
     *
     * @param id 数据ID
     * @return 部门本级和所有子级
     */
    List<SysDept> selectChildrenByPrimaryKey(Long id);

    /**
     * 修改
     * @param sysDept 部门
     * @return 成功条数
     */
    int update(SysDept sysDept);

    /**
     * 查询所有部门
     *
     * @return 所有部门
     * @param deptName 部门名称
     * @param status 激活状态：true激活，false禁用
     */
    List<SysDept> all(String deptName, Boolean status);

    /**
     * 树形结构部门
     *
     * @return 树形部门集合
     */
    List<SysDeptVo> tree();

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 删除成功条数
     */
    int delete(Set<Long> ids);

    /**
     * 分页模糊
     * @param request 查询参数
     * @return 数据集合
     */
    PageResponse<SysDept> list(PageRequest<SysDept> request);

    /**
     * 树形模糊分页
     * @param request 查询参数
     * @return 数据集合
     */
    PageResponse<SysDeptVo> treeList(PageRequest<SysDept> request);

    /**
     * 批量激活(部门)
     * @param sysDepts 激活禁用部门
     * @return 成功条数
     */
    int active(List<SysDept> sysDepts);
}