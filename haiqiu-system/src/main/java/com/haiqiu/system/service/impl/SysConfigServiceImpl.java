package com.haiqiu.system.service.impl;

import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.result.Constants;
import com.haiqiu.system.entity.SysConfig;
import com.haiqiu.system.mapper.SysConfigMapper;
import com.haiqiu.system.service.SysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author haiqiu
 */
@Service
public class SysConfigServiceImpl implements SysConfigService {

    @Autowired
    private SysConfigMapper sysConfigMapper;

    @Override
    public SysConfig get(Integer configId) {
        return sysConfigMapper.selectByPrimaryKey(configId);
    }

    @Override
    public void update(SysConfig sysConfig) {
        if (sysConfigMapper.updateByPrimaryKeySelective(sysConfig)==0){
            throw new BaseException(Constants.FAIL_UPDATE);
        }
    }
}
