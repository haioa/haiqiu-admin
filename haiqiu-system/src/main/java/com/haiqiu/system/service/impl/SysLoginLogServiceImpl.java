package com.haiqiu.system.service.impl;


import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.tools.StringUtil;
import com.haiqiu.system.entity.SysLoginLog;
import com.haiqiu.system.mapper.SysLoginLogMapper;
import com.haiqiu.system.service.SysLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;

/**
 * @author: HaiQiu
 * @Date: 2021/4/6
 * @Description: 公告逻辑层
 */
@Service
public class SysLoginLogServiceImpl implements SysLoginLogService {

    @Autowired
    private SysLoginLogMapper sysLoginLogMapper;


    /**
     * 根据ID删除数据
     *
     * @param id 数据ID
     */
    @Override
    public void deleteByPrimaryKey(Long id) {
        if (StringUtils.isEmpty(id)) {
            throw new BaseException(Constants.ID_EXITS);
        }
        if (sysLoginLogMapper.deleteByPrimaryKey(id) == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
    }



    /**
     * 根据ID查询
     *
     * @param id 数据ID
     * @return 公告实体
     */
    @Override
    public SysLoginLog get(Long id) {
        SysLoginLog email = sysLoginLogMapper.selectByPrimaryKey(id);
        if (email == null) {
            throw new BaseException(Constants.DATA_EXIT);
        }
        return email;
    }



    /**
     * 批量删除公告
     *
     * @param ids 数据ID集合
     * @return 成功条数
     */
    @Override
    public int delete(Set<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            throw new BaseException(Constants.ID_EXITS);
        }
        int i = sysLoginLogMapper.batchDel(ids);
        if (i == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
        return i;
    }


    /**
     * 分页模糊查询
     *
     * @param request 查询实体（包含分页）
     * @return 返回分页数据
     */
    @Override
    public PageResponse<SysLoginLog> list(PageRequest<SysLoginLog> request) {
        SysLoginLog sysLoginLog = StringUtil.checkObjFieldIsNull(request.getParams());
        long count = sysLoginLogMapper.count(sysLoginLog);
        PageResponse<SysLoginLog> response = new PageResponse<>();
        response.setPageSize(request.getPageSize());
        response.setTotal(count);
        response.setPageIndex(request.getPageIndex());
        if (count > 0) {
            List<SysLoginLog> list = sysLoginLogMapper.page(sysLoginLog, request.getOffset(), request.getPageSize());
            response.setData(list);
        }
        return response;
    }


    @Override
    public List<SysLoginLog> all() {
        return sysLoginLogMapper.all();
    }


}
