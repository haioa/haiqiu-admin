package com.haiqiu.system.service;



import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysLoginLog;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysLoginLogService {
    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);


    /**
     * 查询单条
     * @param id
     * @return
     */
    SysLoginLog get(Long id);


    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 返回删除成功的条数
     */
    int delete(Set<Long> ids);

    /**
     * 模糊分页
     * @param request 查询实体（包含分页）
     * @return 返回数据集合
     */
    PageResponse<SysLoginLog> list(PageRequest<SysLoginLog> request);

    /**
     * 所有登录日志
     * @return 所有登录日志
     */
    List<SysLoginLog> all();

}