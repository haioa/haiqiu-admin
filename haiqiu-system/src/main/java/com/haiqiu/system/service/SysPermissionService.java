package com.haiqiu.system.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysPermission;
import com.haiqiu.system.entity.vo.MenuVo;
import com.haiqiu.system.entity.vo.RouterVo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysPermissionService {

    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增
     * @param sysPermission 新增实体
     */
    void save(SysPermission sysPermission);

    /**
     * 根据ID查询
     * @param id 数据ID
     * @return 权限
     */
    SysPermission get(Long id);

    /**
     * 修改
     * @param sysPermission 修改实体
     */
    void update(SysPermission sysPermission);

    /**
     * 根据用户名查询拥有的权限value值
     * @param username 用户名
     * @return 权限集合
     */
    List<String> selectAuthListByUsername(String username);

    /**
     * 根据token解析用户获取菜单
     * @param username 用户名
     * @return 菜单集合
     */
    List<SysPermission> selectPermissionListByUsername(String username);

    /**
     * 根据token解析用户获取菜单
     * @return 菜单集合
     */
    List<RouterVo> menu();

    /**
     * 分页模糊
     * @param request 请求体
     * @return 分页数据
     */
    PageResponse<SysPermission> list(PageRequest<SysPermission> request);

    /**
     * 查询所有菜单权限
     * @return 所有菜单权限
     */
    List<SysPermission> all();

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 成功删除条数
     */
    int delete(Set<Long> ids);

    /**
     * 树形结构菜单
     * @return 树形菜单集合
     */
    List<MenuVo> tree();

    /**
     * 批量激活(权限)
     * @param sysPermissions ID集合
     * @return 成功条数
     */
    int active(List<SysPermission> sysPermissions);

    /**
     * 树形分页菜单权限
     * @param request 查询条件
     * @return 分页递归数据
     */
    PageResponse<MenuVo> treeList(PageRequest<SysPermission> request);

    /**
     * 菜单列表集合
     * @param sysPermission 模糊查询条件
     * @return 菜单集合
     */
    List<SysPermission> menuList(SysPermission sysPermission);

    /**
     * 根据角色ID查询菜单ID集合与菜单下拉树结构
     * @param roleId 角色ID
     * @return 菜单集合
     */
    Map<String,Object> roleMenuTreeSelect(Long roleId);
}