package com.haiqiu.system.service;

import com.haiqiu.common.entity.chat.UserChat;

import java.util.List;

/**
 * @author HaiQiu
 */
public interface ChatService {

    /**
     * 获取聊天用户列表
     * @return 用户列表
     */
    List<UserChat> list();
}
