package com.haiqiu.system.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysPosition;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
public interface SysPositionService {
    /**
     * 新增
     * @param sysPosition 职位
     */
    void save(SysPosition sysPosition);

    /**
     * 修改
     * @param sysPosition 职位
     */
    void update(SysPosition sysPosition);

    /**
     * 批量删除
     * @param ids 数据ID集合
     * @return 成功条数
     */
    int delete(Set<Long> ids);

    /**
     * 根据职位ID查询职位
     * @param id 职位ID
     * @return 职位
     */
    SysPosition get(Long id);

    /**
     * 查询全部职位
     * @param positionName 职位名称模糊匹配参数
     * @return 职位列表
     */
    List<SysPosition> all(String positionName);

    /**
     * 职位模糊查询分页
     * @param request 请求参数
     * @return 职位集合
     */
    PageResponse<SysPosition> list(PageRequest<SysPosition> request);
}
