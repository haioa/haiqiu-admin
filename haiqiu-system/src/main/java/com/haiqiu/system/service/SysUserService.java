package com.haiqiu.system.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.system.entity.SysUser;
import com.haiqiu.system.entity.dto.LoginDto;
import com.haiqiu.system.entity.dto.RegDto;
import com.haiqiu.system.entity.dto.ResetUserDto;
import com.haiqiu.system.entity.query.UserQuery;
import com.haiqiu.system.entity.vo.UserPwd;
import com.haiqiu.system.entity.vo.SysUserVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author haiqiu
 */
public interface SysUserService {
    /**
     * 删除
     * @param id 数据ID
     */
    void deleteByPrimaryKey(Long id);

    /**
     * 新增账户
     * @param userVo 账户实体
     */
    void save(SysUserVo userVo);

    /**
     * 查询单个账户
     * @param id 数据ID
     * @return 返回账户实体
     */
    SysUser selectByPrimaryKey(Long id);

    /**
     * 修改账户
     * @param userVo 账户实体
     */
    void update(SysUserVo userVo);

    /**
     * 登录
     *
     * @param loginDto 登录实体
     * @return 认证信息（token）
     */
    Map<String,Object> login(LoginDto loginDto);

    /**
     * 获取用户信息
     * @Param token 用户token
     * @return 用户信息
     */
    SysUserVo info(String token);

    /**
     * 模糊分页
     *
     * @param request 请求实体
     * @return 分页数据
     */
    PageResponse<SysUserVo> list(PageRequest<UserQuery> request);

    /**
     * 批量删除用户
     *
     * @param ids 数据ID集合
     * @return 成功条数
     */
    int delete(List<Long> ids);

    /**
     * 查询所有(用户)
     *
     * @return 所有用户
     */
    List<SysUser> all();

    /**
     * ID获取用户信息
     *
     * @param id 账户ID
     * @return 账户信息包含角色部门等
     */
    SysUserVo get(Long id);

    /**
     * 激活禁用账户
     *
     * @param sysUser 用户实体
     * @return int
     */
    int active(SysUser sysUser);

    /**
     * 头像上传接口
     *
     * @param file 文件
     * @return 文件地址
     */
    String avatar(MultipartFile file);

    /**
     * 后台列表头像上传
     *
     * @param file 文件
     * @return 文件地址
     */
    String avatarUpload(MultipartFile file);

    /**
     * 注册
     * @param regDto 注册账户实体
     * @return int
     */
    void register(RegDto regDto);

    /**
     * 退出登录
     */
    void logout(String token);


    /**
     * 查询所有(用户昵称和用户名和邮箱)
     * @return 所有(用户昵称和用户名和邮箱)
     */
    List<SysUser> emailAll();

    /**
     * 重置密码
     * @param userPwd 重置信息
     */
    void resetPwd(UserPwd userPwd);

    /**
     * 授权用户角色
     * @param userId 用户ID
     * @param roleIds 角色ID集合
     */
    void authRole(Long userId, List<Long> roleIds);

    /**
     * 用户个人中心修改密码
     * @param userPwd 修改参数
     */
    void updateUserPwd(UserPwd userPwd);

    /**
     * 用户个人中心修改信息
     * @param avatar 头像
     * @param nickname 昵称
     * @param email 邮箱
     * @param phone 手机
     * @param sex 性别
     */
    void updateUserProfile(String avatar,String nickname, String email, String phone, Integer sex);


    /**
     * 同步用户在线
     */
    void online();

    /**
     * 在线用户列表
     * @return 在线用户列表
     * @param userName
     * @param ipaddr
     */
    Map<String,Object> onlineList(String userName, String ipaddr);

    /**
     * 强制踢出用户下线
     * @param token 用户token
     */
    void forceLogout(String token);

    /**
     * 导出用户信息到excel
     */
    void export() throws IOException;

    /**
     * 忘记密码发送验证码
     * @param email 邮箱
     */
    void forgetVerify(String email);

    /**
     * 忘记密码进行重置密码操作
     * @param resetUserDto 重置实体类
     */
    void reset(ResetUserDto resetUserDto);
}