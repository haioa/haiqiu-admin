package com.haiqiu.system.service.impl;

import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.tools.StringUtil;
import com.haiqiu.system.entity.SysDept;
import com.haiqiu.system.entity.vo.SysDeptVo;
import com.haiqiu.system.mapper.SysDeptMapper;
import com.haiqiu.system.service.SysDeptService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @author haiqiu
 */
@Service
public class SysDeptServiceImpl implements SysDeptService {

    @Autowired
    private SysDeptMapper sysDeptMapper;

    /**
     * 删除单个部门
     * @param id 数据ID
     * @return 返回删除成功条数
     */
    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysDeptMapper.deleteByPrimaryKey(id);
    }

    /**
     * 新增部门
     * @param sysDept 新增实体
     */
    @Override
    public void save(SysDept sysDept) {
        if (sysDept == null || sysDept.getDeptName() == null) {
            throw new BaseException("新增数据不能为空");
        }
        if (StringUtils.isEmpty(sysDept.getParentId())){
            sysDept.setParentId(0L);
        }
        sysDept.setStatus(Constants.ON);
        sysDept.setCreateTime(new Date());
        sysDept.setUpdateTime(new Date());
        if (sysDeptMapper.insert(sysDept) == 0) {
            throw new BaseException("新增部门失败");
        }
    }

    /**
     * 查询单个数据
     * @param id 数据ID
     * @return 返回数据实体
     */
    @Override
    public SysDept get(Long id) {
        return sysDeptMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<SysDept> selectChildrenByPrimaryKey(Long id) {
        return sysDeptMapper.selectChildrenByPrimaryKey(id);
    }

    /**
     * 修改数据
     * @param sysDept 修改实体
     * @return 修改成功条数
     */
    @Override
    public int update(SysDept sysDept) {
        if (StringUtils.isEmpty(sysDept.getParentId())){
            sysDept.setParentId(0L);
        }
        sysDept.setUpdateTime(new Date());
        return sysDeptMapper.updateByPrimaryKeySelective(sysDept);
    }

    /**
     * 返回所有的数据
     * @return 所有的部门
     * @param deptName 部门名称
     * @param status 激活状态：true激活，false禁用
     */
    @Override
    public List<SysDept> all(String deptName, Boolean status) {
        return sysDeptMapper.all(deptName,status);
    }

    /**
     * 树形结构部门数据
     * @return 树形结构
     */
    @Override
    public List<SysDeptVo> tree() {
        List<SysDept> sysDeptList = sysDeptMapper.all(null,null);
        List<SysDeptVo> deptVoList = new LinkedList<>();
        for (SysDept sysDept : sysDeptList) {
            SysDeptVo deptVo = new SysDeptVo();
            BeanUtils.copyProperties(sysDept, deptVo);
            deptVoList.add(deptVo);
        }
        return this.deptTreeList(deptVoList);
    }

    /**
     * 批量删除
     *
     * @param ids 数据ID集合
     * @return 删除成功条数
     */
    @Override
    public int delete(Set<Long> ids) {
        int i = sysDeptMapper.batchDel(ids);
        if (i == 0) {
            throw new BaseException(Constants.FAIL_DEL);
        }
        return i;
    }


    /**
     * 分页模糊
     *
     * @param request 查询参数
     * @return 数据集合
     */
    @Override
    public PageResponse<SysDept> list(PageRequest<SysDept> request) {
        SysDept sysDept = StringUtil.checkObjFieldIsNull(request.getParams());
        long count = sysDeptMapper.count(sysDept);
        PageResponse<SysDept> response = new PageResponse<>();
        response.setTotal(count);
        response.setPageIndex(request.getPageIndex());
        response.setPageSize(request.getPageSize());
        if (count > 0) {
            List<SysDept> list = sysDeptMapper.page(sysDept, request.getOffset(), request.getPageSize());
            response.setData(list);
        }
        return response;
    }

    @Override
    public PageResponse<SysDeptVo> treeList(PageRequest<SysDept> request) {
        SysDept sysDept = StringUtil.checkObjFieldIsNull(request.getParams());
        sysDept.setParentId(0L);
        long count = sysDeptMapper.count(sysDept);
        PageResponse<SysDeptVo> response = new PageResponse<>();
        response.setTotal(count);
        response.setPageIndex(request.getPageIndex());
        response.setPageSize(request.getPageSize());
        if (count > 0) {
            List<SysDept> list = sysDeptMapper.page(sysDept, request.getOffset(), request.getPageSize());
            response.setData(this.children(list));
        }
        return response;
    }

    @Override
    public int active(List<SysDept> sysDepts) {
        int flag = 0;
        for (SysDept sysDept : sysDepts) {
            SysDept dept = StringUtil.checkObjFieldIsNull(sysDept);
            sysDeptMapper.updateByPrimaryKeySelective(dept);
            flag++;
        }
        return flag;
    }


    /**
     * 模糊分页递归查询子集
     * @param sysDepts 父级集合
     * @return 树形集合
     */
    public List<SysDeptVo> children(List<SysDept> sysDepts){
        List<SysDeptVo> deptVoList = new ArrayList<>();
        List<SysDept> all = all(null,null);
        List<SysDeptVo> deptVos = new ArrayList<>();
        for (SysDept sysDept : all) {
            SysDeptVo deptVo = new SysDeptVo();
            BeanUtils.copyProperties(sysDept,deptVo);
            deptVos.add(deptVo);
        }
        for (SysDept sysDept : sysDepts) {
            SysDeptVo deptVo = new SysDeptVo();
            BeanUtils.copyProperties(sysDept,deptVo);
            deptVoList.add(selectChildrenList(deptVo,deptVos));
        }
        return deptVoList;
    }


    /**
     * 递归一级
     *
     * @param deptVoList 所有集合
     * @return 树形集合
     */
    private List<SysDeptVo> deptTreeList(List<SysDeptVo> deptVoList) {
        List<SysDeptVo> finalDeptVoList = new LinkedList<>();
        for (SysDeptVo deptVo : deptVoList) {
            if (deptVo.getParentId() == 0) {
                finalDeptVoList.add(this.selectChildrenList(deptVo, deptVoList));
            }
        }
        return finalDeptVoList;
    }


    /**
     * 递归子集
     *
     * @param deptVo     父级对象
     * @param deptVoList 所有集合
     * @return 父级对象
     */
    private SysDeptVo selectChildrenList(SysDeptVo deptVo, List<SysDeptVo> deptVoList) {
        deptVo.setChildren(new LinkedList<>());
        for (SysDeptVo deptNode : deptVoList) {
            if (deptNode.getParentId().equals(deptVo.getId())) {
                if (deptVo.getChildren() == null) {
                    deptVo.setChildren(new LinkedList<>());
                }
                deptVo.getChildren().add(selectChildrenList(deptNode, deptVoList));
            }
        }
        return deptVo;
    }


}
