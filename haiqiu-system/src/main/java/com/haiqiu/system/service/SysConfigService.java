package com.haiqiu.system.service;

import com.haiqiu.system.entity.SysConfig;

/**
 * @author haiqiu
 */
public interface SysConfigService {

    /**
     * 根据ID查询系统配置
     * @param configId 配置ID
     * @return 系统配置
     */
    SysConfig get(Integer configId);

    /**
     * 修改系统配置
     * @param sysConfig 系统配置实体
     */
    void update(SysConfig sysConfig);
}
