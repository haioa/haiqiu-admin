package com.haiqiu.system.service.impl;

import com.haiqiu.common.entity.chat.UserChat;
import com.haiqiu.system.entity.SysUser;
import com.haiqiu.system.mapper.SysUserMapper;
import com.haiqiu.system.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * @author HaiQiu
 * @date 2021/10/26
 * @desc 聊天通讯逻辑处理
 */
@Service
public class ChatServiceImpl implements ChatService {

    @Autowired
    private SysUserMapper sysUserMapper;


    @Override
    public List<UserChat> list() {
        List<SysUser> userList = sysUserMapper.all();
        List<UserChat> userChatList = new LinkedList<>();
        for (SysUser sysUser : userList) {
            UserChat userChat = new UserChat();
            userChat.setId(sysUser.getUsername());
            userChat.setImg(sysUser.getAvatar());
            userChat.setName(sysUser.getNickname());
            userChat.setReadNum(new Random().nextInt(99));
            userChat.setDept(UUID.randomUUID().toString());
            userChatList.add(userChat);
        }
        return userChatList;
    }
}
