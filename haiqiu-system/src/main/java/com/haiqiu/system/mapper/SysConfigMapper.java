package com.haiqiu.system.mapper;

import com.haiqiu.system.entity.SysConfig;
import org.springframework.stereotype.Repository;

/**
 * SysConfigMapper继承基类
 * @author HaiQiu
 */
@Repository
public interface SysConfigMapper {

    /**
     * 根据ID查询系统配置
     * @param id id
     * @return 系统配置
     */
    SysConfig selectByPrimaryKey(Integer id);

    /**
     * 修改系统配置
     * @param sysConfig 系统配置实体
     * @return 成功条数
     */
    int updateByPrimaryKeySelective(SysConfig sysConfig);
}