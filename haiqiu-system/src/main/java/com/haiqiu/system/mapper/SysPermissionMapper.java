package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission,Long> {
    /**
     * 根据用户名查询对应的权限列表
     * @param username 用户名
     * @return 数据集合
     */
    List<String> selectAuthListByUsername(String username);

    /**
     * 根据用户名查询菜单
     * @param username 用户名
     * @return 数据集合
     */
    List<SysPermission> selectPermissionListByUsername(String username);


    /**
     * 根据用户名查询权限路径
     * @param username 用户名
     * @return 数据集合
     */
    List<SysPermission> getAuthListByUsername(String username);

    /**
     * 查询所有的菜单权限
     * @return 数据集合
     */
    List<SysPermission> menuAll();

    /**
     * 批量删除
     * @param ids 删除ID集合
     * @return 成功删除条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页总条数
     * @param sysPermission 查询实体
     * @return 总条数
     */
    long count(@Param("sysPermission") SysPermission sysPermission);

    /**
     * 模糊分页
     * @param sysPermission 查询参数
     * @param offset 页数
     * @param pageSize 每页数量
     * @return 分页数据
     */
    List<SysPermission> list(@Param("sysPermission") SysPermission sysPermission, @Param("offset") Long offset, @Param("pageSize") Long pageSize);

    /**
     * 查询所有
     * @return 返回所有
     */
    List<SysPermission> all();


    /**
     * 批量激活(权限)
     * @param sysPermissions ID集合
     * @return 成功条数
     */
    int active(List<SysPermission> sysPermissions);

    /**
     * 父级总数
     * @param sysPermission 条件
     * @return 总数
     */
    long countTree(@Param("sysPermission") SysPermission sysPermission);

    /**
     * 父级分页
     * @param sysPermission 条件
     * @param offset 页数
     * @param pageSize 每页大小
     * @return
     */
    List<SysPermission> listTree(@Param("sysPermission") SysPermission sysPermission, @Param("offset") long offset, @Param("pageSize") Long pageSize);

    /**
     * 查询子级绑定有父级的id集合
     * @param parentId 父级ID
     * @return 集合
     */
    List<SysPermission> selectByParentId(Long parentId);

    /**
     * 根据用户名查询菜单
     * @param username 用户名
     * @return 菜单集合
     */
    List<SysPermission> getMenuByUsername(String username);

    /**
     * 菜单列表集合
     * @param sysPermission 模糊查询条件
     * @return 菜单集合
     */
    List<SysPermission> menuList(@Param("sysPermission") SysPermission sysPermission);

    /**
     * 根据角色ID查询菜单下拉树结构
     * @param roleId 角色ID
     * @return 菜单集合
     */
    List<SysPermission> getMenuByRoleId(Long roleId);
}