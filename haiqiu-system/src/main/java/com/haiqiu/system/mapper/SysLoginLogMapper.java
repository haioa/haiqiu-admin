package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysLoginLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * SysLogMapper继承基类
 * @author HaiQiu
 */
@Mapper
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog, Long> {

    /**
     * 查询所有
     *
     * @return 返回所有
     */
    List<SysLoginLog> all();

    /**
     * 批量删除
     *
     * @param ids 数据ID集合
     * @return 删除成功条数
     */
    int batchDel(Set<Long> ids);

    /**
     * 模糊分页总条数
     *
     * @param sysLoginLog 查询实体
     * @return 总条数
     */
    long count(@Param("sysLoginLog") SysLoginLog sysLoginLog);

    /**
     * 模糊分页
     *
     * @param sysLoginLog 查询参数
     * @param offset   页数
     * @param pageSize 每页数量
     * @return 分页数据
     */
    List<SysLoginLog> page(@Param("sysLoginLog") SysLoginLog sysLoginLog, @Param("offset") long offset, @Param("pageSize") Long pageSize);
}