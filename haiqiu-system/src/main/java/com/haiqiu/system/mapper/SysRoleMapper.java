package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysPermission;
import com.haiqiu.system.entity.SysRole;
import com.haiqiu.system.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author haiqiu
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole,Long> {
    /**
     * 批量删除
     * @param ids 删除ID集合
     * @return 成功删除条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页总条数
     * @param sysRole 查询实体
     * @return 总条数
     */
    long count(@Param("sysRole") SysRole sysRole);

    /**
     * 模糊分页
     * @param sysRole 查询参数
     * @param offset 页数
     * @param pageSize 每页数量
     * @return 分页数据
     */
    List<SysRole> list(@Param("sysRole") SysRole sysRole, @Param("offset") Long offset, @Param("pageSize") Long pageSize);

    /**
     * 查询所有
     * @return 返回所有
     */
    List<SysRole> all();

    /**
     * 根据用户ID删除对应的角色
     * @param userId 用户ID
     * @return 删除成功条数
     */
    int delRoleByUserId(Long userId);

    /**
     * 根据用户ID添加对应的角色
     * @param userId 用户ID
     * @param roleIds 角色ID
     * @return 新增成功条数
     */
    int addRoleByUserId(@Param("userId") Long userId,@Param("roleIds") List<Long> roleIds);

    /**
     * 根据角色ID添加对应的权限
     * @param id 角色ID
     * @param permissionIds 权限ID
     * @return 添加成功条数
     */
    int insertByPermissionIds(@Param("id") Long id,@Param("permissionIds") List<Long> permissionIds);

    /**
     * 根据角色ID删除对应的权限
     * @param roleId 角色ID
     * @return 返回成功条数
     */
    int delAuthByRoleId(Long roleId);


    /**
     * 根据角色ID查询对应的权限集合
     * @param id 角色ID
     * @return 返回权限集合
     */
    List<SysPermission> selectAuthByRoleId(Long id);

    /**
     * 根据用户ID查询拥有的角色
     * @param userId 用户ID
     * @return 角色集合
     */
    List<SysRole> selectRoleByUserId(Long userId);

    /**
     * 根据角色ID删除对应的用户绑定角色信息关联表
     * @param roleId 角色ID
     * @return 成功条数
     */
    int delUserByRoleId(Long roleId);

    /**
     * 根据角色ID查询角色已授权用户列表
     * @param roleId 角色ID
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 总条数
     */
    long allocatedCount(@Param("roleId") Long roleId,@Param("username") String username,
                        @Param("phone") String phone,@Param("email") String email);

    /**
     * 根据角色ID查询角色已授权用户列表
     * @param roleId 角色ID
     * @param pageIndex 页数
     * @param pageSize 每页数量
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 数据集合
     */
    List<SysUser> allocatedPage(@Param("roleId") Long roleId, @Param("pageIndex") Long pageIndex,
                                @Param("pageSize") Long pageSize, @Param("username") String username,
                                @Param("phone") String phone, @Param("email") String email);


    /**
     * 根据角色ID查询角色已授权用户列表
     * @param roleId 角色ID
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 总条数
     */
    long unallocatedCount(@Param("roleId") Long roleId,@Param("username") String username,
                        @Param("phone") String phone,@Param("email") String email);

    /**
     * 根据角色ID查询角色已授权用户列表
     * @param roleId 角色ID
     * @param pageIndex 页数
     * @param pageSize 每页数量
     * @param username 用户名（查询使用）
     * @param phone 手机（查询使用）
     * @param email 邮箱（查询使用）
     * @return 数据集合
     */
    List<SysUser> unallocatedPage(@Param("roleId") Long roleId, @Param("pageIndex") Long pageIndex,
                                  @Param("pageSize") Long pageSize, @Param("username") String username,
                                  @Param("phone") String phone, @Param("email") String email);


    /**
     * 根据角色ID批量给用户添加该角色
     * @param roleId 角色ID
     * @param userIds 用户ID集合
     * @return 成功条数
     */
    int addUserByRoleId(Long roleId, Set<Long> userIds);

    /**
     * 根据角色ID批量给用户删除该角色
     * @param roleId 角色ID
     * @param userIds 用户ID集合
     * @return 成功条数
     */
    int deleteUserByRoleId(Long roleId, Set<Long> userIds);

    /**
     * 根据菜单ID查询绑定的角色
     * @param menuId 菜单ID
     * @return 角色
     */
    List<SysRole> selectRoleByMenuId(Long menuId);

    /**
     * 根据角色名称查询角色
     * @param roleName 角色名称
     * @return
     */
    SysRole selectRoleByName(String roleName);
}