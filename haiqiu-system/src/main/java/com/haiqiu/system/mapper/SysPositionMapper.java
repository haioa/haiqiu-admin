package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysPosition;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * SysPositionMapper继承基类
 *
 * @author HaiQiu
 */
@Repository
public interface SysPositionMapper extends BaseMapper<SysPosition, Long> {

    /**
     * 批量删除
     *
     * @param ids 数据集合
     * @return 成功条数
     */
    int deleteBatch(Set<Long> ids);

    /**
     * 根据用户ID添加多个职位（绑定用户和职位的关联）
     *
     * @param userId      用户ID
     * @param positionIds 职位ID集合
     * @return 成功条数
     */
    int addPositionByUserId(@Param("userId") Long userId, @Param("positionIds") List<Long> positionIds);

    /**
     * 根据用户ID删除关联的职位信息
     *
     * @param userId 用户ID
     * @return 成功条数
     */
    int delPositionByUserId(Long userId);

    /**
     * 查询全部职位
     *
     * @param positionName 职位名称模糊匹配参数
     * @return 职位列表
     */
    List<SysPosition> all(String positionName);

    /**
     * 模糊分页总数
     *
     * @param sysPosition 参数
     * @return 总条数
     */
    long count(@Param("sysPosition") SysPosition sysPosition);

    /**
     * 模糊分页
     *
     * @param sysPosition 参数
     * @return 数据集合
     */
    List<SysPosition> page(@Param("sysPosition") SysPosition sysPosition, @Param("offset") Long offset, @Param("pageSize") Long pageSize);

    /**
     * 根据用户Id查询关联的职位集合
     * @param userId 用户ID
     * @return 职位集合
     */
    List<SysPosition> selectPositionByUserId(Long userId);
}