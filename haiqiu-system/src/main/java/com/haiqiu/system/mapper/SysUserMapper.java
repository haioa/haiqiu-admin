package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysUser;
import com.haiqiu.system.entity.query.UserQuery;
import com.haiqiu.system.entity.vo.SysUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author HaiQiu
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser, Long> {
    /**
     * 根据用户名查询用户
     * @param username 用户名
     * @return 用户
     */
    SysUser selectByUsername(String username);

    /**
     * 模糊查询总条数
     * @param userQuery 用户实体dto
     * @return 总数
     */
    long count(@Param("userQuery") UserQuery userQuery);

    /**
     * 模糊分页
     * @param userQuery 用户实体dto
     * @param offset 页数
     * @param pageSize 每页条数
     * @return 用户集合
     */
    List<SysUserVo> list(@Param("userQuery") UserQuery userQuery, @Param("offset") Long offset, @Param("pageSize") Long pageSize);

    /**
     * 批量删除
     * @param ids 集合ID
     * @return 执行成功条数
     */
    int delBatch(List<Long> ids);


    /**
     * 查询所有
     * @return 所有用户
     */
    List<SysUser> all();


    /**
     * token查询用户信息
     * @param usernameFromToken 用户名
     * @return 用户信息
     */
    SysUser userInfoByUsername(String usernameFromToken);

    /**
     * 查询单条
     * @param id 数据ID
     * @return 账户信息包含角色部门等
     */
    SysUserVo getById(Long id);

    /**
     * 根据邮箱查询用户
     * @param email
     * @return 用户
     */
    SysUser selectByEmail(String email);

    /**
     * 账户登录方法（邮箱、手机号、用户名都可以登录）
     * @param account 账户名称：手机、邮箱、用户名
     * @return 账户信息
     */
    SysUser login(String account);


    /**
     * 查询所有(用户昵称和用户名和邮箱)
     * @return 所有(用户昵称和用户名和邮箱)
     */
    List<SysUser> emailAll();

    /**
     * 根据职位ID集合查询所绑定的用户
     * @param positionIds
     * @return
     */
    List<SysUser> selectUserByPositionId(Set<Long> positionIds);

    /**
     * 导出用户数据
     * @return 用户集合
     */
    List<SysUserVo> export();
}