package com.haiqiu.system.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.system.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * SysDeptMapper继承基类
 *
 * @author haiqiu
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept, Long> {
    /**
     * 查询所有
     * @param deptName 部门名称
     * @param status 激活状态：true激活，false禁用
     * @return 返回所有
     */
    List<SysDept> all(@Param("deptName") String deptName, @Param("status") Boolean status);

    /**
     * 批量删除
     *
     * @param ids 数据ID集合
     * @return 删除成功条数
     */
    int batchDel(Set<Long> ids);

    /**
     * 模糊分页总条数
     *
     * @param sysDept 查询实体
     * @return 总条数
     */
    long count(@Param("sysDept") SysDept sysDept);

    /**
     * 模糊分页
     *
     * @param sysDept 查询参数
     * @param offset   页数
     * @param pageSize 每页数量
     * @return 分页数据
     */
    List<SysDept> page(@Param("sysDept") SysDept sysDept, @Param("offset") long offset, @Param("pageSize") Long pageSize);

    /**
     * 根据ID查询查询本身和下级数据
     *
     * @param id 数据ID
     * @return 部门本级和所有子级
     */
    List<SysDept> selectChildrenByPrimaryKey(Long id);


    /**
     * 根据部门名称查询部门
     * @param deptName 部门名称
     * @return 部门
     */
    SysDept selectDeptByName(String deptName);
}