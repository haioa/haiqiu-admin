//package com.haiqiu.common.swagger;
//
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.*;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spi.service.contexts.SecurityContext;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//
//import java.util.*;
//
///**
// * @author HaiQiu
// * @date 2021/4/2 21:56
// * @desc swagger配置
// **/
//@EnableSwagger2
//@Configuration
//@Profile({"dev","test","prod","stable"})//环境开启swagger，不写表示不开启
//public class SwaggerConfiguration implements WebMvcConfigurer {
//
//    private final SwaggerProperties swaggerProperties;
//
//    public SwaggerConfiguration(SwaggerProperties swaggerProperties) {
//        this.swaggerProperties = swaggerProperties;
//    }
//
//    @Bean
//    Docket docket() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
//                .paths(PathSelectors.any())
//                .build()
//                //  安全上下文
//                .securityContexts(Arrays.asList(securityContexts()))
//                //token设置
//                .securitySchemes(unifiedAuth())
//
//                .apiInfo(new ApiInfoBuilder()
//                        .description(swaggerProperties.getApplicationDescription())
//                        .title(swaggerProperties.getApplicationName())
//                        .contact(new Contact(swaggerProperties.getDeveloper(), swaggerProperties.getUrl(), swaggerProperties.getEmail()))
//                        .version(swaggerProperties.getApplicationVersion())
//                        .license("Apache2.0")
//                        .build());
//    }
//
//    private static List<ApiKey> unifiedAuth() {
//        List<ApiKey> arrayList = new ArrayList();
//        arrayList.add(new ApiKey("Authorization", "Authorization", "header"));
//        return arrayList;
//    }
//
//    private SecurityContext securityContexts() {
//        return SecurityContext.builder()
//                .securityReferences(defaultAuth())
//                .forPaths(PathSelectors.any())
//                .build();
//    }
//
//    private List<SecurityReference> defaultAuth() {
//        AuthorizationScope authorizationScope = new AuthorizationScope("global", "描述信息");
//        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//        authorizationScopes[0] = authorizationScope;
//        return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
//    }
//
//
//}
