package com.haiqiu.common.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author HaiQiu
 * @date 2021/4/2 21:57
 * @desc swagger配置
 **/

@Component
@ConfigurationProperties("swagger")
@Data
public class SwaggerProperties {
    /**
     * 是否开启swagger，生产环境一般关闭，所以这里定义一个变量
     */
    private Boolean enable;


    /**
     * 项目应用名
     */
    private String applicationName;


    /**
     * 项目版本信息
     */
    private String applicationVersion;


    /**
     * 项目描述信息
     */
    private String applicationDescription;

    /**
     * 包扫描路径
     */
    private String basePackage;


    /**
     * 接口调试地址
     */
    private String tryHost;

    /**
     * 接口调试地址
     */
    private String developer;

    /**
     * 接口调试地址
     */
    private String email;

    /**
     * 接口调试地址
     */
    private String url;
}
