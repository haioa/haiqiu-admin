package com.haiqiu.common.swagger;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author HaiQiu
 * @date 2022-04-27
 * springDoc文档配置
 */
@Configuration
@OpenAPIDefinition(security = @SecurityRequirement(name="jwt"))
@SecurityScheme(
    type = SecuritySchemeType.HTTP,name = "jwt",in = SecuritySchemeIn.HEADER,scheme = "bearer"
)
public class SpringDocConfig {

    private final SwaggerProperties swaggerProperties;

    public static final String TOKEN_KEY = "Authorization";

    public SpringDocConfig(SwaggerProperties swaggerProperties) {
        this.swaggerProperties = swaggerProperties;
    }

    @Bean
    public OpenAPI openAPI(){
        return new OpenAPI()
                .info(new Info()
                        .contact(new Contact()
                                .name(swaggerProperties.getDeveloper())
                                .email(swaggerProperties.getEmail())
                                .url(swaggerProperties.getUrl()))
                        .title(swaggerProperties.getApplicationName())
                        .version(swaggerProperties.getApplicationVersion())
                        .description(swaggerProperties.getApplicationDescription()))
                .externalDocs(new ExternalDocumentation()
                        .description("SpringShop Wiki Documentation")
                        .url("https://springshop.wiki.github.org/docs"));
    }
}
