package com.haiqiu.common.exception;

import com.haiqiu.common.result.ResultCode;

/**
 * @author: HaiQiu
 * @Date: 2021/4/2
 * @Description: 通用异常处理
 */
public class BaseException extends RuntimeException{

    /**
     * 错误码
     */
    int code;

    /**
     * 异常消息
     */
    private String message;

    /**
     * 自定义信息
     * @param message 消息
     */
    public BaseException(String message){
        this.code = ResultCode.FAIL.getCode();
        this.message = message;
    }

    /**
     * 自定义状态码和信息
     * @param code 状态码
     * @param message 消息
     */
    public BaseException(int code,String message){
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
