package com.haiqiu.common.exception;


/**
 * @author HaiQiu
 * 限流自定义异常
 */
public class LimitAccessException extends BaseException  {

    private static final long serialVersionUID = -3608667856397125671L;

    public LimitAccessException(String message) {
        super(message);
    }
}
