package com.haiqiu.common.controller;



import com.haiqiu.common.result.ResultData;

/**
 * @author : haiqiu
 * @date ：2020/12/14 11:03
 * @description: Controller父控制器
 */
public class BaseController {

    /**
     * 成功返回消息
     * @param message 消息
     * @return 提示消息
     */
    public ResultData decide(String message){
        return ResultData.ok(message);
    }

    /**
     * 判断是否成功失败Boolean型
     * @param count 执行之后的int类型条数
     * @return 判断是否成功失败Boolean型
     */
    public ResultData decide(boolean count){
        return count ? ResultData.ok() : ResultData.fail();
    }


    /**
     * 判断是否成功失败int型
     * @param count 执行之后的int类型条数
     * @return 判断是否成功失败int型
     */
    public ResultData decide(int count){
        return count!=0 ? ResultData.ok() : ResultData.fail();
    }
    /**
     * 成功失败并返回数据Object
     * @param o 数据
     * @return 返回数据对象
     */
    public ResultData decide(Object o){
        return ResultData.ok(o);
    }

    /**
     * 返回批量操作提示语句
     * @param msg 消息前缀，如：删除
     * @param count 成功条数
     * @return 返回实列：成功删除8条数据
     */
    public ResultData decide(String msg,int count){
        return ResultData.ok(String.format("成功%s%d数据",msg,count));
    }



}
