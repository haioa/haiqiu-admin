package com.haiqiu.common.mapper;

import java.io.Serializable;

/**
 * DAO公共基类，由MybatisGenerator自动生成请勿修改
 * @author HaiQiu
 * @param <Model> The Model Class 这里是泛型不是Model类
 * @param <PK> The Primary Key Class 如果是无主键，则可以用Model来跳过，如果是多主键则是Key类
 */
public interface BaseMapper<Model, PK extends Serializable> {
    /**
     * 公共删除单个方法接口
     * @param id 数据ID
     * @return 执行成功的条数
     */
    int deleteByPrimaryKey(PK id);

    /**
     * 公共新增数据
     * @param record 数据实体
     * @return 返回成功条数
     */
    int insert(Model record);

    /**
     * 公共新增数据(带字符校验，有数据就新增)
     * @param record 数据实体
     * @return 返回成功条数
     */
    int insertSelective(Model record);


    /**
     * 公共根据ID查询一条数据
     * @param id 数据ID
     * @return 返回的数据实体
     */
    Model selectByPrimaryKey(PK id);

    /**
     * 修改一条数据
     * @param record 数据实体
     * @return 返回成功条数
     */
    int updateByPrimaryKeySelective(Model record);

    /**
     * 修改一条数据(不做数据校验判断)
     * @param record 数据实体
     * @return 返回成功条数
     */
    int updateByPrimaryKey(Model record);
}