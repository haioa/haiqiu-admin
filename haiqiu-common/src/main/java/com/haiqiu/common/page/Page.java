package com.haiqiu.common.page;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author: HaiQiu
 * @Date: 2021/3/31
 * @Description: 分页工具
 */
@Schema(description = "分页工具")
public class Page implements Serializable {
    /**
     * 默认第一页
     */
    private static final long FIRST_PAGE_INDEX = 1;
    /**
     * 默认每页10条
     */
    private static final long DEFAULT_PAGE_SIZE = 10;
    /**
     * 最大每页数量100条
     */
    private static final long PAGE_MAX_SIZE = 100;

    /**
     * 页码.
     * 为了兼容前端展示，页码统一从1开始；不能为0或负数。
     */
    @Schema(description = "页码从1开始", required = true)
    protected Long pageIndex = FIRST_PAGE_INDEX;

    /**
     * 页面大小，即一页的最大条数。
     */
    @Schema(description = "页面大小，即一页的最大条数")
    protected Long pageSize = DEFAULT_PAGE_SIZE;

    /**
     * 无参方法
     */
    public Page() {
        pageIndex = FIRST_PAGE_INDEX;
        pageSize = PAGE_MAX_SIZE;
    }


    /**
     * 有参方法
     * @param pageIndex 页数
     * @param pageSize 每页大小
     */
    public Page(Long pageIndex, Long pageSize) {
        setPageIndex(pageIndex);
        setPageSize(pageSize);
    }


    /**
     * 获取条数
     * @return 第几条开始
     */
    public Long getPageIndex() {
        return pageIndex;
    }

    /**
     * 设置第一页必须是1开始，不能为负数
     *
     * @param pageIndex 第几条开始
     */
    public void setPageIndex(Long pageIndex) {
        if (pageIndex != null && pageIndex > 0) {
            this.pageIndex = pageIndex;
        }
    }

    /**
     * 获取每页大小数
     * @return
     */
    public Long getPageSize() {
        return pageSize;
    }

    /**
     * 设置每页最大的数目不超过100且每页数量大于0
     *
     * @param pageSize
     */
    public void setPageSize(Long pageSize) {
        if (pageSize != null && pageSize > 0 && pageSize <= PAGE_MAX_SIZE){
            this.pageSize = pageSize;
        }
    }

    /**
     * 为数据库查询 获取偏移值（从0开始)
     */
    @JsonIgnore
    public long getOffset() {
        return (pageIndex - 1) * pageSize;
    }


    private static final long serialVersionUID = 1L;

}
