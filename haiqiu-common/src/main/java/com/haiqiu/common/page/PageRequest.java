package com.haiqiu.common.page;



import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author: HaiQiu
 * @Date: 2021/3/31
 * @Description: 数据请求封装的实体
 */
@Schema(description = "数据请求封装的实体")
public class PageRequest<T> extends Page implements Serializable {

    @Schema(description = "数据对象实体")
    private T params;

    public PageRequest() {
        super();
    }

    public PageRequest(T params) {
        super();
        this.params = params;
    }

    public PageRequest(Long pageIndex, Long pageSize) {
        super(pageIndex, pageSize);
    }

    public PageRequest(Long pageIndex, Long pageSize, T params) {
        super(pageIndex, pageSize);
        this.params = params;
    }

    public T getParams() {
        return params;
    }

    public void setParams(T params) {
        this.params = params;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("RequestPageDTO{");
        stringBuilder.append("pageIndex=").append(pageIndex)
                .append(",pageSize=").append(pageSize)
                .append(",params=").append(params)
                .append("}");
        return stringBuilder.toString();
    }
}
