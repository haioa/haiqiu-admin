package com.haiqiu.common.page;



import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * @author: HaiQiu
 * @Date: 2021/3/31
 * @Description: 响应数据实体
 */
@Schema(description = "响应数据实体")
public class PageResponse<T> extends Page {

    @Schema(description = "总条数")
    private Long total = 0L;

    @Schema(description = "分页总页数")
    private Long pages = 0L;

    @Schema(description = "数据列表")
    private List<T> data;

    public PageResponse() {
        super();
    }

    public PageResponse(Long pageIndex, Long pageSize) {
        super(pageIndex, pageSize);
    }

    /**
     * 计算总页数
     */
    public void calculatePages() {
        long total = getTotal();
        if (total % pageSize == 0) {
            pages = total / pageSize;
        } else {
            pages = total / pageSize + 1;
        }
    }


    /**
     * 获取总数
     * @return
     */
    public Long getTotal() {
        if (total != null) {
            return total;
        }
        return 0L;
    }

    /**
     * 获取总数
     * @param total
     */
    public void setTotal(Long total) {
        this.total = total;
        calculatePages();
    }

    public Long getPages() {
        return pages;
    }

    /**
     * 设置总数
     * @param pages
     */
    public void setPages(Long pages) {
        this.pages = pages;
        calculatePages();
    }


    /**
     * 获取数据
     * @return
     */
    public List<T> getData() {
        return data;
    }

    /**
     * 设置数据
     * @param data
     */
    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public void setPageSize(Long pageSize){
        super.setPageSize(pageSize);
        calculatePages();
    }
}
