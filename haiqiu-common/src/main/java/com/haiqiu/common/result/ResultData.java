package com.haiqiu.common.result;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author : haiqiu
 * @date ：2020/12/21 16:52
 * @description: 统一返回数据格式
 */
@Data
@Accessors(chain = true)
@Schema(description = "统一返回数据格式")
public class ResultData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "响应码")
    private int code;

    @Schema(description = "数据")
    private Object data;

    @Schema(description = "信息")
    private String message;

    public ResultData(){

    }

    public ResultData(int code, String message, Object data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public ResultData(Object data) {
        this.code = ResultCode.OK.getCode();
        this.message = ResultCode.OK.getMessage();
        this.data = data;
    }

    public ResultData(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResultData(String message) {
        this.code = ResultCode.OK.getCode();
        this.message = message;
    }

    public ResultData(ResultCode resultCode) {
        this.code = resultCode.getCode();
        this.message = resultCode.getMessage();
    }



    /**
     * 成功默认返回状态
     */
    public static ResultData ok() {
        return new ResultData(ResultCode.OK);
    }


    /**
     * 成功返回，带消息
     */
    public static ResultData ok(String msg) {
        return new ResultData(msg);
    }

    /**
     * 成功返回，带数据
     */
    public static ResultData ok(Object data) {
        return new ResultData(data);
    }

    /**
     * 返回成功状态返回枚举
     */
    public static ResultData ok(ResultCode resultCode) {
        return new ResultData(resultCode);
    }

    /**
     * 成功返回，消息+数据
     */
    public static ResultData ok(String message,Object data) {
        return new ResultData(ResultCode.OK.getCode(),message,data);
    }

    /**
     * 成功返回，状态码+消息+数据
     */
    public static ResultData ok(int code,String message, Object data) {
        return new ResultData(code,message,data);
    }

    /**
     * 失败默认返回状态
     */
    public static ResultData fail() {
        return new ResultData(ResultCode.FAIL);
    }

    /**
     * 失败返回自定义信息
     */
    public static ResultData fail(String message) {
        return new ResultData(ResultCode.FAIL.getCode(), message);
    }


    /**
     * 返回失败状态自定义消息和状态码
     */
    public static ResultData fail(int code, String message) {
        return new ResultData(code,message);
    }

    /**
     * 返回失败状态返回枚举
     */
    public static ResultData fail(ResultCode resultCode) {
        return new ResultData(resultCode);
    }

}