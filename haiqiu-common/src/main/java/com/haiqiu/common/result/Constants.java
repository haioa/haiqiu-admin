package com.haiqiu.common.result;

/**
 * @author HaiQiu
 * @date 2021/3/29 9:55
 * @desc 框架常量
 **/
public interface Constants {

    /**********************************************通用字段***************************************************/

    /**
     * 超级管理员标识
     */
    String ADMIN = "admin";

    /**
     * redis缓存用户验证码key
     */
    String CAPTCHA_KEY = "haiqiu:captcha:user:code:" ;

    /**
     * redis缓存用户忘记密码产生的检验验证码
     */
    String FORGET_KEY = "haiqiu:forget:user:code:" ;

    /**
     * redis缓存用户token
     */
    String USER_TOKEN = "haiqiu:user:token:";

    /**
     * 缓存redis用户信息
     */
    String REDIS_USER_INFO = "haiqiu:user:info:";

    /**
     * 缓存redis用户socket信息
     */
    String REDIS_USER_SOCKET = "haiqiu:user:socket:";


    /**
     * 缓存redis用户权限和账户信息key
     */
    String REDIS_USER = "haiqiu:user:security:info:";

    /**
     * 缓存redis用户权限key
     */
    String REDIS_USER_AUTH = "haiqiu:user:security:menu:";

    /**
     * 缓存在线用户的前缀key
     */
    String ONLINE_KEY = "haiqiu:user:online:list:";

    /**
     * 缓存系统配置
     */
    String CONFIG = "haiqiu:system:config";


    /**
     * 操作成功
     */
    String SUCCESS = "操作成功";

    /**
     * 操作成功
     */
    String SUCCESS_BATCH = "批量完成%d条数据";

    /**
     * 新增成功
     */
    String SUCCESS_ADD = "新增成功";


    /**
     * 删除成功
     */
    String SUCCESS_DEL = "删除成功";

    /**
     * 复制成功
     */
    String SUCCESS_COPY = "复制成功";

    /**
     * 修改成功
     */
    String SUCCESS_UPDATE = "修改成功";

    /**
     * 重置成功
     */
    String SUCCESS_RESET = "重置成功";

    /**
     * 上传成功
     */
    String SUCCESS_UPLOAD = "上传成功";

    /**
     * 数据ID不能为空
     */
    String ID_EXITS = "数据ID不能为空";

    /**
     * 删除失败，可能数据已经不存在了
     */
    String FAIL_DEL = "删除失败，可能数据已经不存在了";

    /**
     * 修改失败，可能服务器故障或者数据被删了
     */
    String FAIL_UPDATE = "修改失败，可能服务器故障或者数据被删了";

    /**
     * 新增失败，服务器出了点故障！
     */
    String FAIL_ADD = "新增失败，服务器出了点故障";

    /**
     * 增删查改关键字
     */
    String SAVE="新增";
    String DELETE="删除";
    String UPDATE="修改";
    String QUERY="查询";
    String ACTIVE="激活";
    String REGISTER="注册成功";
    String AVATAR="https://zenka.love/api/resource/HaiQiu-Admin/system/logo.png";
    String LOGIN_PATH = "/login";

    /**
     * 清除账户缓存信息
     */
    String CLEAR_REDIS = "清除账户缓存信息错误";

    /**
     * 账户不存在
     */
    String USER_NOT_EXIST = "账户不存在";

    /**
     * 密码错误
     */
    String PASSPWD_ERROR = "密码错误";

    /**
     * 账户禁用
     */
    String USER_DISABLE = "账户禁用";

    /**
     * 数据不存在
     */
    String DATA_EXIT = "数据不存在";


    /**
     * 未选择数据ID
     */
    String ID_NOT = "未选择数据ID";

    /**
     * 性别（0保密）
     */
    Short SEX_SECRET = 0;

    /**
     * 性别（1男）
     */
    Short SEX_MAN = 1;

    /**
     * 性别（2女）
     */
    Short SEX_WOMAN = 2;

    /**
     * 初始默认浏览量
     */
    Long DEFAULT_VIEW = 0L;

    /**
     * 激活
     */
    Boolean ON = true;
    Byte ENABLE = 1;
    /**
     * 关闭
     */
    Boolean OFF = false;
    Byte DISABLE = 0;
    /**
     * 菜单类型：0目录，1菜单，2按钮
     */
    Byte MENU_TYPE_CATALOGUE = 0;
    Byte MENU_TYPE_MENU = 1;
    Byte MENU_TYPE_BUTTON = 2;
    /**
     * 顶级菜单parentId默认值
     */
    long MENU_PARENT_ID = 0;

    /**
     * 参数错误
     */
    String PARAMETER_ERROR = "参数不齐全，请检查接口参数";

    /**
     * 参数错误
     */
    String PARAMETER_ERROR_OBJECT = "参数检测获取错误";

    /**
     * 默认系统配置ID
     */
    int CONFIG_ID = 1;

    /**
     * 文件上传类型
     */
    String OSS = "oss";
    String FTP = "ftp";
    String QINIU = "qiniu";
    String SERVER = "server";

    /**
     * 文件排序规则
     */
    String SIZE_ASC = "sizeAsc";
    String SIZE_DESC = "sizeDesc";
    String DATE_ASC = "dateAsc";
    String DATE_DESC = "dateDesc";

    /**
     * 时间格式化到年月日时分秒
     */
    String TIME_YMDHMS = "yyyy-MM-dd hh:mm:ss";

    /**
     * 系统相关
     */
    String WINDOWS = "Windows";
    String LINUX = "Linux";

    /**
     * 初始化配置
     */
    String DEFAULT_ROLE = "初始角色";
    String DEFAULT_DEPT = "默认部门";

    /**
     * 邮件状态
     */
    //草稿
    Integer EMAIL_DRAFT = 0;
    //准备发送
    Integer EMAIL_PREPARE = 1;
    //发送失败
    Integer EMAIL_FAIL = 2;
    //发送成功
    Integer EMAIL_SEND = 3;
}
