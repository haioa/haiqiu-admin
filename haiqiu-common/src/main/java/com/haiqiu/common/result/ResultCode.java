package com.haiqiu.common.result;

/**
 * @author HaiQiu
 * 错误码枚举
 */

public enum ResultCode {

    /**
     * 账户
     */
    ACCOUNT_CANCELLED(5001,"账户已注销"),
    USER_ACCOUNT_EXPIRED(5002,"用户帐户已过期"),
    USER_ACCOUNT_FAIL_LOGIN(5003,"用户帐户在其他地方登录"),
    NOT_LOGIN(5004,"账户未登录"),
    /**
     * 系统
     */
    OK(200, "成功"),
    BAD_REQUEST(400, "错误请求"),
    UNAUTHORIZED(401, "未经授权"),
    PAYMENT_REQUIRED(402, "需要付款"),
    FORBIDDEN(403, "禁止访问"),
    NOT_FOUND(404, "资源未找到"),
    METHOD_NOT_ALLOWED(405, "请求方法不允许使用"),
    INVALID_PARAMETER(420,"无效参数"),
    FAIL(500, "服务器错误");

    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResultCode{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }


    /**
     * 根据code获取message
     *
     * @param code 状态码
     * @return 消息
     */
    public static String getMessage(Integer code) {
        for (ResultCode ele : values()) {
            if (ele.getCode().equals(code)) {
                return ele.getMessage();
            }
        }
        return null;
    }
}