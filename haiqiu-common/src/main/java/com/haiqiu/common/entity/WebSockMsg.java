package com.haiqiu.common.entity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author HaiQiu
 * @date 2021/10/18
 * @desc 实时通讯消息体
 */
@Data
@Schema(description = "实时通讯消息")
public class WebSockMsg {
    //来源
    @Schema(description = "来源")
    private String from;

    //目标
    @Schema(description = "目标")
    private String to;

    //消息创建时间
    @Schema(description = "消息创建时间")
    private String createTime;

    //消息类型int类型(0:text、1:image、2:voice、3:video、4:music、5:news、6: online)
    @Schema(description = "消息类型int类型(0:text、1:image、2:voice、3:video、4:music、5:news、6: online)")
    private int msgType;

    //聊天类型int类型(0:未知,1:公聊,2:私聊)
    @Schema(description = "聊天类型int类型(0:未知,1:公聊,2:私聊)")
    private int chatType;

    //群组id仅在chatType为(1)时需要,String类型
    @Schema(description = "群组id仅在chatType为(1)时需要,String类型")
    private String groupId;

    //内容
    @Schema(description = "内容")
    private String content;

    //扩展字段,JSON对象格式如：{'扩展字段名称':'扩展字段description'}
    @Schema(description = "扩展字段,JSON对象格式如：{'扩展字段名称':'扩展字段description'}")
    private Object extras;

}
