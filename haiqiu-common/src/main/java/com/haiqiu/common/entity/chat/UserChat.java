package com.haiqiu.common.entity.chat;

import lombok.Data;

/**
 * @author HaiQiu
 * @date 2021/10/26
 * @desc 聊天用户
 */
@Data
public class UserChat {
    private String id;
    private String img;
    private String name;
    private String dept;
    private Integer readNum;
}
