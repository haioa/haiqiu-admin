package com.haiqiu.common.entity;



import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author haiqiu
 */
@Data
@Schema(description = "服务器文件")
public class FileServer {
    /**
     * 文件名
     */
    private String filename;
    /**
     * 文件类型
     */
    private String type;
    /**
     * 当前路径列表
     */
    private String parent;
    /**
     * 是否可读
     */
    private Boolean canRead;
    /**
     * 是否可写
     */
    private Boolean canWrite;
    /**
     * 文件或者文件夹本身的路径
     */
    private String absolutePath;
    /**
     * 文件本身所在路径
     */
    private String path;
    /**
     * 检查文件或目录路径是否是绝对路径
     */
    private Boolean isAbsolute;
    /**
     * 对象是否是文件夹
     */
    private Boolean isDirectory;
    /**
     * 对象是否是文件
     */
    private Boolean isFile;
    /**
     * 对象是否是隐藏文件或文件夹
     */
    private Boolean isHidden;
    /**
     * 对象最后修改时间
     */
    private String lastModifiedTime;
    /**
     * 对象文件或者目录大小
     */
    private String size;
    /**
     * 对象文件夹是否为空
     */
    private Boolean folderIsNull;
    /**
     * 文件url地址
     */
    private String url;
    /**
     * 上级目录
     */
    private String back;



}
