package com.haiqiu.common.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

/**
 * sys_user
 *
 * @author haiqiu
 */
@Schema(description = "系统用户")
@Data
@ExcelTarget("user")
public class CurrentUser implements Serializable {
    /**
     * 主键ID
     */
    @Schema(description = "主键ID")
    private Long id;

    /**
     * 部门ID
     */
    @Schema(description = "部门ID")
    @NotNull(message = "没有选择部门")
    private Long deptId;

    /**
     * 用户名
     */
    @Excel(name = "用户名",width = 20,orderNum = "0")
    @Schema(description = "用户名")
    @NotBlank(message = "用户名不能为空")
    @Length(max = 20, message = "用户名不能超过20个字符")
    private String username;

    /**
     * 昵称
     */
    @Excel(name = "昵称",width = 20,orderNum = "1")
    @Schema(description = "昵称")
    @NotEmpty(message = "昵称不能为空")
    @Length(max = 20, message = "昵称不能超过20个字符")
    private String nickname;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱",orderNum = "2",width = 25)
    @Schema(description = "邮箱")
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式不对")
    private String email;

    /**
     * 手机
     */
    @Excel(name = "手机",orderNum = "3",width = 25)
    @Schema(description = "手机")
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String phone;

    /**
     * 性别（0保密，1男，2女）
     */
    @Excel(name = "性别",replace = {"男_1","保密_0","女_2"},orderNum = "4")
    @Schema(description = "性别（0保密，1男，2女）")
    @NotNull(message = "性别不能为空")
    private Short sex;

    /**
     * 头像地址
     */
    @Schema(description = "头像地址")
    private String avatar;

    /**
     * 最后登录时间
     */
    @Schema(description = "最后登录时间", hidden = true)
    private Date loginTime;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Short sort;

    /**
     * 是否开启（true激活，false禁用）
     */
    @Excel(name = "状态",replace = {"激活_true","冻结_false"},orderNum = "6")
    @Schema(description = "是否开启（true激活，false禁用）")
    private Boolean active;

    /**
     * 备注
     */
    @Excel(name = "备注",orderNum = "7")
    @Schema(description = "备注")
    private String remark;

    /**
     * 逻辑删除（false未删除，true已删除）
     */
    @Schema(description = "逻辑删除（false未删除，true已删除）", hidden = true)
    private Boolean del;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间",exportFormat = "yyyy-MM-dd hh:mm:ss",orderNum = "9")
    @Schema(description = "创建时间", hidden = true)
    private Date createTime;

    /**
     * 修改时间
     */
    @Excel(name = "修改时间",exportFormat = "yyyy-MM-dd hh:mm:ss",orderNum = "10")
    @Schema(description = "修改时间", hidden = true)
    private Date updateTime;

}