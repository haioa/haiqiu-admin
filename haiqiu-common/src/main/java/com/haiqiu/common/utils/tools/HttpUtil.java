package com.haiqiu.common.utils.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StreamUtils;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * http请求工具类
 * @author HaiQiu
 */
public class HttpUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class);

    /**
     * HTTP封装
     *
     * @param urlStr      需要访问的Url
     * @param method      请求方法 Constants.GET
     * @param encoding    编码  Constants.UTF8
     * @param property    请求头
     * @param param       请求参数
     * @param connectTime 连接超时时间 单位毫秒
     * @param readTime    读取数据超时时 单位毫秒
     * @return 结果
     */
    public static String request(String urlStr, String method, String encoding, Map<String,
            String> property, Map<String, String> param, int connectTime, int readTime) {
        HttpURLConnection conn = null;
        try {
            // 定义需要访问的地址
            URL url = new URL(urlStr);
            // 连接URL
            conn = (HttpURLConnection) url.openConnection();
            // 设置请求方式---默认GET---GET/POST/HEAD/OPTIONS/PUT/DELETE/TRACE
            conn.setRequestMethod(method);
            // 设置请求头
            if (property != null && property.size() > 0) {
                for (Map.Entry<String, String> pro : property.entrySet()) {
                    conn.setRequestProperty(pro.getKey(), pro.getValue());
                }
            }
            // 设置请求参数
            if (param != null && param.size() > 0) {
                // 是否携带参数---默认false
                conn.setDoOutput(true);
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<String, String> par : param.entrySet()) {
                    sb.append("&").append(par.getKey()).append("=").append(par.getValue());
                }
                conn.getOutputStream().write(sb.substring(1).toString().getBytes(encoding));
//                LOGGER.info("请求参数：" + sb.substring(1));
            }
            if (connectTime > 0) {
                // 设置连接超时时间，单位毫秒
                conn.setConnectTimeout(connectTime);
            }
            if (connectTime > 0) {
                // 设置读取数据超时时间，单位毫秒
                conn.setReadTimeout(readTime);
            }
            //发送请求到服务器
            conn.connect();
            // 获取远程响应的内容
            String responseContent = StreamUtils.copyToString(conn.getInputStream(), Charset.forName(encoding));
            return responseContent;
        } catch (Exception e) {
//            LOGGER.error("HttpUtils==>通过url打开一个连接出现异常:" + e);
//            System.err.println("HttpUtils==>通过url打开一个连接出现异常:" + e);
        } finally {
            if (conn != null) {
                // 关闭连接
                conn.disconnect();
            }
        }
//        LOGGER.error("HttpUtils==>请求为空");
        return "";
    }
}
