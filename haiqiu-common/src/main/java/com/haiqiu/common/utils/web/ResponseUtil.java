package com.haiqiu.common.utils.web;

import com.alibaba.fastjson.JSON;
import com.haiqiu.common.result.ResultData;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName ResponseUtil
 * @Description 响应工具类
 * @Author haiqiu
 */
public class ResponseUtil {
    public static void out(HttpServletResponse response, ResultData resultData) throws IOException {

        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(resultData));
        response.getWriter().flush();
    }
}
