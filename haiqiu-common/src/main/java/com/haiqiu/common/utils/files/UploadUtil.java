package com.haiqiu.common.utils.files;

import lombok.SneakyThrows;
import com.haiqiu.common.exception.BaseException;
import com.haiqiu.common.result.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author haiqiu
 * 文件上传总方法
 */
@Component
public class UploadUtil {
    @Autowired
    private OssUtil ossUtil;

    @Autowired
    private ServerFileUtil serverFileUtil;

    @Autowired
    private FtpUtil ftpUtil;

    private static final String PATH_DEFAULT = "HQ-ADMIN";

    /**
     * 批量上传文件
     * @param file 批量文件
     * @param mode 上传模式
     * @param folder 所属文件夹
     * @return 文件url地址
     */
    @SneakyThrows
    public String upload(MultipartFile file, String mode, String folder) {
        folder = folder == null ? PATH_DEFAULT : folder;
            String url = null;
            if (Constants.OSS.equals(mode)) {
                url = ossUtil.upload(file, folder);
            } else if (Constants.SERVER.equals(mode)) {
                url = serverFileUtil.upload(file, folder);
            } else if (Constants.FTP.equals(mode)) {
                url = ftpUtil.uploadFile(file, folder);
            } else if (Constants.QINIU.equals(mode)) {

            }else {
                throw new BaseException("上传模式错误");
            }
        return url;
    }

}
