/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : haiqiu_admin

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 16/04/2022 00:14:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '配置名称',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '系统标题',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '系统logo',
  `side_theme` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '侧边栏主题 深色主题theme-dark，浅色主题theme-light',
  `color` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '主题颜色',
  `bottom` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '网站底部信息（授权信息）',
  `show_settings` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否系统布局配置（true是，false否）',
  `top_nav` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示顶部导航（true是，false否）',
  `tags_view` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示 tagsView（true是，false否）',
  `fixed_header` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否固定头部（true是，false否）',
  `sidebar_logo` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示logo（true是，false否）',
  `dynamic_title` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否显示动态标题（true是，false否）',
  `many_login` tinyint(1) DEFAULT NULL COMMENT '是否多端登录（true是，false否）',
  `status` int NOT NULL DEFAULT '1' COMMENT '状态（0禁用，1激活）',
  `sort` int DEFAULT NULL COMMENT '排序字段',
  `register` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开放注册账号（true是，false否）',
  `token_time` bigint NOT NULL DEFAULT '6000' COMMENT '用户token令牌缓存时间',
  `background` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '登录页面背景图',
  `captcha` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否开启验证码登录（true是，false否）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
BEGIN;
INSERT INTO `sys_config` (`id`, `name`, `title`, `logo`, `side_theme`, `color`, `bottom`, `show_settings`, `top_nav`, `tags_view`, `fixed_header`, `sidebar_logo`, `dynamic_title`, `many_login`, `status`, `sort`, `register`, `token_time`, `background`, `captcha`, `create_time`, `update_time`) VALUES (1, '默认', 'HQ-管理系统', 'http://localhost:8888/resource/HaiQiu-Admin/system/logo.png', 'theme-dark', '#1296db', 'Copyright <a target=\"_blank\" href=\"http://beian.miit.gov.cn\">黔ICP备2021003475号-1</a> © 2021 zenka.com All Rights Reserved.', 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 86400, 'https://zenka.com/api/resource/HaiQiu-Admin/system/c89083824f074c0bad71a976de306a83.jpg', 0, '2021-09-16 15:51:27', '2021-09-16 15:51:29');
COMMIT;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `dept_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门名称',
  `cover` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门封面',
  `leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门负责人',
  `phone` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门联系电话',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门邮箱',
  `parent_id` bigint DEFAULT NULL COMMENT '父级ID',
  `desc` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '部门简介',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT NULL COMMENT '是否激活（true开启，false关闭）',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '逻辑删除（false未删除，true已删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
BEGIN;
INSERT INTO `sys_dept` (`id`, `dept_name`, `cover`, `leader`, `phone`, `email`, `parent_id`, `desc`, `sort`, `status`, `is_delete`, `create_time`, `update_time`) VALUES (1, 'XX科技集团', 'https://zenka.com/resource/1109120210815012412.png', 'haiqiu', '15100300111', 'xxx@gmail.com', 0, 'HQ-后台系统部门集合', 1, 1, 0, '2021-04-07 22:00:22', '2021-09-15 14:52:03');
COMMIT;

-- ----------------------------
-- Table structure for sys_email
-- ----------------------------
DROP TABLE IF EXISTS `sys_email`;
CREATE TABLE `sys_email` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `subject` varchar(200) CHARACTER SET utf8 COLLATE utf8_croatian_ci NOT NULL COMMENT '主题',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '内容',
  `to_who` mediumtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '收件人【数组多个】',
  `cc_peoples` mediumtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '抄送人【数组多个】',
  `bcc_peoples` mediumtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '密送人【数组多个】',
  `attachments` text CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '附件',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `status` tinyint DEFAULT NULL COMMENT '0草稿，1发送成功，2发送失败',
  `send_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '发送邮件类型：text发送文本，html发送Html',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci ROW_FORMAT=DYNAMIC COMMENT='系统邮箱表';

-- ----------------------------
-- Records of sys_email
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件名称',
  `path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件地址',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件类型',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件大小',
  `file_flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '文件标识',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统文件表';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `operation` varchar(500) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '接口名称',
  `request_param` longtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '请求参数',
  `response_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `request_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '来源IP',
  `request_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '来源地址',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '用户名',
  `rest_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '访问接口',
  `result` longtext CHARACTER SET utf8 COLLATE utf8_croatian_ci COMMENT '接口返回消息',
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '客户端操作系统',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '请求方法',
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '客户端浏览器',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci ROW_FORMAT=DYNAMIC COMMENT='系统日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `request_param` varchar(1000) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '请求参数',
  `response_json` varchar(2000) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '响应参数',
  `request_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '来源IP',
  `request_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '来源地址',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '用户名',
  `rest_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '访问接口',
  `result` varchar(200) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '接口返回消息',
  `os` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '客户端操作系统',
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_croatian_ci DEFAULT NULL COMMENT '客户端浏览器',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1089 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_croatian_ci ROW_FORMAT=DYNAMIC COMMENT='系统登录日志表';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
BEGIN;
INSERT INTO `sys_login_log` (`id`, `request_param`, `response_json`, `request_ip`, `request_address`, `username`, `rest_url`, `result`, `os`, `browser`, `create_time`, `update_time`) VALUES (1086, 'LoginDto(username=admin, password=d1ff1ec86b62cd5f3903ff19c3a326b2, captcha=null, key=a0a47c15-927a-42c2-a9b4-33c4026aec29)', 'com.alibaba.druid.support.http.WebStatFilter$StatHttpServletResponseWrapper@6fb2bda7', '127.0.0.1', 'XX-XX-内网IP 内网IP', 'admin', 'http://localhost:8888/login', '密码错误', 'Mac', 'Chrome', '2022-04-15 23:53:12', '2022-04-15 23:53:12');
INSERT INTO `sys_login_log` (`id`, `request_param`, `response_json`, `request_ip`, `request_address`, `username`, `rest_url`, `result`, `os`, `browser`, `create_time`, `update_time`) VALUES (1087, 'LoginDto(username=admin, password=21232f297a57a5a743894a0e4a801fc3, captcha=null, key=b4decd5c-5dad-4f7c-9dcc-cf55173916dd)', '{token=eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NTA0Njk5OTYsInVzZXJJZCI6MSwiaGFpcWl1IjoiYWRtaW4ifQ.Sxm0s4N8AoRWTlDI2iRzHphEW8cHKBXKNwD-Cd9MdZMNLwSgW-kEdwp6n50Up_yx-Wk-Dk5RZwujGetUejRykA}', '127.0.0.1', 'XX-XX-内网IP 内网IP', 'admin', 'http://localhost:8888/login', '登录成功', 'Mac', 'Chrome', '2022-04-15 23:53:17', '2022-04-15 23:53:17');
INSERT INTO `sys_login_log` (`id`, `request_param`, `response_json`, `request_ip`, `request_address`, `username`, `rest_url`, `result`, `os`, `browser`, `create_time`, `update_time`) VALUES (1088, 'LoginDto(username=admin, password=21232f297a57a5a743894a0e4a801fc3, captcha=null, key=7d71370c-47b9-48d8-9ddb-a0c298f056bb)', '{token=eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2NTA0NzA0MzQsInVzZXJJZCI6MSwiaGFpcWl1IjoiYWRtaW4ifQ.OH65nXMxbgL9gAoF4ehbZzR_vX2uLA5BQab_tBNAw4FOghkMyxhKDgeKLPK-g4kLMrqKa-GZgm-5MEY31MoRtw}', '127.0.0.1', 'XX-XX-内网IP 内网IP', 'admin', 'http://localhost:8888/login', '登录成功', 'Mac', 'Chrome', '2022-04-16 00:00:35', '2022-04-16 00:00:35');
COMMIT;

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '公告标题',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '公告内容',
  `sort` int DEFAULT NULL COMMENT '排序',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发布者ID',
  `view` bigint unsigned DEFAULT '0' COMMENT '阅读量',
  `cover` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '封面图片',
  `type` int DEFAULT NULL COMMENT '0公告，1通知，2日志',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `status` tinyint DEFAULT NULL COMMENT '状态：（0关闭，1正常）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parent_id` bigint DEFAULT NULL COMMENT '父级ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限名称（菜单名称）',
  `value` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限值',
  `icon` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限图标',
  `path` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由地址',
  `active` tinyint(1) DEFAULT NULL COMMENT '是否开启（true激活，false禁用）',
  `desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限描述',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '请求方法',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由组件',
  `type` tinyint DEFAULT NULL COMMENT '类型：0目录。1菜单，2按钮',
  `is_frame` int DEFAULT '0' COMMENT '是否为外链（0否 1是）',
  `is_cache` int DEFAULT '0' COMMENT '是否缓存（0不缓存 1缓存）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统权限菜单表';

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (1, 0, '系统监控', 'home', 'dashboard', '/dashboard', 1, '系统监控', NULL, 1, '#', 0, 1, 0, '2021-03-25 21:16:12', '2021-09-19 13:05:38');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (2, 0, '系统管理', 'system', 'system', '/system', 1, '系统管理：包含许多的操作系统功能', NULL, 0, '#', 0, 1, 0, '2021-08-18 14:29:45', '2021-10-11 20:57:08');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (3, 2, '用户管理', 'user', 'user', 'user', 1, '用户管理', NULL, 0, 'system/user', 1, 1, 0, '2021-08-18 14:29:45', '2021-09-23 15:45:09');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (5, 2, '角色管理', 'role:list', 'post', 'role', 1, '角色管理', NULL, 1, 'system/role', 1, 1, 0, '2021-09-17 21:34:28', '2021-09-28 10:10:35');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (7, 1, '全局预览', 'notice:statistics', 'chart', 'home', 1, '公告内容可视化', NULL, 0, 'system/home', 1, 1, 0, '2021-08-18 14:29:45', '2021-10-26 16:39:22');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (12, 3, '用户删除', 'user:del', '#', '/user', 1, '用户删除', 'DELETE', 10, NULL, 2, 1, 0, '2021-07-26 12:57:00', '2021-09-13 13:10:50');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (13, 3, '用户新增', 'user:add', 'tree', '/user', 1, '用户新增', 'POST', 11, NULL, 2, 1, 0, '2021-07-26 12:58:33', '2021-09-12 17:35:39');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (15, 0, '高级功能', 'senior', 'international', '/senior', 1, '高级功能', NULL, 11, '#', 0, 1, 0, '2021-08-02 20:33:03', '2021-11-02 11:08:05');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (16, 2, '菜单管理', 'menu:tree:list', 'dict', 'menu', 1, '权限管理', NULL, 3, 'system/menu/index', 1, 1, 0, '2021-08-10 12:38:18', '2021-09-16 14:16:51');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (17, 16, '权限添加', 'auth:add', 'system', '/auth', 1, '权限添加', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-09-12 17:36:58');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (18, 16, '权限修改', 'auth:upate', 'monitor', '/auth', 1, '权限修改', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-09-12 17:37:05');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (19, 16, '权限删除', 'auth:delete', 'tool', '/auth', 1, '权限删除', 'DELETE', 0, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-09-12 17:38:04');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (20, 16, '令牌获取菜单', 'auth:menu', '#', '/auth/menu', 1, '菜单权限', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-10-08 17:50:54');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (21, 16, '菜单树形', 'auth:tree', 'user', '/auth/tree', 1, '树形菜单', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-09-12 17:38:15');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (22, 3, '用户修改', 'user:update', 'system', '/user', 1, '修改用户信息', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 14:16:42', '2021-09-12 17:35:14');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (23, 3, '用户查询', 'user:qurey', 'monitor', '/user', 1, '用户查询', 'GET', 0, NULL, 2, 1, 0, '2021-08-10 14:17:50', '2021-09-12 17:35:21');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (25, 16, '权限查询', 'auth:query', 'monitor', '/auth?id=', 1, '权限查询', 'GET', 0, NULL, 2, 1, 0, '2021-08-10 14:20:31', '2021-09-12 17:38:28');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (26, 5, '角色修改', 'role:upate', 'system', '/role', 1, '角色编辑', 'PUT', 0, NULL, 2, 1, 0, '2021-11-02 17:38:41', '2021-09-12 17:35:53');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (28, 2, '公告管理', 'notice:list', 'monitor', 'notice', 1, '公告管理', NULL, 4, 'system/notice', 1, 1, 0, '2021-08-10 14:53:40', '2021-09-19 14:35:12');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (29, 28, '新增', 'notice:add', 'tool', '/notice', 1, '公告新增', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 14:54:26', '2021-08-17 19:11:12');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (30, 28, '编辑', 'notivce:update', 'guide', '/notice', 1, '公告编辑', 'PUT', NULL, NULL, 2, 1, 0, '2021-08-10 14:55:32', '2021-08-17 19:11:19');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (31, 28, '删除', 'notice:delete', 'system', '/notice', 1, '公告删除', 'DELETE', NULL, NULL, 2, 1, 0, '2021-08-10 14:56:05', '2021-08-17 19:11:25');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (32, 28, '查询', '/notice/query', 'monitor', '/notice', 1, '公告查询', 'GET', NULL, NULL, 2, 1, 0, '2021-08-10 14:57:13', '2021-08-17 19:11:29');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (34, 1, '登录日志', 'login_log', 'monitor', 'login_log', 1, '登录日志', NULL, 6, 'monitor/logininfor/index', 1, 1, 0, '2021-08-10 15:01:49', '2021-09-24 11:21:42');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (36, 15, '邮件服务', 'email:list', 'guide', 'email', 1, '邮件服务', NULL, 1, 'system/email/index', 1, 1, 0, '2021-08-10 15:04:14', '2021-10-28 21:48:42');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (37, 170, '接口文档', 'swagger-ui', 'select', 'swagger', 1, '接口文档', 'GET', 1, 'tool/swagger', 1, 1, 0, '2021-08-10 15:05:19', '2021-11-02 11:07:14');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (38, 16, '权限激活', 'auth:active', 'system', '/auth/active', 1, '激活权限', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 15:08:38', '2021-09-12 17:37:59');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (39, 3, '获取用户', 'user:info', 'monitor', '/user/info', 1, '获取用户信息', 'GET', NULL, NULL, 2, 1, 0, '2021-08-10 15:14:04', '2021-08-17 19:08:22');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (40, 41, '树形', 'dept:tree', ' el-icon-files', '/dept/tree', 1, '树形部门', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 15:20:54', '2021-08-18 01:40:42');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (41, 2, '部门管理', 'dept', 'cascader', 'dept', 1, '部门管理', NULL, 2, 'system/dept', 1, 1, 0, '2021-08-10 15:21:30', '2021-09-16 14:07:11');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (42, 5, '所有角色', '/role/all', '#', '/role/all', 1, '所有角色', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 15:23:11', '2021-09-13 11:53:44');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (43, 3, '用户分页', 'user:list', 'monitor', '/user/list', 1, '用户分页', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 16:46:46', '2021-09-12 17:35:27');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (44, 5, '角色新增', 'role:add', 'tool', '/role', 1, '角色新增', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 18:48:46', '2021-09-12 17:36:06');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (45, 5, '角色删除', 'role:delete', 'guide', '/role', 1, '角色删除', 'DELETE', 0, NULL, 2, 1, 0, '2021-08-10 18:50:11', '2021-09-12 17:36:11');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (46, 5, '角色查询', 'role:query', 'user', '/role', 1, '角色查询', 'GET', 0, NULL, 2, 1, 0, '2021-08-10 18:51:27', '2021-09-12 17:36:18');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (47, 5, '角色分页', 'role:list', 'peoples', '/role/list', 1, '角色分页列表', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 18:52:39', '2021-09-12 17:36:24');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (48, 5, '角色激活', 'role:active', 'tree-table', '/role/active', 1, '角色激活', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 18:54:10', '2021-09-12 17:36:30');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (49, 3, '用户激活', 'user:active', 'tree', '/user/active', 1, '用户激活', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 18:55:05', '2021-09-12 17:35:01');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (50, 28, '分页', 'notice:list', 'post', '/notice/list', 1, '公告分页列表', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 18:56:21', '2021-08-17 19:11:36');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (51, 16, '权限分页', 'auth:tree:list', 'dict', '/auth/tree/list', 1, '权限分页列表', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 19:28:24', '2021-09-12 17:38:21');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (52, 28, '增加阅读数', 'notice:view', 'system', '/notice/view', 1, '增加阅读数公告', 'PUT', NULL, NULL, 2, 1, 0, '2021-08-10 19:44:40', '2021-08-17 19:11:43');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (53, 3, '头像上传', 'user:avatar', 'monitor', '/user/avatar', 1, '头像上传接口)', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 19:46:41', '2021-08-17 19:08:40');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (54, 3, '所有用户', 'user:all', 'tool', '/user/all', 1, '查询所有(用户)', 'POST', 0, NULL, 2, 1, 0, '2021-08-10 19:48:09', '2021-09-11 15:49:30');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (55, 16, '权限查询', 'auth:query', 'guide', '/auth', 1, '权限修改', 'GET', NULL, NULL, 2, 1, 0, '2021-08-10 12:38:18', '2021-08-10 19:22:31');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (59, 36, '新增', 'mail:add', 'peoples', '/email', 1, '新增邮箱', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 20:01:44', '2021-08-17 19:13:57');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (60, 36, '分页', 'email:list', 'system', '/email/list', 1, '模糊分页', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 20:02:32', '2021-08-17 19:14:03');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (61, 36, '删除', 'email:delete', '#', '/email', 1, '批量删除', 'DELETE', 0, NULL, 2, 1, 0, '2021-08-10 20:03:20', '2021-10-29 16:22:57');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (62, 36, '查询', 'email:query', 'tool', '/email', 1, '查询单条', 'GET', NULL, NULL, 2, 1, 0, '2021-08-10 20:04:03', '2021-08-17 19:14:13');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (63, 36, '修改', 'email:update', 'guide', '/email', 1, '修改邮箱', 'PUT', NULL, NULL, 2, 1, 0, '2021-08-10 20:04:52', '2021-08-17 19:14:19');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (65, 41, '新增', 'dept:add', 'peoples', '/dept', 1, '新增（部门）', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 21:26:38', '2021-08-17 19:11:55');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (66, 41, '修改', 'dept:update', 'tree-table', '/dept', 1, '修改（部门）', 'PUT', NULL, NULL, 2, 1, 0, '2021-08-10 21:27:28', '2021-08-17 19:12:02');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (67, 41, '删除', 'dept:delete', 'tree', '/dept', 1, '批量删除（部门）', 'DELETE', NULL, NULL, 2, 1, 0, '2021-08-10 21:28:13', '2021-08-17 19:12:08');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (68, 41, '所有部门', 'dept:all', '#', '/dept/all', 1, '所有部门', 'GET', 0, NULL, 2, 1, 0, '2021-08-10 21:28:54', '2021-09-15 10:55:23');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (69, 41, '分页', 'dept:list ', 'dict', '/dept/list ', 1, '模糊分页（部门）', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 21:29:24', '2021-08-17 19:12:23');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (70, 41, '树形分页', 'dept:tree:list', 'edit', '/dept/tree/list', 1, '树形分页部门', 'POST', NULL, NULL, 2, 1, 0, '2021-08-10 21:54:39', '2021-08-17 19:12:29');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (71, 41, '查询', 'dept:query', '#', '/dept', 1, '部门查询', 'GET', 0, NULL, 2, 1, 0, '2021-08-10 22:24:50', '2021-09-13 11:52:10');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (72, 41, '激活', 'dept:active', '#', '/dept/active', 1, '部门激活', 'PUT', 0, NULL, 2, 1, 0, '2021-08-10 23:34:40', '2021-09-13 11:52:02');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (76, 1, '系统日志', 'log', 'log', 'sys_log', 1, '系统日志', NULL, 7, 'monitor/operlog/index', 1, 1, 0, '2021-08-13 19:28:43', '2021-09-24 11:22:02');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (77, 76, '分页', 'log:list', '#', '/system/log/list', 1, '系统日志分页', 'POST', 0, NULL, 2, 1, 0, '2021-08-13 19:30:09', '2021-09-16 10:08:08');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (78, 76, '删除', 'log', '#', '/system/log', 1, '批量删除', 'DELETE', 0, NULL, 2, 1, 0, '2021-08-13 19:31:33', '2021-09-16 10:08:16');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (79, 76, '查询', 'log', '#', '/system/log', 1, '日志查询', 'GET', 0, NULL, 2, 1, 0, '2021-08-13 19:32:02', '2021-09-16 10:08:25');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (85, 3, '所有邮箱', 'user:emailAll', 'post', '/user/emailAll', 1, '所有用户邮箱', 'POST', NULL, NULL, 2, 1, 0, '2021-08-16 18:34:45', '2021-08-17 19:09:14');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (88, 7, '数据统计', 'notice:statistics', 'dict', '/notice/statistics', 1, '数据统计', 'GET', NULL, NULL, 2, 1, 0, '2021-08-18 01:49:09', '2021-08-18 01:49:45');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (91, 34, '分页', ':login_log:list', '#', '/login/log/list', 1, '登录日志分页', 'POST', 0, NULL, 2, 1, 0, '2021-08-18 13:07:30', '2021-09-16 13:20:23');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (102, 16, '权限分页', '/auth/list', 'tool', '/auth/list', 1, '/auth/list', 'POST', 1, NULL, 2, 1, 0, '2021-09-03 13:48:02', '2021-09-12 17:38:36');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (103, 16, '菜单列表分页', 'menu1', '#', '/auth/menu/list', 1, NULL, 'POST', 1, '', 2, NULL, NULL, '2021-09-10 14:08:45', '2021-10-08 17:50:31');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (104, 16, '所有菜单', '/auth/all', 'user', '/auth/all', 1, NULL, 'POST', 1, '', 2, NULL, NULL, '2021-09-10 16:23:16', '2021-09-10 16:25:14');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (106, 3, '重置密码', 'user:resetPwd', '#', '/user/resetPwd', 1, '用户重置账户密码', 'PUT', 0, NULL, 2, 0, 0, '2021-09-12 21:53:21', '2021-09-13 11:52:32');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (107, 3, '授权角色', '/user./authRole', '#', '/user/authRole', 1, '授权角色授权角色', 'PUT', 0, NULL, 2, 0, 0, '2021-09-12 23:55:09', '2021-09-13 11:52:35');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (108, 16, '角色菜单', '/auth/roleMenuTreeSelect', '#', '/auth/roleMenuTreeSelect', 1, '角色拥有菜单', 'GET', 0, NULL, 2, 0, 0, '2021-09-13 17:28:28', '2021-09-13 17:28:28');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (109, 5, '角色已授权用户列表', '/role/allocatedList', '#', '/role/allocatedList', 1, '根据角色ID查询角色已授权用户列表', 'GET', 10, NULL, 2, 0, 0, '2021-09-14 21:40:06', '2021-09-14 21:40:06');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (110, 5, '角色未授权用户列表', '/role/unallocatedList', '#', '/role/unallocatedList', 1, '根据角色ID查询角色未授权用户列表', 'GET', 11, '/role/unallocatedList', 2, 0, 0, '2021-09-14 22:22:23', '2021-09-14 23:19:43');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (111, 5, '批量用户授权角色', '/role/authorize', '#', '/role/authorize', 1, '批量取消或者批量添加用户授权角色', 'PUT', 12, NULL, 2, 0, 0, '2021-09-14 23:19:11', '2021-09-14 23:19:11');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (112, 15, '系统配置', 'config', 'system', 'config', 1, '系统配置(配置主题色彩，系统常用的配置)', NULL, 5, 'system/config/setting', 1, 0, 0, '2021-09-15 15:13:41', '2021-09-24 11:22:16');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (113, 3, '修改密码', '/user/profile/updatePwd', '#', '/user/profile/updatePwd', 1, '修改密码', 'PUT', 16, NULL, 2, 0, 0, '2021-09-15 16:17:34', '2021-09-15 16:19:04');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (114, 3, '个人修改', '/user/profile', '#', '/user/profile', 1, '用户个人中心修改信息', 'PUT', 15, NULL, 2, 0, 0, '2021-09-15 16:39:53', '2021-09-15 16:39:53');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (115, 34, '删除', '/login/log', '#', '/login/log', 1, '登录日志删除', 'DELETE', 5, NULL, 2, 0, 0, '2021-09-16 13:20:12', '2021-09-16 13:20:12');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (116, 34, '查询', 'login/log', '#', 'login/log', 1, '根据ID查询登录日志内容', 'GET', 6, NULL, 2, 0, 0, '2021-09-16 13:21:38', '2021-09-16 13:21:38');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (117, 112, '查询', '/system/config', '#', '/system/config', 1, '根据ID查询系统配置信息', 'GET', 11, NULL, 2, 0, 0, '2021-09-17 11:15:01', '2021-10-08 21:44:32');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (118, 112, '初始化系统参数', '/system/theme', '#', '/system/theme', 1, '获取初始化系统参数（系统名称，logo，登录背景图等信息）', 'GET', 12, NULL, 2, 0, 0, '2021-09-17 14:00:34', '2021-09-17 14:00:34');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (119, 112, '修改', '/system/config', '#', '/system/config', 1, '修改系统配置', 'PUT', 13, NULL, 2, 0, 0, '2021-09-17 16:12:47', '2021-09-17 16:12:47');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (121, 1, 'serve监控', 'monitor/server/index', 'monitor', 'server', 1, '服务器监控，查看服务器硬件相关信息', NULL, 2, 'monitor/server/index', 1, 0, 0, '2021-09-19 12:30:37', '2021-10-02 16:51:10');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (122, 121, '服务器信息', '/monitor/server', '#', '/monitor/server', 1, '查询服务器详细', 'GET', 1, NULL, 2, 0, 0, '2021-09-19 12:31:37', '2021-09-19 12:31:37');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (123, 1, 'redis监控', 'monitor/cache/index', 'redis', 'redis', 1, '获取redis服务监控的运行信息', NULL, 2, 'monitor/cache/index', 1, 0, 0, '2021-09-19 12:50:34', '2021-10-02 16:47:39');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (124, 123, '查询Redis信息', '/monitor/cache', '#', '/monitor/cache', 1, '获取redis服务信息', 'GET', 1, NULL, 2, 0, 0, '2021-09-19 12:51:44', '2021-09-19 12:51:44');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (125, 1, 'druid监控', 'monitor/druid/index', 'code', 'druid', 1, 'druid监控', NULL, 3, 'monitor/druid/index', 1, 0, 0, '2021-09-19 15:44:44', '2021-10-02 16:47:56');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (126, 3, '同步在线', '/user/online', '#', '/user/online', 1, '同步在线', 'GET', 20, NULL, 2, 0, 0, '2021-09-20 00:02:35', '2021-09-20 00:02:35');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (127, 128, '在线列表', '/user/online/list', '#', '/user/online/list', 1, '在线列表', 'GET', 22, NULL, 2, 0, 0, '2021-09-20 00:25:12', '2021-09-20 18:10:33');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (128, 1, '在线用户', 'online', 'peoples', 'online', 1, '在线用户', NULL, 1, 'monitor/online/index', 1, 0, 0, '2021-09-20 00:44:03', '2021-09-28 10:09:36');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (129, 128, '强制下线', '/user/online', '#', '/user/online', 1, '强制下线', 'DELETE', 18, NULL, 2, 0, 0, '2021-09-20 13:21:22', '2021-09-20 18:10:57');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (130, 3, '退出登录', '/account/logout', '#', '/account/logout', 1, '退出登录', 'POST', 12, '#', 2, 0, 0, '2021-09-20 20:38:09', '2021-09-20 20:44:55');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (132, 112, '图片上传', '/file/theme/upload', '#', '/file/theme/upload', 1, '背景图logo图片上传', 'POST', 5, NULL, 2, 0, 0, '2021-09-22 16:54:59', '2021-09-22 16:54:59');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (133, 3, '个人头像', '/user/profile/avatar', '#', '/user/profile/avatar', 1, '用户管理列表头像上传', 'POST', 15, NULL, 2, 0, 0, '2021-09-22 20:12:10', '2021-09-22 20:14:11');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (134, 2, '职位管理', 'post', 'theme', 'post', 1, '职位管理', NULL, 5, 'system/post/index', 1, 0, 0, '2021-09-23 15:30:25', '2021-09-23 15:30:25');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (135, 134, '新增', '/position', '#', '/position', 1, '新增职位', 'POST', 0, NULL, 2, 0, 0, '2021-09-23 15:31:21', '2021-09-23 15:31:21');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (136, 134, '修改', '/position', '#', '/position', 1, '修改职位', 'PUT', 1, NULL, 2, 0, 0, '2021-09-23 15:31:48', '2021-09-23 15:31:48');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (137, 134, '查询', '/position', '#', '/position', 1, '查询职位', 'GET', 2, NULL, 2, 0, 0, '2021-09-23 15:32:14', '2021-09-23 15:32:14');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (138, 134, '删除', '/position', '#', '/position', 1, '删除职位', 'DELETE', 3, NULL, 2, 0, 0, '2021-09-23 15:32:42', '2021-09-23 15:32:42');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (139, 134, '全部职位', '/position/all', '#', '/position/all', 1, '全部职位', 'GET', 4, NULL, 2, 0, 0, '2021-09-23 15:33:22', '2021-09-23 17:27:35');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (140, 134, '分页', '/position/list', '#', '/position/list', 1, '职位模糊匹配分页', 'POST', 5, NULL, 2, 0, 0, '2021-09-23 15:34:17', '2021-09-23 15:34:17');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (141, 2, '文件管理', '/file', 'list', 'file', 1, '文件管理', NULL, 8, 'system/file/index', 1, 0, 0, '2021-09-24 15:55:07', '2021-09-24 15:55:07');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (142, 141, 'FTP文件列表', '/file/ftp/list', '#', '/file/ftp/list', 1, 'FTP文件列表', 'GET', 0, NULL, 2, 0, 0, '2021-09-24 16:03:36', '2021-09-24 16:05:15');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (143, 141, '服务器文件列表', '/file/server/list', '#', '/file/server/list', 1, '服务器文件列表', 'GET', 2, NULL, 2, 0, 0, '2021-09-24 18:02:44', '2021-09-24 18:02:44');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (144, 3, '用户导出', '/user/export', '#', '/user/export', 1, '用户导出', 'GET', 12, NULL, 2, 0, 0, '2021-09-25 22:47:02', '2021-09-25 22:47:02');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (146, 141, '上一级目录', '/server/back', '#', '/file/server/back', 1, '获取当前目录的上一级目录', 'GET', 2, NULL, 2, 0, 0, '2021-09-27 17:44:29', '2021-09-27 17:48:30');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (148, 141, '服务器下载文件', '/server/download', '#', '/file/server/download', 1, '服务器下载文件', 'GET', 2, NULL, 2, 0, 0, '2021-09-28 15:31:31', '2021-09-28 15:31:31');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (149, 141, '批量删除', '/server/delete', '#', '/file//server/delete', 1, '批量删除服务器文件或文件夹', 'DELETE', 3, NULL, 2, 0, 0, '2021-09-28 17:06:53', '2021-09-28 17:06:53');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (150, 141, '创建文件夹', '/server/folder', '#', '/file/server/folder', 1, '创建文件夹', 'POST', 4, NULL, 2, 0, 0, '2021-09-28 17:32:18', '2021-09-28 17:32:18');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (151, 141, '文件上传', '/file/upload', '#', '/file/upload', 1, '批量文件文件上传', 'POST', 3, NULL, 2, 0, 0, '2021-09-29 10:28:47', '2021-09-29 10:28:47');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (152, 141, '重命名', '/file/server/rename', '#', '/file/server/rename', 1, '服务器重命名文件夹或文件', 'GET', 4, NULL, 2, 0, 0, '2021-09-29 14:13:01', '2021-09-29 14:13:01');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (153, 141, '服务器文件上传', '/server/upload', '#', '/file/server/upload', 1, '服务器批量文件上传', 'POST', 5, NULL, 2, 0, 0, '2021-09-30 16:59:15', '2021-09-30 16:59:15');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (155, 141, '复制文件', 'file/server/copy', '#', '/file/server/copy', 1, '服务器复制文件或文件夹', 'GET', 4, NULL, 2, 0, 0, '2021-10-09 11:05:48', '2021-10-09 11:28:15');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (156, 0, '聊天通讯', NULL, 'qq', '/chat', 1, '聊天通讯', NULL, 3, '#', 0, 0, 0, '2021-10-19 15:11:08', '2021-10-20 11:07:01');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (157, 156, '实时沟通', 'person:chat', 'qq', 'im', 1, '聊天通讯', NULL, 0, 'system/chat/index', 1, 0, 0, '2021-10-19 15:12:50', '2021-10-20 11:08:15');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (169, 157, '用户列表', '/chat/list', '#', '/chat/list', 1, '获取聊天用户列表', 'POST', 0, NULL, 2, 0, 0, '2021-10-26 10:19:16', '2021-10-26 10:19:16');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (170, 0, '系统工具', NULL, 'druid', '/tool', 1, '系统工具', NULL, 7, '#', 0, 0, 0, '2021-11-02 11:02:59', '2021-11-02 11:02:59');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (171, 170, '表单生成', NULL, 'button', 'build', 1, '表单生成', NULL, 0, 'tool/build/index', 0, 0, 0, '2021-11-02 11:05:09', '2021-11-02 11:05:09');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (172, 170, '代码生成', 'tool:gen:list', 'code', 'gen', 1, '代码生成', NULL, 1, 'tool/gen/index', 1, 0, 0, '2021-11-02 11:05:52', '2021-11-02 11:06:11');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (173, 76, '日志统计', '/system/log/statistics', '#', '/system/log/statistics', 1, '日志统计', 'GET', 1, NULL, 2, 0, 0, '2022-04-16 00:04:53', '2022-04-16 00:04:53');
INSERT INTO `sys_permission` (`id`, `parent_id`, `title`, `value`, `icon`, `path`, `active`, `desc`, `method`, `sort`, `component`, `type`, `is_frame`, `is_cache`, `create_time`, `update_time`) VALUES (174, 172, '代码生成接口', '/tool/gen/list', '#', '/tool/gen/list', 1, '代码生成接口', 'GET', 2, NULL, 2, 0, 0, '2022-04-16 00:12:40', '2022-04-16 00:12:40');
COMMIT;

-- ----------------------------
-- Table structure for sys_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_position`;
CREATE TABLE `sys_position` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `position_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '岗位编码',
  `position_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '职位名称',
  `describe` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '职位描述',
  `sort` int DEFAULT NULL COMMENT '排序',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '图标',
  `status` tinyint DEFAULT NULL COMMENT '状态：（0禁用，1激活）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统职位信息表';

-- ----------------------------
-- Records of sys_position
-- ----------------------------
BEGIN;
INSERT INTO `sys_position` (`id`, `position_code`, `position_name`, `describe`, `sort`, `icon`, `status`, `create_time`, `update_time`) VALUES (1, 'CEO', '首席执行官', 'CEO(Chief Executive officer)首席执行官', 0, NULL, 0, '2021-11-02 17:42:51', '2021-11-02 17:42:53');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '标识符',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色名称',
  `desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色描述',
  `icon` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '角色图标',
  `active` tinyint(1) DEFAULT NULL COMMENT '是否开启（true激活，false禁用）',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `del` tinyint(1) DEFAULT NULL COMMENT '逻辑删除（false未删除，true已删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `sign`, `name`, `desc`, `icon`, `active`, `sort`, `del`, `create_time`, `update_time`) VALUES (1, 'super:admin', '超级管理员', '系统初始化', NULL, 1, 0, NULL, '2021-11-02 17:43:53', '2021-11-02 17:43:55');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `permission_id` bigint DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1254 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='角色菜单权限关联表';

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_scheduled
-- ----------------------------
DROP TABLE IF EXISTS `sys_scheduled`;
CREATE TABLE `sys_scheduled` (
  `id` bigint NOT NULL COMMENT '主键ID',
  `cron_key` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '定时任务完整类名',
  `cron_expression` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '''cron表达式',
  `task_explain` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '任务描述',
  `status` tinyint(1) DEFAULT NULL COMMENT '任务状态：1启用，2停止',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `sort` int DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='定时任务';

-- ----------------------------
-- Records of sys_scheduled
-- ----------------------------
BEGIN;
INSERT INTO `sys_scheduled` (`id`, `cron_key`, `cron_expression`, `task_explain`, `status`, `create_time`, `update_time`, `sort`) VALUES (1, 'com.haiqiu.schedule.controller.ScheduleController', '*/5 * * * * ?', '定时任务描述', 1, '2022-03-14 14:47:34', '2022-03-14 14:47:32', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
  `nickname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机',
  `sex` tinyint NOT NULL DEFAULT '0' COMMENT '性别（0保密，1男，2女）',
  `avatar` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '头像地址',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最后登录IP',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '最后登录地理位置',
  `sort` smallint DEFAULT NULL COMMENT '排序',
  `active` tinyint(1) DEFAULT NULL COMMENT '是否开启（true激活，false禁用）',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `del` tinyint(1) DEFAULT NULL COMMENT '逻辑删除（false未删除，true已删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`id`, `dept_id`, `username`, `nickname`, `password`, `email`, `phone`, `sex`, `avatar`, `login_time`, `login_ip`, `login_location`, `sort`, `active`, `remark`, `del`, `create_time`, `update_time`) VALUES (1, 1, 'admin', '超级管理员', '$2a$10$EoL4ceHpWPUEVHs9D59KJOwmtHpsMiuVBkaoa278EClX4RS5fuCPe', 'haiqiu88@gmail.com', '18112341234', 0, 'http://localhost:8888/resource/HaiQiu-Admin/avatar/2022041600_21.png', '2022-04-16 00:12:40', NULL, NULL, 0, 1, '超级管理员（系统初始化）', NULL, '2021-11-02 17:33:58', '2021-11-02 17:34:01');
COMMIT;

-- ----------------------------
-- Table structure for sys_user_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_position`;
CREATE TABLE `sys_user_position` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `position_id` bigint DEFAULT NULL COMMENT '岗位ID',
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='岗位用户关联表';

-- ----------------------------
-- Records of sys_user_position
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_position` (`id`, `position_id`, `user_id`) VALUES (1, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统用户角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES (1, 1, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
