package com.haiqiu.syslog.service;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.syslog.entity.SysLog;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author HaiQiu
 * @ClassName SysLogService.java
 * @Description TODO
 * @createTime 2021年08月13日
 */
public interface SysLogService {

    /**
     * 新增日志
     * @param sysLog 日志实体
     */
    void insert(SysLog sysLog);

    /**
     * 批量删除日志
     * @param ids 日志id集合
     * @return 成功条数
     */
    int delBatch(Set<Long> ids);


    /**
     * 模糊分页查询日志
     * @param request 查询实体
     * @return 返回集合数据
     */
    PageResponse<SysLog> list(PageRequest<SysLog> request);

    /**
     * 日志区域统计
     * @param start 指定开始时间
     * @param end 指定结束时间
     * @return 日志区域统计
     */
    List<Map<String,Object>> statistics(String start, String end);
}
