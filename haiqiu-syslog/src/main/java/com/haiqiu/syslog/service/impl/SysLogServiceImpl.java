package com.haiqiu.syslog.service.impl;

import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.page.PageResponse;
import com.haiqiu.common.utils.tools.StringUtil;
import com.haiqiu.syslog.entity.SysLog;
import com.haiqiu.syslog.mapper.SysLogMapper;
import com.haiqiu.syslog.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author HaiQiu
 * @ClassName SysLogServiceIMpl.java
 * @Description TODO
 * @createTime 2021年08月13日
 */
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogMapper sysLogMapper;


    @Override
    public void insert(SysLog sysLog) {
        sysLog.setCreateTime(new Date());
        sysLog.setUpdateTime(new Date());
        sysLogMapper.insert(sysLog);
    }

    @Override
    public int delBatch(Set<Long> ids) {
        return sysLogMapper.delBatch(ids);
    }

    @Override
    public PageResponse<SysLog> list(PageRequest<SysLog> request) {
        SysLog sysLog = StringUtil.checkObjFieldIsNull(request.getParams());
        PageResponse<SysLog> response = new PageResponse<>();
        long count = sysLogMapper.count(sysLog);
        response.setTotal(count);
        response.setPageSize(request.getPageSize());
        response.setPageIndex(request.getPageIndex());
        if (count > 0){
            List<SysLog> list = sysLogMapper.page(sysLog,request.getOffset(),request.getPageSize());
            response.setData(list);
        }
        return response;
    }

    @Override
    public List<Map<String,Object>> statistics(String start, String end) {
        List<Map<String,Object>> map =  sysLogMapper.statistics(start,end);
        return map;
    }
}
