package com.haiqiu.syslog.controller;

import com.haiqiu.common.controller.BaseController;
import com.haiqiu.common.page.PageRequest;
import com.haiqiu.common.result.ResultData;
import com.haiqiu.syslog.annotation.NotLog;
import com.haiqiu.syslog.entity.SysLog;
import com.haiqiu.syslog.service.SysLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author HaiQiu
 * @ClassName SysLogController.java
 * @Description 日志管理
 * @createTime 2021年08月13日
 */
@Tag(name = "日志管理", description = "日志记录管理")
@RestController
@RequestMapping("/system/log")
public class SysLogController extends BaseController {

    @Autowired
    private SysLogService sysLogService;

    @NotLog
    @Operation(summary = "日志模糊分页")
    @PostMapping("/list")
    public ResultData list(@Parameter(description = "模糊分页可选参数") @RequestBody PageRequest<SysLog> request) {
        return decide(sysLogService.list(request));
    }

    @Operation(summary = "批量删除日志")
    @DeleteMapping("")
    public ResultData delBatch(@RequestBody Set<Long> ids){
        return decide(String.format("已成功删除%d条数据", sysLogService.delBatch(ids)));
    }

    @NotLog
    @Operation(summary = "日志区域统计",description = "不传时间默认查询近期三个月数据")
    @GetMapping("/statistics")
    public ResultData statistics(@Parameter(description = "指定开始时间") @RequestParam(required = false) String start,
                           @Parameter(description = "指定结束时间") @RequestParam(required = false) String end) {
        return decide(sysLogService.statistics(start,end));
    }

}
