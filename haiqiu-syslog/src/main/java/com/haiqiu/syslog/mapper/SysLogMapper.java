package com.haiqiu.syslog.mapper;

import com.haiqiu.common.mapper.BaseMapper;
import com.haiqiu.syslog.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * SysLogMapper继承基类
 * @author HaiQiu
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog, Long> {

    /**
     * 批量删除日志
     * @param ids 日志id集合
     * @return 成功条数
     */
    int delBatch(Set<Long> ids);

    /**
     * 模糊分页总数
     * @param sysLog 查询实体
     * @return 总数
     */
    long count(@Param("sysLog") SysLog sysLog);

    /**
     * 模糊分页
     * @param sysLog 查询实体
     * @param offset 页数
     * @param pageSize 每页大小
     * @return 数据集合
     */
    List<SysLog> page(@Param("sysLog") SysLog sysLog,@Param("offset") long offset,@Param("pageSize") Long pageSize);

    /**
     * 日志区域统计
     * @param start 指定开始时间
     * @param end 指定结束时间
     * @return 日志区域统计
     */
    List<Map<String,Object>> statistics(@Param("start") String start, @Param("end") String end);
}