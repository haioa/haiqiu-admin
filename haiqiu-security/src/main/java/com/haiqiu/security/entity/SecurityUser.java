package com.haiqiu.security.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.*;

/**
 * sys_user
 *
 * @author haiqiu
 */
@Schema(description = "com.gitee.zenka.sysyem.entity.SysUser用户表")
@Data
public class SecurityUser implements UserDetails, Serializable {

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    private String username;


    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;


    /**
     * 是否开启（true激活，false禁用）
     */
    @Schema(description = "是否开启（true激活，false禁用）")
    private Boolean active;


    /**
     * 权限集合
     */
    @Schema(description = "权限集合", hidden = true)
    private Set<GrantedAuthority> authorities;

    private static final long serialVersionUID = 1L;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.getActive();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }



}