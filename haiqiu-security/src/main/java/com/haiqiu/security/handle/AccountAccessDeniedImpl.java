package com.haiqiu.security.handle;

import com.haiqiu.common.utils.web.ResponseUtil;
import com.haiqiu.common.result.ResultCode;
import com.haiqiu.common.result.ResultData;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author HaiQiu
 * @date 2021/4/11 1:56
 * @desc 用来解决认证过的用户访问无权限资源时的异常
 **/
@Component
public class AccountAccessDeniedImpl implements  AccessDeniedHandler {


    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        ResponseUtil.out(httpServletResponse,ResultData.fail(ResultCode.FORBIDDEN));
    }


}
