package com.haiqiu.security.handle;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.haiqiu.common.utils.web.ResponseUtil;
import com.haiqiu.common.result.ResultCode;
import com.haiqiu.common.result.ResultData;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;


/**
 * 认证失败处理类 返回未授权
 * 用来解决匿名用户访问无权限资源时的异常
 * @author ruoyi
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws ServletException, IOException {

        ResponseUtil.out(response,ResultData.fail(ResultCode.UNAUTHORIZED));

    }



}
