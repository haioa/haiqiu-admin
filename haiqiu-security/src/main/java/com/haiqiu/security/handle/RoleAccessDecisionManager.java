package com.haiqiu.security.handle;

import com.haiqiu.common.result.ResultCode;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author HaiQiu
 * @ClassName RoleAccessDecisionManager.java
 * @Description 权限决策（获取当前用户所对应的权限）
 * 有了权限资源，知道了当前访问的url所需要的具体权限，接下来就是决策当前的访问是否能够通过权限验证了。
 * AccessDecisionManager：决策管理器
 * 实现AccessDecisionManager接口，自定义决策管理器。
 * decide()方法有三个参数：
 *
 * authentication： 当前用户对应的信息
 * object：包含客户端发起的请求的requset信息
 * collection 当前路径对应的权限
 * 判断该用户对应的权限信息是否跟当前路径对应的权限信息相等。
 * @createTime 2021年08月09日
 */
@Component
public class RoleAccessDecisionManager implements AccessDecisionManager {

    /**
     * decide 方法是判定是否拥有权限的决策方法，
     * @param authentication 当前用户的信息
     * @param o 包含客户端发起的请求的requset信息
     * @param collection  当前路径对应的权限
     * @throws AccessDeniedException
     * @throws InsufficientAuthenticationException
     */
    @Override
    public void decide(Authentication authentication, Object o, Collection<ConfigAttribute> collection) throws AccessDeniedException, InsufficientAuthenticationException {
        //数据库获取的权限信息
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        //来自客户端的权限信息
        Iterator<ConfigAttribute> iterator = collection.iterator();
        while (iterator.hasNext()){
            ConfigAttribute configAttribute = iterator.next();
            //访问该请求url需要的权限角色信息
            String authValue = configAttribute.getAttribute();
            //如果只是登陆就能访问，即没有匹配到资源信息
            if ("ROLE_ANONYMOUS".equals(authValue)){
                //判断是否登陆，没有登陆则authentication是AnonymousAuthenticationToken接口实现类的对象
                if (authentication instanceof AnonymousAuthenticationToken){
                    throw new BadCredentialsException(ResultCode.NOT_LOGIN.getMessage());
                } else {
                    //否则就是登录了没有权限访问
                    throw new AccessDeniedException(ResultCode.FORBIDDEN.getMessage());
                }
            }
            //如果匹配上了资源信息，就拿登陆用户的权限信息来对比是否存在于已匹配的角色集合中
            for (GrantedAuthority authority : authorities) {
                if (authority.getAuthority().equals(authValue)){
                    return;
                }
            }
        }
        //如果没有匹配上，则权限不足
        throw new AccessDeniedException(ResultCode.FORBIDDEN.getMessage());
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
