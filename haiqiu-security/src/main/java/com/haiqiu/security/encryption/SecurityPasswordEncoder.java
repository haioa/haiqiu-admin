package com.haiqiu.security.encryption;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author HaiQiu
 * @date 2021/4/4 16:27
 * @desc 默认不加密
 **/
@Component
public class SecurityPasswordEncoder implements PasswordEncoder {

    /**
     * 加密
     * @param rawPassword 原密码（前端传入的密码）
     * @return 是否加密成功
     */
    @Override
    public String encode(CharSequence rawPassword) {
        return new BCryptPasswordEncoder().encode(rawPassword);
//        return rawPassword.toString();
    }

    /**
     * 匹配密码
     * @param rawPassword 原密码（前端传入的密码）
     * @param encodedPassword 加密密码(数据库存储的加密密码)
     * @return 是否匹配
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return new BCryptPasswordEncoder().matches(rawPassword,encodedPassword);
//        return rawPassword.toString().equals(encodedPassword);
    }
}
