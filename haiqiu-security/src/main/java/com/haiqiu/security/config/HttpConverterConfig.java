package com.haiqiu.security.config;

import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.web.ServletUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author HaiQiu
 */
@Configuration
public class HttpConverterConfig implements WebMvcConfigurer {

    /**
     * 文件父级存储路径
     */
    @Value("${http.parent:C:\\}")
    private String parent;

    /**
     * 文件资源虚拟映射路径
     */
    @Value("${http.virtual:/resource}")
    private String virtual;

    private static final String DELETE_LINUX_PATH = "/";

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //获取路径分隔符
        String symbol =  ServletUtil.getLocalOs().equals(Constants.WINDOWS) ?  "\\" : "/";
        String folder = parent.equals(DELETE_LINUX_PATH) ? parent : parent + symbol;
        //其中fictitious表示访问的前缀。"file:F:/img/"是文件真实的存储路径
        registry.addResourceHandler(virtual+"/**").addResourceLocations("file:" + folder);

        //过滤swagger
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        registry.addResourceHandler("/swagger-resources/**")
                .addResourceLocations("classpath:/META-INF/resources/swagger-resources/");

        registry.addResourceHandler("/swagger/**")
                .addResourceLocations("classpath:/META-INF/resources/swagger*");

        registry.addResourceHandler("/v2/api-docs/**")
                .addResourceLocations("classpath:/META-INF/resources/v2/api-docs/");
    }

//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        //调用父类的配置
//        WebMvcConfigurer.super.configureMessageConverters(converters);
//        //创建FastJson的消息转换器
//        FastJsonHttpMessageConverter convert = new FastJsonHttpMessageConverter();
//        //创建FastJson的配置对象
//        FastJsonConfig config = new FastJsonConfig();
//        //对Json数据进行格式化
//        config.setSerializerFeatures(SerializerFeature.PrettyFormat,
//                SerializerFeature.WriteNullStringAsEmpty,
//                SerializerFeature.WriteNullNumberAsZero,
//                SerializerFeature.WriteNullListAsEmpty,
//                SerializerFeature.WriteNullBooleanAsFalse,
//                SerializerFeature.WriteMapNullValue,
//                //禁止循环引用
//                SerializerFeature.DisableCircularReferenceDetect);
//        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
//        config.setCharset(Charset.forName("UTF-8"));
//        convert.setFastJsonConfig(config);
//        convert.setSupportedMediaTypes(getSupportedMediaTypes());
//        converters.add(convert);
//    }
//
//    public List<MediaType> getSupportedMediaTypes() {
//        //创建fastJson消息转换器
//        List<MediaType> supportedMediaTypes = new ArrayList<>();
//        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
//        supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
//        supportedMediaTypes.add(MediaType.APPLICATION_ATOM_XML);
//        supportedMediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
//        supportedMediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
//        supportedMediaTypes.add(MediaType.APPLICATION_PDF);
//        supportedMediaTypes.add(MediaType.APPLICATION_RSS_XML);
//        supportedMediaTypes.add(MediaType.APPLICATION_XHTML_XML);
//        supportedMediaTypes.add(MediaType.APPLICATION_XML);
//        supportedMediaTypes.add(MediaType.IMAGE_GIF);
//        supportedMediaTypes.add(MediaType.IMAGE_JPEG);
//        supportedMediaTypes.add(MediaType.IMAGE_PNG);
//        supportedMediaTypes.add(MediaType.TEXT_EVENT_STREAM);
//        supportedMediaTypes.add(MediaType.TEXT_HTML);
//        supportedMediaTypes.add(MediaType.TEXT_MARKDOWN);
//        supportedMediaTypes.add(MediaType.TEXT_PLAIN);
//        supportedMediaTypes.add(MediaType.TEXT_XML);
//        supportedMediaTypes.add(MediaType.ALL);
//        return supportedMediaTypes;
//    }

}