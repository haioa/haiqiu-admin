package com.haiqiu.security.config;


import com.haiqiu.security.encryption.SecurityPasswordEncoder;
import com.haiqiu.security.filter.JwtAuthenticationTokenFilter;
import com.haiqiu.security.filter.JwtFilterInvocationSecurityMetadataSource;
import com.haiqiu.security.handle.AccountAccessDeniedImpl;
import com.haiqiu.security.handle.AuthenticationEntryPointImpl;
import com.haiqiu.security.handle.LogoutSuccessHandlerImpl;
import com.haiqiu.security.handle.RoleAccessDecisionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;


/**
 * @author HaiQiu
 * 开启权限注解
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * 自定义用户认证逻辑
     */
    @Resource
    private UserDetailsService userDetailsService;

    /**
     * 自定义无权限逻辑
     */
    @Autowired
    private AccountAccessDeniedImpl accountAccessDenied;

    /**
     * 认证失败处理类
     */
    @Autowired
    private AuthenticationEntryPointImpl unauthorizedHandler;

    /**
     * 退出处理类
     */
    @Autowired
    private LogoutSuccessHandlerImpl logoutSuccessHandler;

    /**
     * token认证过滤器
     */
    @Autowired
    private JwtAuthenticationTokenFilter authenticationTokenFilter;

    @Autowired
    private RoleAccessDecisionManager roleAccessDecisionManager;

    @Autowired
    private JwtFilterInvocationSecurityMetadataSource jwtFilterInvocationSecurityMetadataSource;


//    /**
//     * 跨域过滤器
//     */
//    @Autowired
//    private CorsFilter corsFilter;

    /**
     * 解决 无法直接注入 AuthenticationManager
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .cors().and()
                .csrf().disable()// 由于使用的是JWT，我们这里不需要csrf
                .sessionManagement()// 基于token，所以不需要session
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()// 过滤请求
                //自定义路径动态鉴权
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                        o.setAccessDecisionManager(roleAccessDecisionManager);
                        o.setSecurityMetadataSource(jwtFilterInvocationSecurityMetadataSource);
                        return o;
                    }
                })
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated();
        // 禁用缓存
        httpSecurity.headers().cacheControl();

        httpSecurity.exceptionHandling()
                // 认证失败处理类
                .authenticationEntryPoint(unauthorizedHandler).
                and()
                .exceptionHandling()
                //自定义无权访问的资源异常
                .accessDeniedHandler(accountAccessDenied)
                .and()
                .headers().frameOptions().disable();
        //自定义退出登录器，这里暂时不用，使用我们自己写的controller逻辑，也可以使用默认的/logout
//        httpSecurity.logout().logoutUrl("/logout").logoutSuccessHandler(logoutSuccessHandler);
        // 添加JWT filter
        httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        // 添加CORS filter
//        httpSecurity.addFilterBefore(corsFilter, JwtAuthenticationTokenFilter.class);
//        httpSecurity.addFilterBefore(corsFilter, LogoutFilter.class);
    }

    /**
     * 密码加密
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new SecurityPasswordEncoder();
    }

    /**
     * 身份认证接口
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }


    /**
     * 资源放行
     *
     * @param web
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/swagger-ui.html","/swagger-ui/**","/v3/api-docs/**")
                .antMatchers("/webjars/**")
                .antMatchers("/v2/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/static/**")
                //登录+验证码
                .antMatchers("/login","/captcha")
                //获取初始化系统参数（系统名称，logo，登录背景图等信息）
                .antMatchers("/system/theme")
                //暂时放行资源文件+websocket+druid+忘记密码验证码
                .antMatchers("/resource/**","/ws/**","/druid/**","/forget/code","/forget/reset");
    }

}
