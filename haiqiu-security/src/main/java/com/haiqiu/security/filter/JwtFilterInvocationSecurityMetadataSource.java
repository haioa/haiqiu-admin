package com.haiqiu.security.filter;

import com.haiqiu.common.result.Constants;
import com.haiqiu.common.utils.web.RedisUtil;
import com.haiqiu.common.utils.web.ServletUtil;
import com.haiqiu.system.entity.SysPermission;
import com.haiqiu.system.mapper.SysPermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import java.util.Collection;
import java.util.List;



/**
 * @author HaiQiu
 * 根据当前访问的路径进行匹配数据库相应的权限菜单
 */
@Component
public class JwtFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Autowired
    private ServletUtil servletUtil;

    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        String userName = servletUtil.getUserName();
        if (userName != null) {
            //获取当前url
            String requestUrl = ((FilterInvocation) o).getRequestUrl();
            //获取当前请求方法
            String method = ((FilterInvocation) o).getRequest().getMethod();
            //get带参数的请求url
            if (requestUrl.contains("?")) {
                requestUrl = requestUrl.substring(0, requestUrl.indexOf("?"));
            }
            //获取数据库
            List<SysPermission> sysPermissionList = null;
            if (userName.equals(Constants.ADMIN)) {
                //获取登录之后缓存的权限
                if (redisUtil.hasKey(Constants.REDIS_USER_AUTH + userName)) {
                    sysPermissionList =(List<SysPermission>) redisUtil.get(Constants.REDIS_USER_AUTH + userName);
                    //否则从数据库获取数据账户
                } else {
                    sysPermissionList = sysPermissionMapper.all();
                }
            } else {
                //获取登录之后缓存的权限
                if (redisUtil.hasKey(Constants.REDIS_USER_AUTH + userName)) {
                    sysPermissionList =(List<SysPermission>) redisUtil.get(Constants.REDIS_USER_AUTH + userName);
                    //否则从数据库获取数据账户
                } else {
                    sysPermissionList = sysPermissionMapper.selectPermissionListByUsername(userName);
                }
            }
            //判断当前url和allPath是否匹配
            for (SysPermission sysPermission : sysPermissionList) {
                //匹配数据请求方法
                if (sysPermission.getMethod() != null) {
                    if (new AntPathMatcher().match(sysPermission.getPath(), requestUrl) && sysPermission.getMethod().equals(method)) {
                        //如果匹配上了，获取这个路径的权限
                        String[] rs = new String[]{sysPermission.getValue()};
                        //返回出去
                        return SecurityConfig.createList(rs);
                    }
                }

            }
        }
        //没有匹配上就默认一个权限值
        return SecurityConfig.createList("ROLE_ANONYMOUS");
    }

    /**
     * 返回所有定义好的权限资源
     * Spring Security在启动时会校验相关配置是否正确，如果不需要校验，直接返回null
     */
    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    /**
     * 是否支持校验
     */
    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
